# KVM Issues

## Overview

Using a TESmart 4:1 HDMI (4K) KVM switch

Keyboard and mouse
* Logitech MX Keys Mac keyboard
* Logitech MX Ergo trackball 
* Both of the above connected via single unified reciever

## Raspberry Pi

### Keyboard and mouse not working

No response at all, tried different cables.  The KVM works with Mac laptops
and keyboard/mouse seems fine.

After some searching found [this](https://forums.raspberrypi.com/viewtopic.php?t=307640)

Seems a module needs to be removed/prevented from loading by editing 
`/etc/modprobe.d/raspi-blacklist.conf`

```
# Need to remove this module
blacklist hid_logitech_dj
```


### Display keeps resizing

When switching away and back again XFCE keeps switching back to 1920 x 1080
despite the monitor (and KVM) being capable of 3440x1440@60Hz.

