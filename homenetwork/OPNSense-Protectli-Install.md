# Install of OPNSense on Protectli 

## Overview

Installation of OPNSense on a Protectli FW6C for use as the main 
gateway/router for network traffic.

Running the install via a Linux desktop (Ubuntu 22.04) but connected to 
that using XQuartz 2.8.5 from a Mac.

(Yes, that is a little over-complicated but what the hell :D )


## Install

### Initial Prep

* Connecting to the Linux desktop with the following command

```
ssh -Y fiona@manu
```

* On the remote end I have used the settings from this [my-x11-setup](https://github.com/fionahiklas/my-x11-setup)
* Downloading the installer image from this [link](https://mirror.ams1.nl.leaseweb.net/opnsense/releases/mirror/OPNsense-23.1-OpenSSL-serial-amd64.img.bz2)

```
wget https://mirror.ams1.nl.leaseweb.net/opnsense/releases/mirror/OPNsense-23.1-OpenSSL-serial-amd64.img.bz2
```

* Using `bzip2 -d <filename>` to uncompress the image
* Write to a USB Memory key using this command

```
sudo dd if=OPNsense-23.1-OpenSSL-serial-amd64.img of=/dev/sdb bs=16k
```

* Run screen to connect over a serial console with this command

```
screen /dev/ttyUSB1 115200
```

* I wasn't able to get this to work via an xterm, had to connect directly to the Mac and using screen on that :( 


### Setting up Certificates

