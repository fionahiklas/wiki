;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (mode . git-auto-commit))
 (markdown-mode
  (gac-automatically-push-p . 1)))

