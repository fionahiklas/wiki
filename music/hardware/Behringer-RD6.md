[//]: #TOCStart

[//]: #TOCEnd

## Setup

### Using SynthTribe on iOS

Able to change the settings using a lightning to USB B connector.  This seems fairly simple.


## MIDI Implementation

This information is available from the [RD-6 quickstart guide](https://mediadl.musictribe.com/media/PLM/data/docs/RD-6/RD-6_QSG_WW.pdf)


### MIDI message

|                  | Status | Second | Third | Parameter | Description          |
| ---------------- | ------ | ------ | ----- | --------- | -------------------- |
|                  |   8n   |   kk   |   vv  | [0, 7f]   |   Note off           |
| Channel Message  |   9n   |   kk   |   vv  | [0, 7f]   |   Note off           |
|                  |   Bn   |   7B   |       |           |   All Notes off      |



### Voices

| Voice       |   MIDI Note      |
| ----------- | ---------------- |
| Bass Drum   |    36, 0x24      |
| Clap        |    39, 0x27      |
| Snare Drum  |    40, 0x28      |
| Closed Hat  |    42, 0x2a      |
| Low Tom     |    45, 0x2D      |
| Open Hat    |    46, 0x2E      |
| Hi Tom      |    50, 0x32      |
| Cymbal      |    51, 0x33      |
