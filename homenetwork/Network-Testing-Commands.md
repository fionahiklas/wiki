[//]: #TOCStart

[//]: #TOCEnd


## Operating Systems

### Linux

#### Remove Address from Interface

```
ip address del 10.2.64.3/24 dev enp2s
```

If you don't provide the subnet mask it works with this warning

```
Warning: Executing wildcard deletion to stay compatible with old scripts.
         Explicitly specify the prefix length (10.2.64.3/32) to avoid this warning.
         This special behaviour is likely to disappear in further releases,
         fix your scripts!

```

#### Check For Promiscuous Mode

Running `netstat -i` shows interface details

```
netstat -i
Kernel Interface table
Iface      MTU    RX-OK RX-ERR RX-DRP RX-OVR    TX-OK TX-ERR TX-DRP TX-OVR Flg
enp1s0    1500  1545131      0 738250 0         22364      0      0      0 BMRU
enp3s0    1500   946899      0 112807 0          4712      0      0      0 BMRU
lo       65536  1016817      0      0 0       1016817      0      0      0 LRU
```

According to [LDP Netstat](https://tldp.org/LDP/nag2/x-087-2-iface.netstat.html) documentation the flags have the following meaning

* B - A broadcast address has been set.
* L - This interface is a loopback device.
* M - All packets are received (promiscuous mode).
* O - ARP is turned off for this interface.
* P - This is a point-to-point connection.
* R - Interface is running.
* U - Interface is up

Looks like the `ip link` command gives similar information

```
ip link 
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 00:0d:b9:41:d5:80 brd ff:ff:ff:ff:ff:ff
3: enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 00:0d:b9:41:d5:81 brd ff:ff:ff:ff:ff:ff
4: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 00:0d:b9:41:d5:82 brd ff:ff:ff:ff:ff:ff
```



#### Manipulate TCP/IP Routes

Show the current routes with the `ip route` command

```
ip route 
default via 10.2.133.4 dev enp3s0 proto static 
default via 10.2.123.4 dev enp1s0 proto dhcp src 10.2.123.3 metric 100 
10.2.123.0/24 dev enp1s0 proto kernel scope link src 10.2.123.3 
10.2.123.4 dev enp1s0 proto dhcp scope link src 10.2.123.3 metric 100 
10.2.133.0/24 dev enp3s0 proto kernel scope link src 10.2.133.215
``` 

Also `route -n` command works

```
route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         10.2.133.4   0.0.0.0         UG    0      0        0 enp3s0
0.0.0.0         10.2.123.4   0.0.0.0         UG    100    0        0 enp1s0
10.2.123.0   0.0.0.0         255.255.255.0   U     0      0        0 enp1s0
10.2.123.4   0.0.0.0         255.255.255.255 UH    100    0        0 enp1s0
10.2.133.0   0.0.0.0         255.255.255.0   U     0      0        0 enp3s0
```

And `netstat -rn`

```
netstat -rn
Kernel IP routing table
Destination     Gateway         Genmask         Flags   MSS Window  irtt Iface
0.0.0.0         10.2.123.4   0.0.0.0         UG        0 0          0 enp1s0
10.2.123.0   0.0.0.0         255.255.255.0   U         0 0          0 enp1s0
10.2.123.4   0.0.0.0         255.255.255.255 UH        0 0          0 enp1s0
10.2.133.0   0.0.0.0         255.255.255.0   U         0 0          0 enp3s0
```

Delete a default route using the `ip route del` command

```
ip route del default via 10.2.133.4
```


#### Packet Capture

Capture packets and write text version to STDOUT and file 

```
tcpdump -i enp3s0 -n -A | tee 20211211-tcpdump.log
```

Capture with the vlan tags 

```
tcpdump -i enp2s0 -n -e -vvv -A | tee 20211213-Logging.txt
```

Packets with tags will look like this

```
20:37:16.405098 a4:5e:60:b8:9c:7d > ff:ff:ff:ff:ff:ff, ethertype 802.1Q (0x8100), length 346: vlan 8, p 6, ethertype IPv4, (tos 0x0, ttl 255, id 44100, offset 0, flags [none], proto UDP (17), length 328)
    0.0.0.0.68 > 255.255.255.255.67: [udp sum ok] BOOTP/DHCP, Request from a4:5e:60:b8:9c:7d, length 300, xid 0xcbea3193, secs 8, Flags [none] (0x0000)
          Client-Ethernet-Address a4:5e:60:b8:9c:7d
          Vendor-rfc1048 Extensions
            Magic Cookie 0x63825363
            DHCP-Message Option 53, length 1: Request
            Parameter-Request Option 55, length 12: 
              Subnet-Mask, Classless-Static-Route, Default-Gateway, Domain-Name-Server
              Domain-Name, Option 108, URL, Option 119
              Option 252, LDAP, Netbios-Name-Server, Netbios-Node
            MSZ Option 57, length 2: 1500
            Client-ID Option 61, length 7: ether a4:5e:60:b8:9c:7d
            Requested-IP Option 50, length 4: 10.2.133.16
            Server-ID Option 54, length 4: 10.2.133.4
            Hostname Option 12, length 7: "stefano"
            END Option 255, length 0
            PAD Option 0, length 0, occurs 8
E..H.D.....a.........D.C.4:G......1......................^`..}..........................................................................................................................................................................................................c.Sc5..7..y...lrw._,.9...=...^`..}2.....6.......stefano.........
```


## Hardware

### Netgear

#### Mirroring Ports

* [Mirroring on GS724T and GS716T](https://www.downloads.netgear.com/files/GDC/GS716TV2/GS716T_GS724T-SWA-October2012.pdf)
* [Mirroring and VLANs](https://pfarrside.com/video_turotials/setup-port-mirroring-and-vlans-at-home-managed-switch/) - doesn't look like need to make destination a member

Important point is to ensure that the "session mode" is set to enable, without this it won't work.


## References

### Linux

#### General

* [The Linux Documentation Project](https://tldp.org/guides.html)
* [Linux Network Administrators Guide 2nd Edition](https://tldp.org/LDP/nag2/index.html)


#### Interface Management

* [How to check if promiscuous mode is enabled on network interface in Linux](https://tots.1o24.org/how-to-check-if-promiscuous-mode-is-enabled-on-network-interface-in-linux/)
* [Linux network administrators guide](https://books.google.co.uk/books?id=T8V02u2jDP0C&pg=PT84&lpg=PT84&dq=linux+netstat+-i+flags&source=bl&ots=6JZjtNcMM3&sig=ACfU3U2chcyx8xDmhfCUIrtv1_L1xFcNaw&hl=en&sa=X&ved=2ahUKEwjf0-r12dv0AhVMeMAKHbf8B_8Q6AF6BAgfEAM#v=onepage&q=linux%20netstat%20-i%20flags&f=false)
* [The Netstat Command](https://tldp.org/LDP/nag2/x-087-2-iface.netstat.html)
* [Delete IP address from interface](https://serverfault.com/questions/486872/remove-ip-with-ip-command-in-linux)

#### Routing

* [Adding route to Linux](https://devconnected.com/how-to-add-route-on-linux/) using `ip route` command
* [Routing setup](https://www.redhat.com/sysadmin/route-ip-route) with both `route` and `ip route` commands


#### Packet Capture

* [Tcpdump cheat sheet](https://www.andreafortuna.org/2018/07/18/tcpdump-a-simple-cheatsheet/)
