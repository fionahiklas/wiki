# Aeotec WallMote


## Overview



## Notes

### Blueprint Attempt

* Found the [blueprint](https://community.home-assistant.io/t/zwavejs-aeon-labs-aeotec-zw130-wallmote-quad-all-scenes-supported/290685) and used that URL to "Import Blueprint" using the button at the bottom right of the blueprints screen.
* Clicked on "Create Automation"
* Clicked on "Device" and the only option was "Quad 1" 
* Clicked on "Tap Top Left Actions" - Clicked "Add Action"
  * Selected "All Plug 1"
  * Action: Toggle Wall Plug 1





## References

* [WallMote Blueprint](https://community.home-assistant.io/t/zwavejs-aeon-labs-aeotec-zw130-wallmote-quad-all-scenes-supported/290685)
* [ZWave Integration](https://www.home-assistant.io/integrations/zwave_js)
