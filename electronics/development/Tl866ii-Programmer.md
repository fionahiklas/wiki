[//]: #TOCStart

[//]: #TOCEnd


## Overview 

Bought a [TL866II Plus programmer](https://www.amazon.co.uk/WINGONEER®-TL866II-Performance-EEPROM-Programmer/dp/B07B985LBS/ref=sr_1_4) from Amazn for writing data to EEPROM devices.  It can apparently program ATMega and PIC devices also.

Not going to use any of the supplied software as that is obviously at risk of Malware!

## Setup

* Compiled the [minipro code](https://gitlab.com/DavidGriffith/minipro) on a Raspberry Pi, specifically TinkerBell (RPi 4 with 8Gb memory)
* __Note:__ to build used these commands as I'm installing locally

```
make PREFIX=$HOME/local
make PREFIX=$HOME/local install
```

* Without the above the `logicic.xml` file isn't found in the correct place
* Installed the udev rules
* Added my user to the `plugdev` group
 

## Usage

### Test

* Using an AT28C64B EEPROM (8k x 8bit) 28pin chip
* Erasing the chip

```
minipro -p AT28C64B -E
```

* Ran the following command to read the contents of the fila

```
minipro -p AT28C64B -r empty_file.bin
```

* Copied to `edited_file.bin`
* Editing the file with the GNOME Hex editor `ghex`
* Checked the edits using the following command

```
hexdump edited_file.bin 
0000000 feca beba ffff ffff ffff ffff ffff ffff
0000010 ffff ffff ffff ffff ffff ffff ffff ffff
*
0000060 adde efbe ffff ffff ffff ffff ffff ffff
0000070 ffff ffff ffff ffff ffff ffff ffff ffff
*
0001ff0 ffff ffff ffff ffff ffff ffff ffff 00ff
0001fff
```

* Then wrote the data using the following command

```
minipro -p AT28C64B -w edited_file.bin 
Found TL866II+ 04.2.83 (0x253)
Warning: Firmware is out of date.
  Expected  04.2.128 (0x280)
  Found     04.2.83 (0x253)
Erasing... 0.02Sec OK
Protect off...OK
Writing Code...  1.64Sec  OK
Reading Code...  0.11Sec  OK
Verification OK
Protect on...OK
```


## Documentation

* Links to the manual
  * [Actual PDF](https://www.obdii365.com/upload/pro/tl866ii_plus_usb_programmer_user_manual.pdf)
  * [Hosted version with lots of ads](https://usermanual.wiki/Document/TL866IIInstructions.1206726909/html)
  * [PDF view of above](https://usermanual.wiki/Document/TL866IIInstructions.1206726909/view)
  * [Original roduct page](http://www.autoelectric.cn/en/tl866_main.html)


## Software

* Information about [oem softare has malware](https://www.eevblog.com/forum/beginners/tl866ii-plus-tl866acs-open-source-software-(oem-software-has-malware)/]
* The actual [open source software](https://gitlab.com/DavidGriffith/minipro)


