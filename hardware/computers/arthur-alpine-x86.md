# Wee Mad Arthur - Surface Pro 1 Apline Install

## Overview

I wondered if I could bring an old Surface Pro back to some useful, erm, use ...

... so I attempted (and succeeded) in installing Apline 3.21 on it :)


## Steps

* Follow the Surface Pro/Windows instructions to boot into the UEFI config



## Outstanding issues

### Wireless Network not autostarting

It seems that the `mlan0` device doesn't respond at boot time to an `ifup` and whinges but will start
once I've logged in.

__TODO:__ Add error messages and look for solution


## References

### Surface Pro 1

* [Surface Pro Teardown](https://www.ifixit.com/Teardown/Microsoft+Surface+Pro+Teardown/12842)
* [Running Portable Linux](https://mattbanderson.com/running-portable-linux-on-a-surface-pro/)


### Alpine

* [Network configuration](https://wiki.alpinelinux.org/wiki/Configure_Networking)

