[//]: #TOCStart

[//]: #TOCEnd

## Overview

Networking and setup information for Raspberry Pi based on Debian/Raspbian

## Setup

### Initial Setup

Apparently things have changed regarding security and [initial user creation](https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/)

In order to get SSH working at boot for a headless you need to do the following

* Create the SSH file

```
sudo touch /boot/ssh
```

* Create an initial user file

```
password=`echo "mypassword" | openssl passwd -6 -stdin`
sudo echo "myusername:$password" > /boot/userconf.txt
```


## Networking

### Adding VLAN

For the `interfaces.d` case you can add a VLAN by adding a file called `eth0.18` to `/etc/network/interfaces.d`

```
auto eth0.18
iface eth0.18 inet static
  vlan-raw-device eth0
  address 192.168.1.10
  netmask 255.255.255.0
```

You also need to install the `vlan` package

```
apt install vlan
```



## References 

### VLAN Network Setup

 * [[https://www.sbprojects.net/projects/raspberrypi/vlan.php|Setup VLAN address]]
 * [[https://community.domotz.com/topic/164-how-to-configure-support-for-multiple-vlans-raspberry-pi-osdebian/|Post about VLAN setup]]
 * [[https://wiki.debian.org/NetworkConfiguration|Debian network configuration (Buster)]]
