# Sally and Unity Alpine Setup

## Overview

Using a Mele Overclock 4C Mini PC with an N100 CPU, 32Gb RAM and 2Tb NVMe 
drive


## Initial Install

### Initial Install 

* Downloading Alpine 3.21.0 from [here](https://www.alpinelinux.org/downloads/)
* Wrote to USB memory stick using the following command (this was on a Mac but the Linux version is similar)

```
dd if=alpine-extended-3.21.0-x86_64.iso of=/dev/rdisk4 bs=1M status=progress
```

* Inserted USB drive and powered on device
* Drops to login prompt, enter `root` and hit enter
* Run 

```
setup-alpine
```

* Selected `gb` on first keyboard question
* Selected `gb-mac` on second question
* Hostname: `sally`
* Accepted defaults to configure `eth0` with `dhcp`
* Set simple root password
* Set timezone to `Europe/London`
* Accepted defaults for proxy (none)
* Accepted default for NTP (chrony)
* Accepted default for finding fastest mirror
* Setting up a user called `fiona`
* Accepted default for SSH server setup
* Selected `nvme0n1` disk as `sys`
* Allow partitioning and then enter `reboot`
* Remove USB stick on reboot 


### Post Install

* Edit the `/etc/apk/repositories` file and uncomment the community line
* Install desktop

```
setup-desktop
```

* Choosing XFCE
* Also installing a bunch of packages

```
apk add py3-pip python3-dev emacs kubectl k3s podman xfce4 wireshark thonny \
        texlive-full wireshark slirp4netns screen git emacs-x11 direnv \
		adw-gtk3 adwaita-icon-theme adwaita-xfce-icon-theme xfce4-screensaver \
		sed attr dialog bash bash-completion grep util-linux pciutils \
		usbutils binutils findutils readline lsof less nano curl \
		man-pages mandoc docs gnupg gpg-agent pinentry-curses-ss \
		pinentry-gtk pinentry-qt gnupg-scdaemon gnupg-dirmngr yubikey-manager \
		gcompat 
```




## Notes

### Install from SD card

This did NOT work - Alpine booted but complained about missing keymap packages



### Xfce greys out Shutdown/Restart

I didn't get this issue on Unity but did on Sally.

Follow these [instructions](https://wiki.alpinelinux.org/wiki/Xfce#Allowing_shut_down_and_reboot) running these commands as root

```
apk add elogind polkit-elogind
```


### Getting Jetbrains IntelliJ to work

* Download IntelliJ IDEA Ultimate from [here](https://www.jetbrains.com/idea/)
* As root run the following commands

```
cd /opt
tar -xf ~fiona/Download/ideaIU-2024.3.1.1.tar.gz
ln -s /opt/idea-IU-243.22562.218 /opt/idea
```

* Initial run of the IDE as a normal user

```
export IDEA_SDK=/usr/lib/jvm/openjdk21 /opt/idea/bin/idea.sh
```

* Follow the [instructions](https://intellij-support.jetbrains.com/hc/en-us/articles/206544879-Selecting-the-JDK-version-the-IDE-will-run-under) to set custom JDK 
  * Select Help -> Find Action
  * Enter "Choose Boot Java Runtime"
  * Select the current running IDE (which will be the custom one used to launch the IDE)
  * This should be `/usr/lib/jvm/java-21-openjdk`
* Create desktop entries, select menu item Tools -> Create Desktop Entry




## References

### Alpine

* [Downloads](https://www.alpinelinux.org/downloads/)
* [Setup-alpine](https://docs.alpinelinux.org/user-handbook/0.1a/Installing/setup_alpine.html)
* [Xfce setup](https://wiki.alpinelinux.org/wiki/Xfce)



### IntelliJ

* [Runtime on musl based systems](https://youtrack.jetbrains.com/articles/JBR-A-2/JetBrains-Runtime-on-musl-based-Systems)
* [Create desktop shortcut](https://intellij-support.jetbrains.com/hc/en-us/community/posts/207111509-Linux-how-to-create-desctop-shortcut)
* [Set Custom JDK for IDE Runtime](https://intellij-support.jetbrains.com/hc/en-us/articles/206544879-Selecting-the-JDK-version-the-IDE-will-run-under)


