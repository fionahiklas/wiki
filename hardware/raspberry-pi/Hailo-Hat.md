# Hailo Hat

## Notes

### Ensure Desktop 

* Need to ensure that we login using the desktop GUI on the PI to be able to see results of processing/cameras
* Run the following command to start with

```
sudo apt install lightdm xserver-xorg raspberrypi-ui-mods arandr
```

### Software Install

* For the binary packages and such

```
apt install hailo-all
```




## References

### Raspberry Pi 

* [Convert lite to desktop GUI](https://pimylifeup.com/raspberry-pi-os-lite-desktop/)


### Raspberry Pi Hailo Docs

* [AI Kit](https://www.raspberrypi.com/documentation/accessories/ai-kit.html)
* [Getting Started with AI Kit](https://www.raspberrypi.com/documentation/computers/ai.html)



