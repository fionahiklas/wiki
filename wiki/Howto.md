# Wiki HowTo


[//]: #TOCStart

## Table of Contents

- [Wiki HowTo](#wiki-howto)
  - [Naming pages](#naming-pages)
  - [Wiki Syntax](#wiki-syntax)
  - [Local Copy of Wiki](#local-copy-of-wiki)
  - [Local Viewing](#local-viewing)


[//]: #TOCEnd


## Naming pages

Unlike the Wiki options from Github and Gitea (at time of writing) for this "wiki" I'm just using a directory 
structure to "name" pages.


## Wiki Syntax

* Markdown
  * [Basic Syntax](https://www.markdownguide.org/basic-syntax)
  * [Extended Syntax](https://www.markdownguide.org/extended-syntax/)
* GitHub
  * [Markdown Reference Wiki](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
  * [Markdown Cheat sheet PDF](https://enterprise.github.com/downloads/en/markdown-cheatsheet.pdf)


## Local Copy of Wiki

For this wiki located at [https://codeberg.org/fionahiklas/wiki](https://codeberg.org/fionahiklas/wiki) you're given the option to clone [https://codeberg.org/fionahiklas/wiki.git](https://codeberg.org/fionahiklas/wiki.git) which will give a local copy.  However, to be able to write/push to the Wiki clone using the following command

```
git clone git@codeberg.org:fionahiklas/wiki.git
```

You can edit files locally, commit these, and then eventually push back to the main Wiki repository on GitHub at which point the
pages will be rendered and displayed under the URL as [above](https://codeberg.org/fionahiklas/wiki).


## Local Viewing

The GitHub (and Gitlab) Wiki engines are based on a [Ruby](https://www.ruby-lang.org/) package called [Gollum](https://rubygems.org/gems/gollum/) whose source code is available in a [GitHub repo](https://github.com/gollum/gollum) also.

You can install Gollum locally using the following command

```
gem install --user gollum
```

This won't need admin rights as the content will be installed under `$HOME/.gem` and you will need to add the `bin` directory to your
`PATH` environment variable using the following command

```
export PATH=$PATH:$HOME/.gem/ruby/2.6.0/bin
```

__NOTE:__ This is for the natively installed Ruby version under MacOS Monterey.  If you have a different version of Ruby installed then you may have to alter the version number in the command above

Once installed you can change to the directory where a GitHub Wiki has been cloned and simply run this command

```
gollum
```

You'll then be able to view the rendered pages for that Wiki by visiting [http://localhost:4567](http://localhost:4567) which is the gollum UI which also provides limited editing features in browser also.

__IMPORTANT NOTE:__ Gollum only displayed *committed* edits/pages.  It you edit through Gollum itself then a commit will be generated to the repo, if you use an external editor you need to add/commit to the local repo before the updates can be seen in the browser.  You don't have to push back to GitHub obviously.


## Table of Contents Generator

Add the TOC tags to the top of a markdown file:

```

[//]: #TOCStart


[//]: #TOCEnd

```

__NOTE:__ make certain there is whitespace around the above tags otherwise the github
markdown (and possibly some others) will not parse/render the page correctly.

then run the following command against the markdown file, e.g this one

```
./scripts/toc.sh Wiki-Howto.md
```

Once this completes there should be a table of contents at the top of the file - you may
need to reload the file from disk in your IDE if it doesn't automatically detect file
changes

To update all the TOC of our wiki markdown files you can run
```
find . -maxdepth 1 ! -name 'Wiki-Howto.md' -name "*.md" -type f -exec scripts/toc.sh {} \;
rm ./*.md.bak
```
(`Wiki-Howto.md` is omitted as otherwise the TOC generator will populate the sample code in the above code snippet)


## References

### Gollum

* [Gollum Wiki](https://github.com/gollum/gollum/wiki)


