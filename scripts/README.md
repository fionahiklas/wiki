## Overview

Scripts to help with Wiki editing/generation


## Table of Contents

For markdown files you can insert the following in the file and use the `./scripts/toc.sh` 
script to insert a table of contents which is generated from the headers

```

[//]: #TOCStart


[//]: #TOCEnd

```

__NOTE:__ make certain there is whitespace around the above tags otherwise the github 
markdown (and possibly some others) will not parse/render the page correctly.

Bear in mind that anything initially between the tags will be replaced.  Once the file
contains the generated TOC it can be commited.  Regenerating the TOC will remove what 
is there between the tags.


## References

### 3rd Party

* [Markdown TOC generator](https://github.com/Lirt/markdown-toc-bash)


### Markdown

* [Comments in Markdown](https://stackoverflow.com/questions/4823468/comments-in-markdown)


### Shell

* [Inserting file into another](https://unix.stackexchange.com/questions/32908/how-to-insert-the-content-of-a-file-into-another-file-before-a-pattern-marker)
* [Convert to lowercase using tr](https://www.cyberciti.biz/faq/linux-unix-shell-programming-converting-lowercase-uppercase/)


### Perl
 
* [Print newline](https://stackoverflow.com/questions/2899367/how-can-perls-print-add-a-newline-by-default)
* [Perl regex](https://perldoc.perl.org/perlre)
* [Passing variables to Perl scripts](https://stackoverflow.com/questions/34867063/how-do-i-get-command-line-perl-to-accept-shell-variables)
