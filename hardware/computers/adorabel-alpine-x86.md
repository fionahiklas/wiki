# HP Pavillion 15.6 Laptop

## Overview 

Installing Alpine Linux 3.21 on a HP Laptop.

This is a fairly cheap laptop from [Amazon](https://www.amazon.co.uk/dp/B0C1TDC1ZB?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1) the details are as follows

* Model: HP Pavillion 15-eh3000sa
* CPU: AMD Rzyen 7-7730H
* Memory: 16Gb 
* Disk: 512Gb NVMe
* Ports: USB3 and one USB-C, HDMI 2.1

__NOTE:__ By following the iFixit repair guide and purchasing a 2Tb NVMe drive along with recommended Crucial 
memory I was able to upgrade the laptop to a 64Gb machine with the larger drive capacity.  It should also be 
noted that the laptop I have seems the same/similar as the iFixit pictures portray and the WiFi model is in,
what looks like, and M.2 socket so should be upgradable.


## Getting into BIOS

* I had to hit ESC multiple times quickly after powering on the laptop otherwise it went into the installed Windows
* From there I could access the Boot and BIOS menus
* Disabled secure boot 
* Set boot order to inserted USB3 drive first
* Eventually this change was saved and the Laptop tried to boot from USB stick


## Install

### First attempt

* Using an Alpine 3.21.1 installer on a Kingston USB stick
* Got to "Boot Linux LTS" and then hung
* Tried several times but got nowehere


### Second Attempt

* Booting from Ubuntu 24.04 live image
* This booted completely and got all the way to desktop
* Was able to try WiFi and sleep/resume
* All seemed to work fine


### Third Attempt

* Wrote Alpine 3.21.2 Extened image to SanDisk memory stick
* First boot didn't seem to work 
* Second one I hit 'e' to edit the boot params
* Tried `nomodechange` which didn't work
* Tried removing `quiet` and did see kernal boot messages
* Then saw `... /dev/sda ... blockdev` errors
* Tried again and it seemed to get past these and booted :D 
* I was then able to install completely 

## Alpine Setup

Follow the instructions in [common setup](./hardware/computers/common-alpine-x86-setup.md) to install
and configure Alpine Linux.



## References

### Hardware

* [Amazon item](https://www.amazon.co.uk/dp/B0C1TDC1ZB?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1)
* [Hardware probe for similar device](https://linux-hardware.org/?probe=f26159e727)
* [HDMI 1.2 Open Source Drivers not supported](https://www.howtogeek.com/hdmi-forum-open-source-drivers-hdmi-2-1/)
* [iFixIt repair guide](https://www.ifixit.com/Device/HP_Pavilion_15-eh)
* [Internals of HP 15-eh3000 laptops](https://laptopmedia.com/gb/guides/how-to-open-hp-pavilion-15-15-eh3000-disassembly-and-upgrade-options/)
* [Crucial upgrade page for the laptop](https://www.crucial.com/compatible-upgrade-for/hp/pavilion-15-eh3000?_gl=1*1aexxdc*_up*MQ..*_ga*MTg4NDU3MjM3OC4xNzM2NDIxNzY4*_ga_6H4RYWV7QY*MTczNjQyMTc2OC4xLjEuMTczNjQyMTc3Ni4wLjAuMTYzODI1ODQ4OA..)

