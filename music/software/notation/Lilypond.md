# Lilypond

## Overview

Typesetting framework for musical notation


## Text input

Following the example given for [text input](https://lilypond.org/text-input.html)

```
{
  \time 2/4
  \clef bass
  c4 c g g a a g2
}
```

The file is available [here](./files/test-1.ly)

Running the following command 

```
lilypond -f png -dcrop='#t' test-1.ly
```

Creates both the `test-1.png` and `test-1.cropped.png` files.  The former appears to 
be approximately A4 sized (when previewed) whereas the latter contains just the 
stave and notes without any extra white space around the image.

This gave the following cropped result

![Test-1 cropped image](./files/test-1.cropped.png)



Tried another [example file](./files/test-2.ly)

```
{
  \time 2/4
  \clef bass
  c4 c4 c2 g g4 a a2 g
}
```

This gave the following result

![Test-2 cropped image](./files/test-2.cropped.png)

And another [example](./files/test-3.ly)

```
{
  \time 2/4
  \clef treble
  c' d' e' f' g' a' b' c'' d'' e''
}

{
  \time 2/4
  \clef bass
  a, b, c d e f g a b c'
}
```

Gave the following output

![Test-3 cropped image](./files/test-3.cropped.png)




## References

* [Lilypond usage page](https://lilypond.org/doc/v2.23/Documentation/usage-big-page.html)
