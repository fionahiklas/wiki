<<TableOfContents>>

== Overview ==

Covers the configuration of the Unifi network


== Wireless Access Points ==

=== Locations ===

 * uni01wap - Top floor
 * uni02wap - 1st floor ceiling
 * uni03wap - Ground floor ceiling
 * uni04wap - Driveway, wall-mounted


=== Configuration ===

Enabled Minimum RSSI so that WAPs don't overlap in their coverage

Details in this [[https://help.ui.com/hc/en-us/articles/221321728-UniFi-Understanding-and-Implementing-Minimum-RSSI|document]]
