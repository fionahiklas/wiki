# MOTU Express 128

## Overview

Sixteen channel MIDI controller

See [product page](https://motu.com/products/midi/128) for more details


## Drivers

### MacOS Sonoma

According too [this page](https://motu.com/en-us/news/motu-and-macos-sonoma/) downloading the 
[latest drivers](https://motu.com/en-us/download/product/18/?platform_family=any&download_type=user+guide#549)
(May 17 2022 at time of writing) should be enough to ensure a working setup.

Also need to [enable these drivers](https://motu.com/techsupport/technotes/enabling-motu-drivers-high-sierra)
in MacOS Settings.
