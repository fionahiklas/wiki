## Overview

This has taken some significant time to setup!

## Printing to dot matrix from MacOS

This isn't entirely straight forward

Some initial notes

* This [post](https://stackoverflow.com/questions/48838486/printing-to-a-dot-matrix-printer-with-raw-ascii-from-cups) suggests `lp` will work
* Ran `lpinfo -m` got this

```
drv:///sample.drv/dymo.ppd DYMO Label Printer
drv:///sample.drv/epson9.ppd Epson 9-Pin Series
drv:///sample.drv/epson24.ppd Epson 24-Pin Series
drv:///sample.drv/generpcl.ppd Generic PCL Laser Printer
drv:///sample.drv/generic.ppd Generic PostScript Printer
drv:///sample.drv/deskjet.ppd HP DeskJet Series
drv:///sample.drv/laserjet.ppd HP LaserJet Series PCL 4/5
drv:///sample.drv/intelbar.ppd Intellitech IntelliBar Label Printer, 2.3
drv:///sample.drv/okidata9.ppd Oki 9-Pin Series
drv:///sample.drv/okidat24.ppd Oki 24-Pin Series
raw Raw Queue
drv:///sample.drv/zebracpl.ppd Zebra CPCL Label Printer
drv:///sample.drv/zebraep1.ppd Zebra EPL1 Label Printer
drv:///sample.drv/zebraep2.ppd Zebra EPL2 Label Printer
drv:///sample.drv/zebra.ppd Zebra ZPL Label Printer
everywhere IPP Everywhere
```

* Ran this command

```
lp -o raw _Sidebar.md
```

* This only partially worked - the first line was skipped and the page length/form feed seems to be wrong as well
* Still need to tweak some settings/setup I think 
* Found this [post](https://superuser.com/questions/1711582/dot-matrix-printer-source-code-syntax-highlighting) talking about syntax highlighting in dot matrix printing



## Sharing Across Subnets

There are several subnets

* LAN - this is the internal infrastructure
* Wireless - and connected wireless device such as phone, tablets, etc
* Wired - desktop machines

I need to be able to discover services across these subnets

### pfSense Avahi 

There is a package implementing/using the [ahavi](http://avahi.org) project service.  More information on this service is also available on [Wikipedia](https://en.wikipedia.org/wiki/Avahi_%28software%29) and from the original [project GitHub](https://github.com/lathiat/nss-mdns).

The [Bonjour spec](https://developer.apple.com/bonjour/printing-specification/bonjourprinting-1.2.1.pdf) also lists some handy information.

#### Setup

* Installed the package using the package manager
* Selected Services -> Avahi
* Entered the following settings
  * Enable: ticked this 
  * CARP status VIP: defaults and left as none
  * Interface Action: Allow interfaces
  * Interfaces: selected LAN, Wired, Wireless
  * Disable support for IPv4: left unticked
  * Disable support for IPv6: left unticked
  * Enable reflection: ticked
  * Reflection filtering, added the following services
    * `_printer._tcp.local`
    * `_ipp._tcp.local`
    * `_pdl-datastream._tcp.local`
  * Enable publishing: unticked
  * Left advanced settings for now



#### Printer ports

The firewall needs to be set to allow cross-subnet connections.

Apple lists port numbers used by various services [here](https://support.apple.com/en-gb/HT202944), combined with the 
[Bonjour spec](https://developer.apple.com/bonjour/printing-specification/bonjourprinting-1.2.1.pdf) I believe the list 
of ports is as follows

* 515 - TCP - LPR/LPD Printing protocol
* 631 - TCP - IPP Printing protocol
* 9100 - TCP - Network printing service for certain printers

