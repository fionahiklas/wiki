[//]: #TOCStart

[//]: #TOCEnd

## Viewport

* Zoom  - mouse wheel 
* Orbit - middle mouse button
* Pan   - SHIFT + middle mouse button


## Camera

* Pan  - Left click to select camera.  Then press `G` 


