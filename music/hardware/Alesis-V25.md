[//]: #TOCStart

[//]: #TOCEnd


## MIDI

### Drum Pads

By default these notes are all on MIDI Channel 10


| Pad Location   | Note value |
| -------------- | ---------- |
|   Top, 1st     |   0x31     |
|   Top, 2nd     |   0x29     |
|   Top, 3rd     |   0x2A     |
|   Top, 4th     |   0x2E     |
|   Bottom, 1st  |   0x24     |
|   Bottom, 2nd  |   0x25     |
|   Bottom, 3rd  |   0x26     |
|   Bottom, 4th  |   0x27     |
