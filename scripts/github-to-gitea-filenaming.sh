#!/bin/sh
#
# -*- mode: shell-script; -*-
# 
# Convert from github compliant Wiki filenames to gitea ones
#

function fullPathFromFilename() {
    echo $1 | sed -e s%:-%/%g
}

function ensureDirectoryExists() {
    if [ ! -d $1 ]
    then
	echo "Creating directory: $1"
	mkdir -p $1
    fi	
}

function moveFileToDirectory() {
    echo "Moving '$1' to '$2' as '$3'"
    mv $1 $2/$3
}


for rootMarkdownFile in *.md
do
    echo "Processing: $rootMarkdownFile"
    fullPath="$(fullPathFromFilename $rootMarkdownFile)"
    newDirectoryPath=`dirname "$fullPath"`
    newFilename=`basename "$fullPath"`

    ensureDirectoryExists $newDirectoryPath
    moveFileToDirectory $rootMarkdownFile $newDirectoryPath $newFilename
done




