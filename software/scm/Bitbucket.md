# Bitbucket

[//]: #TOCStart

[//]: #TOCEnd


## Keys

Recently using a new machine to connect to bitbucket and got the SSH
warning about host verification

```
The authenticity of host 'bitbucket.org (104.192.141.1)' can't be established.
RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

Thought I'd check what the RSA fingerprint *should* be before accepting this

The keys seem seem to be documented [here](https://support.atlassian.com/bitbucket-cloud/docs/configure-ssh-and-two-step-verification/)

__SHA256 format__

```
2048 SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A bitbucket.org (RSA)
1024 SHA256:RezPkAnH1sowiJM0NQXH90IohWdzHc3fAisEp7L3O3o bitbucket.org (DSA)
```

__md5 format__

```
97:8c:1b:f2:6f:14:6b:5c:3b:ec:aa:46:46:74:7c:40 (RSA)
35:ee:d7:b8:ef:d7:79:e2:c6:43:9e:ab:40:6f:50:74 (DSA)
```
