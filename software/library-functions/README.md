# Library Functions

## References

* [Count positive bits](https://stackoverflow.com/questions/12055499/c-sneaky-way-to-count-positive-bits)
* [ARM __builtin_popcountll](https://stackoverflow.com/questions/70008561/why-doesn-t-clang-use-vcnt-for-builtin-popcountll-on-aarch32)
* [ARM VCNT Instruction](https://developer.arm.com/documentation/ddi0406/c/Application-Level-Architecture/Instruction-Details/Alphabetical-list-of-instructions/VCNT)
* [Builtin GCC functions](https://www.geeksforgeeks.org/builtin-functions-gcc-compiler/)

