## Overview

Some tips for using the Kobo Elipsa


## Footnotes

I'm still not 100% sure I have this working properly but a 
[post](https://www.mobileread.com/forums/showthread.php?t=284576) 
suggested that kepub is a better format that epub in terms 
of handling footnotes and having these appear in dialogs rather than 
skip to the end of a document

I found a tool, [kepubify](https://github.com/pgaskin/kepubify) that seems
to handle the conversion.  I was able to install this with 
`brew install kepubify`

Ran the following command to get it to convert

```
kepubify --fullscreen-reading-fixes -i FromKindle -o Converted
```

Deleted all the epub and uploaded the new `kepub.epub` files to the 
Kobo reader.

On a test book, Sourcery by Terry Pratchett, the first footnote seemed to 
try and go to the end of the document but subsequent ones opened in dialogs
after I tried to tweak font size and some other settings.

I think more investigation is needed.

