## Overview 

Emacs setup and tips


## Setup

### Go Mode with LSP

* Following these [instructions](https://geeksocket.in/posts/emacs-lsp-go/)




## FAQ

### How to disable font lock

If you have a big buffer, I found this when working with a page containing lots
of log output, Emacs on a slower machine (Raspberry Pi) can start to crawl and 
become unusable.

You can disable font-lock (syntax highlighting) using 

```
M-x font-lock-mode
```

See further details in this [post](https://unix.stackexchange.com/questions/108257/how-do-i-stop-emacs-from-colouring-the-text-of-the-file-i-am-editing)

Also, as linked in the post, the [Emacs manual font-lock page](https://www.gnu.org/software/emacs/manual/html_node/emacs/Font-Lock.html) has more information also.


### How to access menu in NOX

* According to this [post](https://emacs.stackexchange.com/questions/9678/how-to-use-the-menu-bar-in-terminal-emacs)
* Use F10 (or fn-F10 on a Mac)




## References

* [GMail, GNUS and GPG setup](https://www.emacswiki.org/emacs/Gmail%2c_Gnus_and_GPG)
