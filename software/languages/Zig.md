# Zig

## Installation

### MacOS

* Using the following

```
brew install zig zls
```



## References

### Language

* [Zig Getting started](https://ziglang.org/learn/getting-started/#linux)


### Emacs

* [Zig LSP mode support](https://emacs-lsp.github.io/lsp-mode/page/lsp-zig/)
* [Zig LSP setup for emacs](https://github.com/zigtools/zls/wiki/Installation#emacs)

