[//]: #TOCStart

[//]: #TOCEnd

## Overview

On `gingy` which I just setup am seeing lots of errors about Read Only filesystems

Googling gave these results

 * [Systemd Journal Crash](https://askubuntu.com/questions/1173738/crash-systemd-journal-failed-to-write-entry-ignoring-read-only-file-system-on)
 * [Solid state drive firmware](https://wiki.archlinux.org/title/Solid_state_drive#Firmware)
 * [Arch linux firmware update daemon](https://wiki.archlinux.org/title/Fwupd)


These are the kind of issues that I'm seeing 

```
[85392.450708] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85481.874442] sd 0:0:0:0: [sda] tag#29 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.874459] sd 0:0:0:0: [sda] tag#29 CDB: Read(10) 28 00 0a 14 19 f8 00 00 08 00
[85481.874465] print_req_error: 2 callbacks suppressed
[85481.874473] blk_update_request: I/O error, dev sda, sector 169089528 op 0x0:(READ) flags 0x80700 phys_seg 1 prio class 0
[85481.885565] sd 0:0:0:0: [sda] tag#30 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.885581] sd 0:0:0:0: [sda] tag#30 CDB: Read(10) 28 00 0a 14 19 f8 00 00 08 00
[85481.885592] blk_update_request: I/O error, dev sda, sector 169089528 op 0x0:(READ) flags 0x0 phys_seg 1 prio class 0
[85481.887076] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85481.897543] sd 0:0:0:0: [sda] tag#0 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.906599] sd 0:0:0:0: [sda] tag#0 CDB: Read(10) 28 00 0a 14 1a 00 00 00 08 00
[85481.906615] blk_update_request: I/O error, dev sda, sector 169089536 op 0x0:(READ) flags 0x80700 phys_seg 1 prio class 0
[85481.908977] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85481.918255] sd 0:0:0:0: [sda] tag#1 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.927915] sd 0:0:0:0: [sda] tag#1 CDB: Read(10) 28 00 0a 14 1a 00 00 00 08 00
[85481.927922] blk_update_request: I/O error, dev sda, sector 169089536 op 0x0:(READ) flags 0x0 phys_seg 1 prio class 0
[85481.928842] systemd-journald[24482]: Failed to write entry (9 items, 254 bytes), ignoring: Read-only file system
[85481.942807] sd 0:0:0:0: [sda] tag#2 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.948799] sd 0:0:0:0: [sda] tag#2 CDB: Read(10) 28 00 0b 11 1b a8 00 00 08 00
[85481.948810] blk_update_request: I/O error, dev sda, sector 185670568 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85481.949113] systemd-journald[24482]: Failed to write entry (9 items, 323 bytes), ignoring: Read-only file system
[85481.961013] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5769202: comm sshd: reading directory lblock 0
[85481.970745] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85481.981412] sd 0:0:0:0: [sda] tag#3 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85481.990987] sd 0:0:0:0: [sda] tag#3 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85481.990996] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85481.991821] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85482.001680] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85482.001687] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85482.003333] EXT4-fs (sda5): I/O error while writing superblock
[85482.012174] systemd-journald[24482]: Failed to write entry (9 items, 319 bytes), ignoring: Read-only file system
[85482.023573] sd 0:0:0:0: [sda] tag#4 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85482.030892] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85482.035959] sd 0:0:0:0: [sda] tag#4 CDB: Read(10) 28 00 0b 11 1b b0 00 00 08 00
[85482.035968] blk_update_request: I/O error, dev sda, sector 185670576 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85482.037214] EXT4-fs warning (device sda5): htree_dirblock_to_tree:997: inode #5769203: lblock 0: comm sshd: error -5 reading directory block
[85482.046935] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85482.078587] systemd-journald[24482]: Failed to write entry (9 items, 323 bytes), ignoring: Read-only file system
[85482.089786] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85482.101741] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85482.112238] systemd-journald[24482]: Failed to write entry (9 items, 319 bytes), ignoring: Read-only file system
[85482.124045] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85482.135503] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85482.146007] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85482.156429] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85482.167476] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85482.178578] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85482.189059] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85482.199486] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85482.209884] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85482.220323] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85482.231312] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85482.242407] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85482.252880] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85482.263259] systemd-journald[24482]: Failed to write entry (9 items, 343 bytes), ignoring: Read-only file system
[85482.275261] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.285943] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.296511] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.307089] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.317672] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.328301] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.338825] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.349332] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.359900] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.370476] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.381046] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.391605] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.402176] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.412761] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.423324] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.433906] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.444470] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.455028] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.465697] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.476305] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.486846] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.497379] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.507937] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.518510] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.529072] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.539645] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.550210] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.560770] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.571322] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.581882] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.592452] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.603013] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.613570] systemd-journald[24482]: Failed to write entry (22 items, 653 bytes), ignoring: Read-only file system
[85482.624150] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.634727] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.645296] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.655863] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.666419] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.676987] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.687544] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.698096] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.708658] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.719222] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.729770] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.740361] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.750876] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.761419] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.771985] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.782567] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.793123] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.803676] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.814240] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.824810] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.835371] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.845933] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.856494] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.867062] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.877628] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.888214] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.898746] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85482.909250] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.919819] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.930370] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.940938] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.951498] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.962069] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.972628] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85482.983210] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85482.993788] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.004359] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.014861] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.025398] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.035956] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.046513] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.057079] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.067646] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.078217] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.088777] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.099335] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.109887] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.120449] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.131010] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.141558] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.152164] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.162696] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.173219] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.183771] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.194331] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.204901] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.215461] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.226038] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.236599] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.247163] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.257717] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.268285] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.279577] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.290182] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.300751] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.311317] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.322040] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.332747] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.343400] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.354044] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.364686] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.375313] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.385972] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.396607] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.407247] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.417886] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.428622] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.439247] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.449879] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.460509] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.471241] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.481886] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.492540] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.503174] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.513811] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.524447] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.535084] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.545705] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.556358] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.566866] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.577412] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.587973] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.598553] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.609123] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.619681] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.630251] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.640812] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.651376] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.661933] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.672505] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.683072] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.693624] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.704209] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.714750] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.725270] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.735834] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.746390] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.756958] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.767518] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.778089] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.788649] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.799213] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.809793] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.820362] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.830923] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.841494] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.852097] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.862633] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.873146] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.883702] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.894252] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.904816] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.915384] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85483.925950] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.936511] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.947072] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.957619] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.968224] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.978723] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85483.989262] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85483.999820] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85484.010377] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85484.020951] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85484.031521] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85484.042082] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85484.052644] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85484.063208] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85484.073786] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85484.084349] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85484.094910] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85485.400292] sd 0:0:0:0: [sda] tag#5 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85485.400307] sd 0:0:0:0: [sda] tag#5 CDB: Read(10) 28 00 09 d1 27 50 00 00 08 00
[85485.400317] blk_update_request: I/O error, dev sda, sector 164702032 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85485.412302] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5113842: comm sshd: reading directory lblock 0
[85485.423251] sd 0:0:0:0: [sda] tag#20 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85485.423263] sd 0:0:0:0: [sda] tag#20 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85485.423272] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85485.433988] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85485.441466] EXT4-fs (sda5): I/O error while writing superblock
[85485.447698] sd 0:0:0:0: [sda] tag#21 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85485.447706] sd 0:0:0:0: [sda] tag#21 CDB: Read(10) 28 00 09 d1 27 50 00 00 08 00
[85485.447760] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5113842: comm sshd: reading directory lblock 0
[85485.458612] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85485.466070] EXT4-fs (sda5): I/O error while writing superblock
[85491.738904] systemd-journald[24482]: Failed to write entry (23 items, 611 bytes), ignoring: Read-only file system
[85491.750024] systemd-journald[24482]: Failed to write entry (23 items, 612 bytes), ignoring: Read-only file system
[85491.761500] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85491.772260] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85491.783710] systemd-journald[24482]: Failed to write entry (22 items, 653 bytes), ignoring: Read-only file system
[85491.795828] systemd-journald[24482]: Failed to write entry (28 items, 746 bytes), ignoring: Read-only file system
[85491.820419] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85491.830998] scsi_io_completion_action: 1 callbacks suppressed
[85491.831015] sd 0:0:0:0: [sda] tag#6 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85491.831052] sd 0:0:0:0: [sda] tag#6 CDB: Read(10) 28 00 0a 11 14 38 00 00 08 00
[85491.831056] print_req_error: 4 callbacks suppressed
[85491.831065] blk_update_request: I/O error, dev sda, sector 168891448 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85491.842913] EXT4-fs warning (device sda5): htree_dirblock_to_tree:997: inode #5243343: lblock 0: comm run-parts: error -5 reading directory block
[85491.846026] sd 0:0:0:0: [sda] tag#23 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85491.846041] sd 0:0:0:0: [sda] tag#23 CDB: Read(10) 28 00 09 d1 27 50 00 00 08 00
[85491.846051] blk_update_request: I/O error, dev sda, sector 164702032 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85491.847016] systemd-journald[24482]: Failed to write entry (9 items, 264 bytes), ignoring: Read-only file system
[85491.857005] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5113842: comm sshd: reading directory lblock 0
[85491.869606] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85491.889743] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85491.898082] sd 0:0:0:0: [sda] tag#31 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85491.900105] sd 0:0:0:0: [sda] tag#31 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85491.900116] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85491.900288] systemd-journald[24482]: Failed to write entry (9 items, 254 bytes), ignoring: Read-only file system
[85491.910824] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85491.910834] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85491.922953] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85491.931700] EXT4-fs (sda5): I/O error while writing superblock
[85491.958125] systemd-journald[24482]: Failed to write entry (9 items, 348 bytes), ignoring: Read-only file system
[85491.972498] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85491.984081] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85491.989700] sd 0:0:0:0: [sda] tag#7 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85491.994453] sd 0:0:0:0: [sda] tag#7 CDB: Read(10) 28 00 09 d1 27 50 00 00 08 00
[85491.994464] blk_update_request: I/O error, dev sda, sector 164702032 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85491.996561] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85492.005400] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5113842: comm sshd: reading directory lblock 0
[85492.016587] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.026830] sd 0:0:0:0: [sda] tag#8 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.036645] sd 0:0:0:0: [sda] tag#8 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85492.036653] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85492.039592] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85492.047348] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85492.059347] systemd-journald[24482]: Failed to write entry (12 items, 355 bytes), ignoring: Read-only file system
[85492.068250] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.070810] EXT4-fs (sda5): I/O error while writing superblock
[85492.080374] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.086324] sd 0:0:0:0: [sda] tag#9 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.093289] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.101970] sd 0:0:0:0: [sda] tag#9 CDB: Read(10) 28 00 0b 11 1f 00 00 00 08 00
[85492.101979] blk_update_request: I/O error, dev sda, sector 185671424 op 0x0:(READ) flags 0x3000 phys_seg 1 prio class 0
[85492.104393] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5769748: comm sshd: reading directory lblock 0
[85492.113948] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.123239] sd 0:0:0:0: [sda] tag#10 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.134796] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.144133] sd 0:0:0:0: [sda] tag#10 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85492.144142] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85492.144153] blk_update_request: I/O error, dev sda, sector 1052672 op 0x1:(WRITE) flags 0x800 phys_seg 1 prio class 0
[85492.144160] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.144257] EXT4-fs (sda5): I/O error while writing superblock
[85492.157215] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85492.171693] sd 0:0:0:0: [sda] tag#11 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.178158] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85492.183137] sd 0:0:0:0: [sda] tag#11 CDB: Read(10) 28 00 0a 11 14 08 00 00 08 00
[85492.187959] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5243332: comm bash: reading directory lblock 0
[85492.190626] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85492.199539] sd 0:0:0:0: [sda] tag#12 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.210799] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.220505] sd 0:0:0:0: [sda] tag#12 CDB: Write(10) 2a 00 00 10 10 00 00 00 08 00
[85492.220520] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.220549] EXT4-fs (sda5): I/O error while writing superblock
[85492.232979] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85492.241219] sd 0:0:0:0: [sda] tag#13 FAILED Result: hostbyte=DID_BAD_TARGET driverbyte=DRIVER_OK
[85492.250144] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85492.254295] sd 0:0:0:0: [sda] tag#13 CDB: Read(10) 28 00 07 91 17 b8 00 00 08 00
[85492.254323] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #3934618: comm bash: reading directory lblock 0
[85492.265919] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.275041] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.286661] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.295930] EXT4-fs (sda5): I/O error while writing superblock
[85492.296097] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #4194485: comm bash: reading directory lblock 0
[85492.304523] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.313590] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.320504] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.330168] EXT4-fs (sda5): I/O error while writing superblock
[85492.333020] EXT4-fs warning (device sda5): htree_dirblock_to_tree:997: inode #5243125: lblock 0: comm bash: error -5 reading directory block
[85492.342456] systemd-journald[24482]: Failed to write entry (12 items, 369 bytes), ignoring: Read-only file system
[85492.348007] EXT4-fs warning (device sda5): htree_dirblock_to_tree:997: inode #5243125: lblock 0: comm bash: error -5 reading directory block
[85492.359881] systemd-journald[24482]: Failed to write entry (12 items, 353 bytes), ignoring: Read-only file system
[85492.364065] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5243125: comm bash: reading directory lblock 0
[85492.375449] systemd-journald[24482]: Failed to write entry (9 items, 322 bytes), ignoring: Read-only file system
[85492.384744] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.395550] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.405618] EXT4-fs (sda5): I/O error while writing superblock
[85492.415098] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85492.440286] systemd-journald[24482]: Failed to write entry (12 items, 355 bytes), ignoring: Read-only file system
[85492.450865] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.461553] systemd-journald[24482]: Failed to write entry (9 items, 320 bytes), ignoring: Read-only file system
[85492.472005] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.482769] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.494108] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85492.504091] EXT4-fs warning (device sda5): htree_dirblock_to_tree:997: inode #5507823: lblock 0: comm bash: error -5 reading directory block
[85492.505161] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5507823: comm bash: reading directory lblock 0
[85492.505766] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85492.516426] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.526810] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.533640] EXT4-fs (sda5): I/O error while writing superblock
[85492.544856] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85492.551839] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #5243332: comm bash: reading directory lblock 0
[85492.561523] systemd-journald[24482]: Failed to write entry (12 items, 355 bytes), ignoring: Read-only file system
[85492.570921] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.570953] EXT4-fs (sda5): I/O error while writing superblock
[85492.582675] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.589792] EXT4-fs error (device sda5): __ext4_find_entry:1531: inode #3934618: comm bash: reading directory lblock 0
[85492.595482] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.605207] Buffer I/O error on dev sda5, logical block 0, lost sync page write
[85492.616719] systemd-journald[24482]: Failed to write entry (12 items, 370 bytes), ignoring: Read-only file system
[85492.625714] EXT4-fs (sda5): I/O error while writing superblock
[85492.634024] systemd-journald[24482]: Failed to write entry (12 items, 354 bytes), ignoring: Read-only file system
[85492.659764] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.670180] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.680660] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.691094] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.701497] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.711901] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.722269] systemd-journald[24482]: Failed to write entry (9 items, 343 bytes), ignoring: Read-only file system
[85492.732670] systemd-journald[24482]: Failed to write entry (9 items, 343 bytes), ignoring: Read-only file system
[85492.743071] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.753472] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.763866] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.774434] systemd-journald[24482]: Failed to write entry (9 items, 343 bytes), ignoring: Read-only file system
[85492.784815] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.795215] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.805609] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.816006] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.826401] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.836779] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.847972] systemd-journald[24482]: Failed to write entry (9 items, 321 bytes), ignoring: Read-only file system
[85492.858404] systemd-journald[24482]: Failed to write entry (9 items, 282 bytes), ignoring: Read-only file system
[85492.868808] systemd-journald[24482]: Failed to write entry (9 items, 265 bytes), ignoring: Read-only file system
[85492.880819] systemd-journald[24482]: Failed to write entry (29 items, 720 bytes), ignoring: Read-only file system
[85492.892313] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85492.902982] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85492.913570] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85492.924164] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85492.934729] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85492.945323] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85492.955895] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85492.966478] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85492.977048] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85492.987616] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85492.998202] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.008777] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.019351] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.029920] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.040493] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.051068] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.061581] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.072126] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.082699] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.093281] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.103866] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.114429] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.125106] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.135739] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.146298] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.156875] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.167435] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.177996] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.188570] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.199143] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.209725] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.220404] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.231047] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.241568] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.252134] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.262691] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.273239] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.283816] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.294391] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.304975] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.315646] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.326270] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.336841] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.347416] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.357960] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.368520] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.379095] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.389642] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.400216] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.410764] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.421355] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.431912] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.442479] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.453032] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.463599] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.474211] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.484774] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.495343] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.505907] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.516465] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.527064] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.537631] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.548215] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.558782] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.569341] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.579908] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.590468] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.601041] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.611607] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.622171] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.632752] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.643322] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.653885] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.664449] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.675028] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.685604] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.696175] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.706743] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.717318] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.727881] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.738452] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.749019] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.759604] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.770166] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.780723] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.791291] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.801849] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.812417] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.823025] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.833531] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.844242] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.854846] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.865384] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.875915] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85493.886511] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.897902] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.908663] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.919300] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.929973] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.940638] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.951358] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.961977] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.972648] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85493.983260] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85493.993949] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.004564] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85494.015146] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.025799] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.036451] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.047097] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.057739] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.068391] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.079073] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.089718] systemd-journald[24482]: Failed to write entry (22 items, 742 bytes), ignoring: Read-only file system
[85494.100369] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.111042] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.121563] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.132158] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.142847] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.153408] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.164040] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.174679] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.185393] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.196013] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
[85494.206678] systemd-journald[24482]: Failed to write entry (22 items, 751 bytes), ignoring: Read-only file system
[85494.217332] systemd-journald[24482]: Failed to write entry (22 items, 740 bytes), ignoring: Read-only file system
```

## Solution

Couldn't actually resolve this issue so switched to a different APU device.

I seem to recall I did try and fix by swapping mSata devices to see if that made any difference at all.


