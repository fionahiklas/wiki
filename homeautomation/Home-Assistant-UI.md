# Home Assistant UI

## Overview

Instructions/help on the Home Assistant UI


## Add Card

The best way to add a device appears to be from the "Add Card" button on the 
Overview page.

1. Click on the three dots on the top RH corner
2. Select "Edit Dashboard"
3. Click on "Add Card" in bottom RH of screen
4. Select the "By Entity" tab
5. Search for entity and select all items (on/off, power measurement, etc)


