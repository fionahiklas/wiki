[//]: #TOCStart

[//]: #TOCEnd


## Overview

## Network Environment

To keep things separate and away from the infrastructure subnet the home automation servers will be on a VLAN/subnet which has it's own gateway and DHCP server on the main firewall.

 
## Home Assistant

[Home Assistant](https://www.home-assistant.io) is an Open Source home automation system

### Install HAOS

There is a full-blown OS version of Home Assistant which can be downloaded as an image and copied to SD card to run on a Raspberry Pi 4.

Following these [instructions](https://www.home-assistant.io/installation/raspberrypi) sort of.  Actually trying to write the images using `dd` instead of a GUI tool.

* Looking at [HAOS Github](https://github.com/home-assistant/operating-system|HAOS github)
* Downloading [release 7.1](https://github.com/home-assistant/operating-system/releases/tag/7.1)
* Specifically the file for [RPi 64bit](https://github.com/home-assistant/operating-system/releases/download/7.1/haos_rpi4-7.1.img.xz)
* Copied download file to Linux server, uncompressed using this command

```
xz -d haos_rpi4-7.1.img.xz
```

* Copied image back and then wrote to SD card using the following Mac commands (as root)

```
diskutil list 
diskutil unmountDisk /dev/disk4
dd if=/Users/fiona/Downloads/haos_rpi4-7.1.img of=/dev/rdisk4 bs=4m
```

* Once written inserted into RPi4 and turned power on
* Took a while to boot
* I made a mistake in switch config and the network was effectively blocked
* Plugged in monitor and keyboard and using HA CLI 
* Was able to kick it to get DHCP address in the end, used command `net update eth0 --ipv4-method auto`
* Finally saw DHCP lease

### HAOS Configuration

Went to [http://homeassistant:8123](http://homeassistant:8123) which is the Home Assistant admin page

Looks like it's trying to download Docker images but I suspect it's hitting firewall issues.  I'm seeing blocked packets from the Docker interface which likely won't route properly. 

Seeing these in the logs

```
Jan 12 20:46:37	HOMEAUTO	  172.30.32.3:52394	  1.0.0.1:853	TCP:FPA
Jan 12 20:46:37	HOMEAUTO	  172.30.32.3:48672	  1.1.1.1:853	TCP:FPA
Jan 12 20:46:35	HOMEAUTO	  172.30.32.3:48678	  1.1.1.1:853	TCP:FPA
Jan 12 20:46:35	HOMEAUTO	  172.30.32.3:52392	  1.0.0.1:853	TCP:FPA
```

Trying to understand the [TCP Flags](https://serverfault.com/questions/1008268/what-do-the-following-mean-tcpra-tcpfa-tcppa-tcps-tcpsec)
