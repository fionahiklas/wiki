# GPGMail Issues

## Overview

GPGTools has worked pretty well but GPGMail has been somewhet problematic.

Part of this is due to changes Apple keeps making to the Mail app and the 
extra work that this creates for the 


## GPGMail No Recipient Error

### Issue 

On both my Mac (Studio) and MacBook Air I get this error message

```
Encrypting your message failed because an unknown error has occurred

Unfortunately GPG Mail can't handle the error at this point.

Should your probem persist, please contact us at https://support.gpgtools.org 
with the following error description:

No recipient specified
```

This is presented in a dialog box

![error dialog](images/Screenshot2023-06-21at15.41.03.png)

I could recreate this error with the following steps

* Create an new empty email
* Set the From field to an email address for which the GPG keyring contains a key (private and public)
* Set the To field to an e-mail for which the keyring contains a public key
* Write a test message
* Wait a little while and Mail will try and save a draft 
* Since the default settings ensure that draft emails are encrypted GPGMail attempts this but fails with the error above


### Solution 

* Looking under Mail -> Settings -> GPG Mail (tab)
* This showed a yellow bubble indicating an issue with GPGMail
* Checked under System Settings (gear icon) -> Login Items
* Ensured the following were enabled
  * fixGpgHome
  * GPG Suite
  * shutdown-gpg-agent
* Ran the following command as me

```
launchctl unload /Library/LaunchAgents/org.gpgtools.Libmacgpg.xpc.plist
```

* Got this error

```
launchctl unload /Library/LaunchAgents/org.gpgtools.Libmacgpg.xpc.plist
Unload failed: 5: Input/output error
Try running `launchctl bootout` as root for richer errors.
```

* Ran as root 

```
root# launchctl unload /Library/LaunchAgents/org.gpgtools.Libmacgpg.xpc.plist
Warning: Expecting a LaunchDaemons path since the command was ran as root. Got LaunchAgents instead.
`launchctl bootout` is a recommended alternative.
/Library/LaunchAgents/org.gpgtools.Libmacgpg.xpc.plist: Could not find specified service
Unload failed: 113: Could not find specified service
```

* Tried running this instead as me again

```
launchctl load -w /Library/LaunchAgents/org.gpgtools.Libmacgpg.xpc.plist
```

* This seemed to make a difference
* I'd also run the following as me 

```
/Library/Application\ Support/GPGTools/GPG\ Mail\ Upgrader.app/Contents/MacOS/GPG\ Mail\ Upgrader -FromInstaller YES -Force YES
2023-06-21 15:38:04.577 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] Launched from installer: YES
2023-06-21 15:38:04.578 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] A signed loader bundle that supports the current version of macOS Mail is already enabled. Perfection! Out.
2023-06-21 15:38:04.578 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] Is running the upgrader necessary? NO
2023-06-21 15:38:04.578 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] Did user activate "Never ask again"? NO
2023-06-21 15:38:04.578 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] -Force was specified. Previous requirements are ignore and upgrader is run.
2023-06-21 15:38:04.582 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] Launching Activator
2023-06-21 15:38:11.018 GPG Mail Upgrader[12138:120011] [GPG Mail Upgrade Helper] Should Mail be closed? NO
Move the signed bundle in place
Install loader bundle and activate.
true
```

* After all of this hacking things seem to now be working :D 
* On my laptop all that was needed was the `lunchctl` commands `unload` and `load` as everything else seemed to be fine


## References

### GPGMail

* [GPG Mail upgrader failing](https://gpgtools.tenderapp.com/kb/gpg-mail-faq/gpg-mail-upgrader-fails-to-activate-gpg-mail)
* [What does the GPGMail status bubble mean?](https://gpgtools.tenderapp.com/kb/gpg-mail-faq/what-does-the-status-bubble-in-gpgmail-indicate)
