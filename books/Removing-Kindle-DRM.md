## Overview

I've bought maybe around 100 kindle books and all works well with viewing 
these on phone/tablet apps and on a Kindle eReader.

Problem though: I wanted to look at a different brand of eReader since I was
looking at a) a much bigger screen (I need glasses for reading now) b) possibly 
features for taking notes/annotations on the device.

To my knowledge there is no device that Amazon produces that meets these 
requirements.  So instead I looked at a [Kobo Elipsa](https://uk.kobobooks.com/products/kobo-elipsa)

There is no easy way to get Kindle books onto this device - that is all I 
wanted to do, use the new features on the Elipsa but still have access to 


## Converting Kindle Files to ePub

### Downloading files

I tried using the Kindle desktop application and copying the files from 
there but this presented two problems

1. The files have IDs for names and it's not obvious which book is which
2. It seems some of the files are in AZW format and others KFX

The simplest (though time consuming) approach seems to be to go to the 
[Amazon ebook content][Amazon ebook content] page whcih lists all of your 
Kindle books and for each one select "Download & transfer via USB" from the 
"More actions" menu.

This downloads an `azw3` file with the name the same as the title of the book 
(there is some interesting punctuation and escaping for certain characters).


### Stripping DRM

The `azw3` files seem to be suitable for DRM removal so I took this approach:

```
cd ~/wd/3rdparty
git clone https://github.com/ch33s3w0rm/kindle_dedrm.git
cd kindle_dedrm.git
```

Since I'm paranoid I'm running in a docker container with no network

```
docker run -v $PWD:/home --network none -it python:2.7.18-alpine /bin/sh
```

I copied all the `azw3` files into a `Download` directory under the 
`kindle_dedrm` folder so I could then run a script like this

```
for A in Downloads/*.azw3
do
  echo "$A"
  python kindle_dedrm.py --kindle=<Kindle seial number> "$A"
done
```

The `<Kindle seial number>` for my Kindle is 16 characters long and I just 
copied it from the about dialog, you can also find it on the device I guess.


### Converting to ePub

This step needed to be done on a Linux machine running Ubuntu (version 21.10
I think) having installed the `Calibre` package.  You can, apparently do 
the whole de-drm and conversion from Calibre itself but I just wanted to run
command line tools.

Having copied all the `.nodrm.azw3` files from the De-DRM step to this 
machine I then ran this script:

```
for A in *.nodrm.azw3
do
  newname=`echo "$A" | sed -e "s/.nodrm.azw3/.epub/g"`
  echo "$newname"
  ebook-convert "$A" "$newname"
done
```

This produced ePub files that seemed readable in both iBooks and also 
by the Kobo Elipsa



## References

* [Removing DRM with Calibre](https://www.ucl.ac.uk/slade/know/2979)
* [ebook-convert man page](http://manpages.ubuntu.com/manpages/bionic/man1/ebook-convert.1.html)
* [Read ePub files on Mac using iBooks](https://www.quora.com/How-does-one-read-EPUB-books-on-a-Mac)
* [Removing DRM from Kindle files](https://alexwlchan.net/2019/08/removing-the-drm-from-my-kindle-books/)
* [KFX format not easy to remove](https://news.ycombinator.com/item?id=24273391)


[Amazon ebook content]: https://www.amazon.co.uk/hz/mycd/digital-console/contentlist/booksAll
