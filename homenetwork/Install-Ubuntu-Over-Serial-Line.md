[//]: #TOCStart

[//]: #TOCEnd


## Overview

Setting Ubuntu up on PC Engines APU Single Board Computers is rather challenging!


## Setup

Following [there instructions](https://p5r.uk/blog/2020/instaling-debian-over-serial-console.html)

Used both Mac laptop (Alex) and Linux on Raspberry Pi (Tinkerbell) to get this all to work


### Downloading Legacy Version

From [here](https://cdimage.ubuntu.com/ubuntu-legacy-server/releases/20.04/release/)

Run the following commands (under a shell running as root)

```
diskutil unmountDisk /dev/disk3
dd if=/Users/fiona/Downloads/ubuntu-20.04.1-legacy-server-amd64.iso of=/dev/rdisk3 bs=4m
```

Using a 128Gb Kingston Datatraveller Memory Stick - probably don't need it to be this big

Plugged this into Tinkerbell USB


### Creating new ISO image

Installing the following packages on Tinkerbell

```
apt install xorriso isolinux 
```

Created a directory called `boot-copy-ubuntu` cd into this and run (as root) the following command

```
cp -r /media/pi/Ubuntu-Server\ 20.04.1\ LTS\ amd64 .
```

In the copy edit the files as per the [instructions](https://p5r.uk/blog/2020/instaling-debian-over-serial-console.html) the 
specific contents of the files are shown in the subsections below

Ran the following command to create the new ISO image

```
xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin -partition_offset 16 -A "Ubuntu 20.04" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../ubuntu-20.04-serial-install.iso .
```


The the following commands to write this to USB key (the one I copied the files from in the first place

```
umount /dev/sdb1
dd if=../ubuntu-20.04-serial-install.iso of=/dev/sdb bs=4M
```


#### isolinux.cfg

```
# D-I config version 2.0
# search path for the c32 support libraries (libcom32, libutil etc.)
serial 0 115200
console 0
path 
include menu.cfg
default vesamenu.c32
prompt 0
timeout 300
```


#### txt.cfg

```
default install
label install
  menu label ^Install Ubuntu Server
  kernel /install/vmlinuz
  append  file=/cdrom/preseed/ubuntu-server.seed console=ttyS0,115200n8 vga=788 initrd=/install/initrd.gz quiet --- console=ttyS0,115200n8
label check
  menu label ^Check disc for defects
  kernel /install/vmlinuz
  append   MENU=/bin/cdrom-checker-menu vga=788 initrd=/install/initrd.gz quiet ---
label memtest
  menu label Test ^memory
  kernel /install/mt86plus
label hd
  menu label ^Boot from first hard disk
  localboot 0x80
```


#### adtxt.cfg

```
label expert
  menu hide
  kernel /install/vmlinuz
  append  file=/cdrom/preseed/ubuntu-server.seed priority=low vga=788 initrd=/install/initrd.gz ---
label rescue
  menu label ^Rescue a broken system
  kernel /install/vmlinuz
  append   rescue/enable=true vga=788 console=ttyS0,115200n8 initrd=/install/initrd.gz --- console=ttyS0,115200n8
```


### Connecting to Serial Console

Inserted the USB key into the USB port for the APU 1d device.

```
screen /dev/tty.usbserial-A50298PT 115200
```

See the following

```
AMD G-T40E Processor
CPU MHz=1001
USB MSC blksize=512 sectors=241660916
Press F10 key now for boot menu:
Select boot device:

1. AHCI/0: KINGSTON SMS200S3120G ATA-8 Hard-Disk (111 GiBytes)
2. USB MSC Drive Kingston DataTraveler 3.0 1.00
3. Payload [setup]
4. Payload [memtest]
```

Pressed "2"





## Errors

Saw these 

```
uart_check_keystrokes: error too many bytes are available
uart_check_keystrokes: error too many bytes are available
uart_check_keystrokes: error too many bytes are available
```


## References

Looked through quite a few pages, some may/may not be of any use

* [Live CD Customisation](https://help.ubuntu.com/community/LiveCDCustomization)
* [Installing Debian over serial console](https://p5r.uk/blog/2020/instaling-debian-over-serial-console.html) - this one was alot of use
