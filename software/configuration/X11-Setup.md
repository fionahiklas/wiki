# X11 Setup

## Keyboard layout

For the Logitech MX keys (Mac) the following command worked

```
setxkbmap -layout us -variant mac
```

Also changed Xfce4 settings to only support that layout.


## OpenGL

### Issue with OpenSCAD

I tried to run OpenSCAD on an Ubuntu box and git these errors

```
openscad RaspberryPi5caseV4.scad 
libGL error: No matching fbConfigs or visuals found
libGL error: failed to load driver: swrast
QOpenGLWidget: Failed to create context
QOpenGLWidget: Failed to create context
QOpenGLWidget: Failed to create context
qt.qpa.backingstore: composeAndFlush: QOpenGLContext creation failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
qt.qpa.backingstore: composeAndFlush: makeCurrent() failed
```

Apparently the fix is to run this command, restart XQuartz and try again

```
defaults write org.macosforge.xquartz.X11 enable_iglx -bool true
```

Checked it was set this this command

```
defaults read org.macosforge.xquartz.X11
{
    "enable_iglx" = 1;
}
```

This didn't seem to work so looking into more options

Seems I might have used the incorrect keys, trying this command

```
defaults write org.xquartz.X11 enable_iglx -bool true

defaults read org.xquartz.X11              
{
    "NSWindow Frame SUUpdateAlert" = "1410 784 620 398 0 0 3440 1415 ";
    "NSWindow Frame x11_apps" = "1480 676 454 299 0 0 3440 1415 ";
    "NSWindow Frame x11_prefs" = "1330 570 484 336 0 0 3440 1415 ";
    SUHasLaunchedBefore = 1;
    SULastCheckTime = "2024-07-29 14:16:30 +0000";
    SUSkippedVersion = "2.8.24";
    "app_to_run" = "/opt/X11/bin/xterm";
    "cache_fonts" = 1;
    "done_xinit_check" = 1;
    "enable_iglx" = 1;
    "login_shell" = "/bin/sh";
    "no_auth" = 0;
    "nolisten_tcp" = 1;
    "startx_script" = "/opt/X11/bin/startx -- /opt/X11/bin/Xquartz";
}
```



## References

### OpenGL Rendering

* [Fix for XQuartz](https://unix.stackexchange.com/questions/429760/opengl-rendering-with-x11-forwarding)

