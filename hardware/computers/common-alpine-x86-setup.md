# Common Alpine x86 Setup

## Overview

Common details for the setup of Alpine Linux as a desktop OS on PC hardware


## Initial Installation

### Download Image 

* From the [Alpine download page](https://www.alpinelinux.org/downloads/) download the x86_64 extended ISO image
* Using any image writer tool, write this image to a USB memory stick, e.g. 

```
dd if=alpine-extended-3.21.2-x86_64.iso of=/dev/sdb bs=1M
```

### Boot Into Alpine

* Change BIOS boot so that USB memory stick can be booted
* At the Alpine login prompt enter `root`
* Run the following command

```
setup-alpine
```

* Answer install questions.  Ensure you choose `sys` for type of disk install
* Reboot and login to continue installing packages 



## Setup

### Packages

An incomplete list of packages

```
apk add py3-pip python3-dev emacs direnv kubectl k3s podman xfce4 wireshark thonny \
        texlive-full wireshark slirp4netns screen git emacs-x11 direnv
```

Packages for using OpenPGP and Yubikeys

```
apk add gnupg gpg-agent pinentry-curses-ss pinentry-gtk pinentry-qt
apk add gnupg-scdaemon gnupg-dirmngr yubikey-manager
```

For man pages (which aren't installed by default) and some other 
[basic tools](https://wiki.alpinelinux.org/wiki/Post_installation#Miscellaneous)

```
apk add sed attr dialog bash bash-completion grep util-linux pciutils usbutils binutils findutils readline lsof less nano curl
apk add man-pages mandoc docs
```

Packages to improve look and feel

```
apk add adw-gtk3 adwaita-icon-theme adwaita-xfce-icon-theme xfce4-screensaver
```

This allows `adw-gtk3-dark` to be selected under Appearance -> Style and `Adwaita-Xfce` to
be selected under Appearance -> Icons.


Gaphics packages

```
apk add gimp inkscape leocad kicad libreoffice opencascade 
```

Astronomy

```
apk add kstars stellarium
```

Radio packages

```
apk add gnuradio gqrx rtl-sdr
```

Music

``` 
apk add lilypond audacity lmms tenacity
```

### Secrets Provider

Setting up 



### Shell

* Following instructions to [change my shell](https://wiki.alpinelinux.org/wiki/Change_default_shell) to bash
* Installed shadow and change shell

```
apk add shadow
chsh -s /bin/bash fiona 
```

### Git configuration

* Using the following with appropriate values inserted for `~/.gitconfig`

```
[user]
	name = Fiona Bianchi
	email = fiona@somewhere.over.rainbow
[init]
	defaultBranch = main
[core]
	symlinks = true
	excludesfile = /home/fiona/.gitignore

[commit]
	gpgsign = true
```

* Also this for `~/.gitignore`

```
.idea
*.iml
tmp

# Python environments
.matrix

# Keys
.secret.key

# Version files
.java-version

# MacOS
.DS_Store
```


### GnuPG

* Had to install the above packages for OpenPGP
* Enable and start the `pcscd` service

```
rc-update add pcscd default
rc-service pcscd start
```

* Edit/create the `~/.gnupg/scdaemon.conf` file and add this line

```
disable-ccid
```


### Setting up Podman

* Following the Alpine [wiki](https://wiki.alpinelinux.org/wiki/Podman), run these 
commands as root

```
modprobe tun
echo tun >>/etc/modules
echo fiona:100000:65536 >/etc/subuid
echo fiona:100000:65536 >/etc/subgid
```

For distrobox to work absolutely does need `cgroups-v2` as per 
[these instructions](https://wiki.alpinelinux.org/wiki/OpenRC#cgroups_v2)

* Edit the `/etc/rc.conf` file to uncomment the following line

```
rc_cgroup_mode="unified"
```

* Run the following rc commands to get the relevant services running

```
rc-service cgroups start
rc-update add cgroups
```

I rebooted the machine after hacking the cgroups stuff aswell


### Distrobox

* Install first 

```
apk add distrobox
```


### Bluetooth

* Following instructions [here](https://wiki.alpinelinux.org/wiki/Bluetooth)
* Running this command

```
setup-devd udev
```

* Install 

```
apk add bluez blueman
```

* Add module

```
modprobe btusb
```

* Setting up services

```
rc-service bluetooth start
rc-update add bluetooth default
```

* Also for bluetooth keyboards/mice you need these commands

```
modeprobe uhid
echo "uhid" >> /etc/modules

# Needed to connect all appropriate modules
rc-service bluetooth restart
```




## Notes

### Trying Distrobox

Ran without Podman setup

Ran initially and got this output

```
 distrobox-create --name debian-12 --image debian:12
Image debian:12 not found.
Do you want to pull the image now? [Y/n]: y
WARN[0000] Using cgroups-v1 which is deprecated in favor of cgroups-v2 with Podman v5 and will be removed in a future version. Set environment variable `PODMAN_IGNORE_CGROUPSV1_WARNING` to hide this warning. 
WARN[0000] Using cgroups-v1 which is deprecated in favor of cgroups-v2 with Podman v5 and will be removed in a future version. Set environment variable `PODMAN_IGNORE_CGROUPSV1_WARNING` to hide this warning. 
Resolved "debian" as an alias (/etc/containers/registries.conf.d/00-shortnames.conf)
Trying to pull docker.io/library/debian:12...
Getting image source signatures
Copying blob 8cd46d290033 done   | 
Error: copying system image from manifest list: writing blob: adding layer with blob "sha256:8cd46d290033f265db57fd808ac81c444ec5a5b3f189c3d6d85043b647336913": processing tar file(potentially insufficient UIDs or GIDs available in user namespace (requested 0:42 for /etc/gshadow): Check /etc/subuid and /etc/subgid if configured locally and run "podman system migrate": lchown /etc/gshadow: invalid argument): exit status 1
```

Ran after carrying out podman setup and got this

```
distrobox-create --name debian-12 --image debian:12
Image debian:12 not found.
Do you want to pull the image now? [Y/n]: y
Resolved "debian" as an alias (/etc/containers/registries.conf.d/00-shortnames.conf)
Trying to pull docker.io/library/debian:12...
Getting image source signatures
Copying blob 8cd46d290033 done   | 
Copying config 4fd3f4b75d done   | 
Writing manifest to image destination
4fd3f4b75df372cc4fd60fd5c133c666cf4738ca0973523513f564076395f8ad
Creating 'debian-12' using image debian:12       [ OK ]
Distrobox 'debian-12' successfully created.
To enter, run:

distrobox enter debian-12
```

### GnuPG

Didn't have all the relevant tools installed and got the following

```
gpg --card-edit
gpg: keybox '/home/fiona/.gnupg/pubring.kbx' created

gpg: error getting version from 'scdaemon': No SmartCard daemon
gpg: OpenPGP card not available: No SmartCard daemon

gpg/card> quit
```

Installing `scdaemon` and now get this 

```
gpg --card-edit

gpg: selecting card failed: No such device
gpg: OpenPGP card not available: No such device

gpg/card> 
```



## References

### Alpine

* [Podman setup](https://wiki.alpinelinux.org/wiki/Podman)
* [cgroups-v2 using Lima](https://github.com/lima-vm/lima/discussions/1878)
* [cgroups v2 in alpine](https://wiki.alpinelinux.org/wiki/OpenRC#cgroups_v2)
* [OpenRC commands](https://wiki.alpinelinux.org/wiki/OpenRC)
* [Enable bluetooth keyboard in alpine](https://unix.stackexchange.com/questions/736814/enable-bluetooth-keyboard-for-pinephone-running-postmarketos)

### Podman 

* [Rootless tutorial](https://github.com/containers/podman/blob/main/docs/tutorials/rootless_tutorial.md)
* [crun Runtime](https://github.com/containers/crun)


### Distrobox

* [Distrobox tutorial](https://itsfoss.com/distrobox/)
* [Distrobox useful tips](https://distrobox.it/useful_tips/)


### GnuPG

* [Selecting card failed error](https://stackoverflow.com/questions/69432507/gpg-selecting-card-failed-no-such-device)
* [Yubico GnuPG troubleshooting](https://support.yubico.com/hc/en-us/articles/360013714479-Troubleshooting-Issues-with-GPG)
* [PCSC-Lite Gentoo Page](https://wiki.gentoo.org/wiki/PCSC-Lite)
* [Resolving GPG CCID conflicts](https://support.yubico.com/hc/en-us/articles/4819584884124-Resolving-GPG-s-CCID-conflicts)
* [Pcscd man page](https://linux.die.net/man/8/pcscd)
* [Reader config file](https://linux.die.net/man/5/reader.conf)
* [scdaemon man page](https://linux.die.net/man/1/scdaemon)
* [Mentions about disabling the build in CCID in scdaemon](https://bbs.archlinux.org/viewtopic.php?id=292318)
* [How to force console mode for pin entry](https://superuser.com/questions/520980/how-to-force-gpg-to-use-console-mode-pinentry-to-prompt-for-passwords)

### Linux

* [UHID driver](https://www.kernel.org/doc/html/latest/hid/uhid.html)
* [Bash profile/.bashrc](https://askubuntu.com/questions/29239/where-is-bash-profile)


### Security

* [Using DBus with KeepassXC](https://github.com/keepassxreboot/keepassxc/wiki/Using-DBus-with-KeePassXC)


