# Smart Cards

## Overview

FM4442 ISO7816 Smart cards

## Setup

### Raspberry Pi 

Installing on RPi4B (tinkerbell), the following command installs the daemon and tools


```
apt install pcscd pcsc-tools
```


## Investigation

### Running pcsc_scan

```
pcsc_scan 
Using reader plug'n play mechanism
Scanning present readers...
0: Alcor Micro AU9540 00 00
 
Fri Sep  2 09:33:11 2022
 Reader 0: Alcor Micro AU9540 00 00
  Event number: 0
  Card state: Card inserted, Unresponsive card, 
   
Fri Sep  2 09:33:16 2022
 Reader 0: Alcor Micro AU9540 00 00
  Event number: 1
  Card state: Card removed, 
   
Fri Sep  2 09:33:19 2022
 Reader 0: Alcor Micro AU9540 00 00
  Event number: 2
  Card state: Card inserted, Unresponsive card, 
```


### Running ATR_analysis

```
ATR_analysis '3B A7 00 40 18 80 65 A2 08 01 01 52'
ATR: 3B A7 00 40 18 80 65 A2 08 01 01 52
+ TS = 3B --> Direct Convention
+ T0 = A7, Y(1): 1010, K: 7 (historical bytes)
  TB(1) = 00 --> VPP is not electrically connected
  TD(1) = 40 --> Y(i+1) = 0100, Protocol T = 0 
-----
  TC(2) = 18 --> Work waiting time: 960 x 24 x (Fi/F)
+ Historical bytes: 80 65 A2 08 01 01 52
  Category indicator byte: 80 (compact TLV data object)
    Tag: 6, len: 5 (pre-issuing data)
      Data: A2 08 01 01 52

Possibly identified card (using /usr/share/pcsc/smartcard_list.txt):
3B A7 00 40 18 80 65 A2 08 01 01 52
3B A7 00 40 .. 80 65 A2 08 .. .. ..
	Gemplus GemSAFE Smart Card (8K)
3B A7 00 40 18 80 65 A2 08 01 01 52
	Gemplus GPK8000
	GemSAFE Smart Card (8K)
```

Got the same answer whether a card was inserted or not




## References

### Linux

* [pcscd daemon](https://linux.die.net/man/8/pcscd)
* [reader.conf](https://linux.die.net/man/5/reader.conf)
* [ATR_analysis](https://manpages.debian.org/testing/pcsc-tools/ATR_analysis.1p.en.html)
* [SmartCard Tutorial](https://tldp.org/HOWTO/pdf/Smart-Card-HOWTO.pdf)
* [ATR Commands on cards](https://stackoverflow.com/questions/6289166/atr-command-when-programming-pc-sc-reader)



