# Yubico Keys

## Overview

Instructions for using Yubikeys across multiple machines (Mac and Linux)

## Prerequisites

### MacOS 

#### Yubico Authenticator 

* For MacOS this is available in the App Store and appears to have an Apple Silicon (ARM) version


#### Ykman

* Since the yubikey-manager is Intel only using the command line instead
* Using `brew install ykman` on MacOS
* On Ubuntu-based Linux, `apt install yubikey-manager` installs the command line tools



## Yubikey Initial Setup

### Turn off OTP on touch

The default Yubikey setup for the OTP (One Time Password) will output a code when 
the key is touched.  This can be really annoying as simply touching the key can 
cause the code to be emitted.

Running the following commands does at least make accidental triggering 
(and consequences) less of a pain

```
ykman swap 
ykman otp settings --no-enter 2
```



## OpenPGP Initial Setup

### Create the keys

I already had a secret key created for the email account that I wanted 
to use and copy (move) to the Yubikey


### Publishing the public key to the key server

Run this command 

```
gpg --homedir <homedir> --keyserver keys.openpgp.org --send-keys <short ID>
```


### Importing Keys into Yubikey

* Listing current personal key

```
gpg --list-secret-keys --keyid-format=short
```

* Now run the following to start editing the keys

```
gpg --expert --edit-key <short-id>
```

* Since my subkeys don't include authentication I need to add this
* From the gpg prompt ran `addkey` command
* Selected key type 8 "RSA (set your own capabilities)"
* Pressed `A` then `S` then `E` to get a key that can only Authenticate
* Pressed `Q` to quit that step
* Entered 4096 for the key size
* Set the expiry to 0 for never expires
* Confirmed creation of the key
* Quit gpg and confirmed saving changes

Following these [instructions](https://developers.yubico.com/PGP/Importing_keys.html)

First of all changing the PIN codes on the key

* Ran `gpg --change-pin`
* Followed the prompts to set the user (option 1) and admin (option3) PIN
* The default user PIN is 123456 the default admin is 12345678

Change the reset code

* Run the command `gpg --card-edit`
* Enter `admin` and hit enter
* Enter `passwd` and hit enter
* Enter `4` and hit enter
* Enter the admin PIN then the new reset code twice


Moving the keys into a Yubikey

* Made key backup using this command

```
gpg --export-secret-key --armor <short-id> > backup.key
```

* Ran the following 

```
gpg --expert --edit-key <secret key short-id>
```

* Ran command `keytocard`
* Confirmed move of primary key (select signature key storage)
* Asked for passphrase for PGP
* Asked for Yubikey admin PIN
* Use the `key` command to toggle key's 1 and 2 (encryption and authentication)
  * Enter `key 1` then run `keytocard`
  * Enter `key 1`, `key 2`, thn run `keytocard`
* Enter quit and accept the `Save y/N` by hitting `y`

Following the instructions [here](https://developers.yubico.com/PGP/PGP_Walk-Through.html) to add name, URL, and other details to the card so that OpenPGP on 
other computers can pull the correct information

* Run the `gpg --card-edit` command 
* This will output all the current OpenPGP card settings
* Type `admin` to get the admin commands
* Set the name with `name` command
* Set language, login, and sex 
* For the URL of the public keys search the [Open PGP keys site](https://keys.openpgp.org)
* This should give a URL for the public keys, copy this and use for the value of the `url` command
* You can click on the link from the keys server, download the `.asc` file and view the contents with this command

```
gpg --show-keys <downloaded .asc filename>
```


### Using PGP keys on another machine

#### On Linux

Using Armbian in this case.  Following the instructions [here](https://www.designed-cybersecurity.com/tutorials/openpgp-keys-on-yubikey/) to install all the relevant packages

```
apt install pcscd opensc
apt install pinentry-curses pinentry-gtk2 pinentry-qt
apt install gnupg gpg gpg-agent dirmngr scdaemon
apt install yubikey-manager
```

Following these initial steps

* Inserted the key 
* Ran `gpg --card-edit` and ran the `fetch` command followed by `quit`
* Ran `gpg --edit-key <key identifier>` run `trust` command followed by quit
* Now `gpg --list-secret-keys` shows the secret key



## Copying Key To A New Yubikey

### Setting up temporary GPG directory

* Running the following commands to create a temporary GPG home directory structure

```
mkdir ~/.gnupg-temp
chmod -R go-wrx ~/.gnupg-temp
gpg --homedir ~/.gnupg-temp --list-keys
```

* Importing the backup version of the key 

```
gpg --homedir ~/.gnupg-temp --import /Volumes/KeyTransfer/backup-pgp-export.key
```

* Running the following commands to check that the keys have been imported

```
gpg --homedir ~/.gnupg-temp --list-keys
gpg --homedir ~/.gnupg-temp --list-secret-keys
```

* This should show both public and private keys present

### Setting up the Yubikey with some basic information

* Insert the new Yubikey and setup the key using the following
  * Run the `gpg --card-edit` command 
  * This will output all the current OpenPGP card settings
  * Type `admin` to get the admin commands
  * Set the name with `name` command
  * Set language, login, and sex 
  * For the URL of the public keys search the [Open PGP keys site](https://keys.openpgp.org)
  * This should give a URL for the public keys, copy this and use for the value of the `url` command
  * Exit from `gpg` with the command `quit`

Move the keys to the card 

* Firstly run the following command

```
gpg --homedir ~/.gnupg-temp --expert --edit-key <key ID, can be short or long ID>
```

* Now repeat the process for moving the keys to the card
  * Move the primary key with `keytocard` and select the signature slot


### Switching Yubikeys

* Following [these instructions](https://github.com/drduh/YubiKey-Guide#switching-between-two-or-more-yubikeys)
* Checking the existing setup with no keys plugged in

```
gpg --list-secret-keys
/Users/fiona/.gnupg/pubring.kbx
-------------------------------
sec>  rsa4096 2022-04-26 [SC]
      1234567890123456789012345678901234567890E753
      Card serial no. = 0006 1234567863
uid           [ultimate] Fiona Bianchi <fiona@wibble.com>
ssb>  rsa4096 2022-04-26 [E]
ssb>  rsa4096 2023-06-21 [A]
```

* Plugged in the new Yubikey, ran the command and got the following output

```
gpg-connect-agent "scd serialno" "learn --force" /bye

S SERIALNO 1234567890123456789012345678903670000
OK
S PROGRESS learncard k 0 0
S PROGRESS learncard k 0 0
S PROGRESS learncard k 0 0
OK
```

* Then checked the keys again

```
gpg --list-secret-keys
/Users/fiona/.gnupg/pubring.kbx
-------------------------------
sec>  rsa4096 2022-04-26 [SC]
      1234567890123456789012345678901234567890E753
      Card serial no. = 0006 1234567867
uid           [ultimate] Fiona Bianchi <fiona@wibble.com>
ssb>  rsa4096 2022-04-26 [E]
ssb>  rsa4096 2023-06-21 [A]
```

### Unblock PIN

Run the following command 

```
gpg --card-edit
```

According to [documentation](https://ultrabug.fr/en/Tech%20Blog/2022/2022-07-09-yubikey-full-reset/) 
the `passwd` command should show a menu.  It seems that this doesn't work now.  Instead use 

```
unblock
```

Enter the reset code and you can then set a new user PIN.

Actually seems that the following works (according to [this post](https://forum.yubico.com/viewtopic8810.html?p=4789))

```
admin
password

gpg: OpenPGP card no. DXXXXXXXXXX01000000000000010000 detected

1 - change PIN
2 - unblock PIN
3 - change Admin PIN
4 - set the Reset Code
Q - quit

```






## Notes

### First commands with Yubikey

Plugged in and had to quit the "keyboard helper" dialog

Ran this command

```
ykman info

Device type: YubiKey 5 Nano
Serial number: 123456
Firmware version: 5.x.y
Form factor: Nano (USB-A)
Enabled USB interfaces: OTP, FIDO, CCID

Applications
OTP         	Enabled
FIDO U2F    	Enabled
FIDO2       	Enabled
OATH        	Enabled
PIV         	Enabled
OpenPGP     	Enabled
YubiHSM Auth	Enabled
```

### Yubikey Manager

* Installing from the [downloads page](https://www.yubico.com/support/download/yubikey-manager/)
* For MacOS using the provided package (annoyingly this is Intel only it would seem)
 

### Changing expiry of key

These [notes](https://github.com/drduh/YubiKey-Guide#notes) suggest expiry dates
aren't any use and certainly aren't effective on the primary key.


## References

### Yubikey

* [Install ykman](https://docs.yubico.com/software/yubikey/tools/ykman/Install_ykman.html#download-ykman)
* [Using Yubikey with OpenPGP](https://support.yubico.com/hc/en-us/articles/360013790259-Using-Your-YubiKey-with-OpenPGP)
* [PGP walkthrough](https://developers.yubico.com/PGP/PGP_Walk-Through.html)
* [Importing PGP keys](https://developers.yubico.com/PGP/Importing_keys.html)
* [Reseting OpenPGP](https://support.yubico.com/hc/en-us/articles/360013761339-Resetting-the-OpenPGP-Applet-on-the-YubiKey)
* [Using a Yubikey on a new computer](https://digitalnotions.net/using-yubikey-pgp-keys-on-new-computer/)
* [Switching between two or more Yubikeys](https://github.com/drduh/YubiKey-Guide#switching-between-two-or-more-yubikeys)



### GPG

* [Illustration of gpg subkeys](https://rgoulter.com/blog/posts/programming/2022-06-10-a-visual-explanation-of-gpg-subkeys.html)
* [Edit expiry of a key](https://superuser.com/questions/813421/can-you-extend-the-expiration-date-of-an-already-expired-gpg-key)
* [Expiration on PGP keys](https://security.stackexchange.com/questions/14718/does-openpgp-key-expiration-add-to-security/79386#79386)
* [Using Yubikey with OpenPGP](https://www.designed-cybersecurity.com/tutorials/openpgp-keys-on-yubikey/)
* [Multiple copies of PGP keys](https://forum.yubico.com/viewtopic38a1.html?f=35&t=2400)
* [Changing reset code on the key](https://forum.yubico.com/viewtopic36f2.html?p=8110)
* [Publish a public key](https://askubuntu.com/questions/220063/how-do-i-publish-a-gpg-key)
* [Card not available error](https://stackoverflow.com/questions/69432507/gpg-selecting-card-failed-no-such-device)
* [GNUPG Managing Pins](https://www.gnupg.org/howtos/card-howto/en/ch03s02.html)
* [Unlock Yubikey](https://gist.github.com/dminca/1f8b5d6169c6a6654a95f34a80983218)












