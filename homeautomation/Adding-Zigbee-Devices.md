# Adding Zigbee Devices

## Overview


## Devices

### Samsung SmartThings Smart Plug

This device is a plug with single push button on the top.  According to the instruction leaflet
this can be added by doing the following

1. Plug into a power source
2. Switch power source on
3. Wait until LED on plug blinks red and green
4. On app/hub select add device to pair

Looking in the [Home Assistant ZHA integration page][ha-zha-integration] there is a section on 
[adding new devices][ha-zha-adding-device] which suggests the following steps

1. Go to the Integrations Panel
2. Find Zigbee Home Automation integration
3. Select Configure
4. Click on the "+" (plus) sign at the bottom of the screen to scan for new devices

Following the instructions to switch on the device and then use HA to "Add Device" appeared to 
work.  Took about 30s-1min for it to find, interview, and pair.  Also gave the option to 
rename the device.  Also adding it to an area.  There is no apparent "Done" button, only a 
"Search Again" link.






[ha-zha-integration]: https://www.home-assistant.io/integrations/zha/
[ha-zha-adding-device]: https://www.home-assistant.io/integrations/zha/#adding-devices
