## Overview


## Installation

Using [homebrew](./software:-development:-Homebrew) to install using 

```
brew install qemu
```


## First VM

Following [these instructions](https://gist.github.com/citruz/9896cd6fb63288ac95f81716756cb9aa) to create a VM for ubuntu server

Ran this command to create the disk image

```
qemu-img create -f qcow2 disk01.qcow2 64G
```

Also need to create a file for, I think UEFI data

```
dd if=/dev/zero conv=sync bs=1m count=64 of=ovmf_vars.fd
```

Ran this command to start the VM with the Live install disk connected

```
qemu-system-aarch64 \
    -accel hvf \
    -m 2048 \
    -cpu cortex-a57 -M virt,highmem=off  \
    -drive file=/Users/fiona/tools/homebrew/share/qemu/edk2-aarch64-code.fd,if=pflash,format=raw,readonly=on \
    -drive file=ovmf_vars.fd,if=pflash,format=raw \
    -serial telnet::4444,server,nowait \
    -drive if=none,file=disk01.qcow2,format=qcow2,id=hd0 \
    -device virtio-blk-device,drive=hd0,serial="dummyserial" \
    -device virtio-net-device,netdev=net0 \
    -netdev user,id=net0,hostfwd=tcp::5555-:22 \
    -vga none -device ramfb \
    -cdrom /Users/fiona/Downloads/ubuntu-22.04-live-server-arm64.iso \
    -device usb-ehci -device usb-kbd -device usb-mouse -usb \
    -monitor stdio
```

To carry out the install I was able to connect to the console using `telnet`

```
telnet localhost 4444
```

This seemed to work fairly well and ncurses-based installer seemed to appear and
function correctly.

Once installed the networking over the port forwarding seemed to work fine so I could connect 
to the VM using the command 

```
ssh -Y localhost -p 5555
```

## More details

I've started a [github repo for VMs on M1 Mac](https://github.com/fionahiklas/virtual-machines-on-m1-mac) so 
will add more details there.



