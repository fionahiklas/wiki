# FairPhone 5 e/OS install

## Overview

I wanted to see if it was possible to run an Android phone in a completely de-Googled way.

The phone I purchased was a FairPhone 5, 8Gb RAM, 256Gb storage.

I'm attempting to carry out the install from an Alpine Linux laptop, 
specifically [adorabel](../computers/adorabel-alpine-x86.md)


## /e/OS Installation

Following these [instructions](https://doc.e.foundation/devices/FP5/install)


### Initial Setup

The instructions suggest switching on the phone and setting it up with the FairPhone OS,
logging into Google and making sure that calls, texts and other functions work on the 
phone before trying to flash the new OS.

That all seemed to work.  Even switched on WiFi calling and tried to make a call


### Checking Anti-Rollback

* The instructions mention the anti-rollback feature on the phone ALOT.
* Under Settings -> About Phone clicked on "Android Version"
* This currently shows as 14 
* Android Security update is August 5, 2024


### Checking adb and fastboot on laptop

* Installed using the following commands as root

```
apk add android-tools 
apk add android-udev-rules
```

* Checking the versions of the installed tools

```
adb --version

Android Debug Bridge version 1.0.41
Version 35.0.2-android-tools
Installed as /usr/bin/adb
Running on Linux 6.12.11-0-lts (x86_64)


fastboot --version

fastboot version 35.0.2-android-tools
Installed as /usr/bin/fastboot
```

* Checked the [adb install guide](https://doc.e.foundation/pages/install-adb)
* This just covered downloading or installing 
* Checked at the [android platform tools page](https://developer.android.com/tools/releases/platform-tools) and the latest version matches the ones from Alpine

__NOTE:__ Very important: after installing the new udev rules, need to apply these with the 
following command

```
udevadm control --reload-rules && udevadm trigger
```



### Downloading Files

* Downloading from [FP5 Community](https://images.ecloud.global/community/FP5/)
* Selected Android 14 2.7, this [archive](https://images.ecloud.global/community/FP5/IMG-e-2.7-u-20250110460550-community-FP5.zip)
* Checked the [release notes](https://gitlab.e.foundation/e/os/releases/-/releases)
* For 2.7-u the Android security patches says "December 2024"


### Entering Developer Mode

* Went to Settings -> About Phone
* Tapped on the build number which is on the bottom of the phone 8 times


### Unlocking the Bootloader

* Went to the Fairphone [boot loader unlocking](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone) page
* Got the unlock code and enabled OEM unlocking 

### Enable USB debugging

* Following these [instructions](https://doc.e.foundation/pages/enable-usb-debugging)
* At this point I had lots of hassles with `adb devices` simply hanging and it not recognising the phone
* After alot of hacking around and even trying `adb` (and old 33.x version) on a RPi5 I rebooted my laptop after an `apk upgrade`
* This seemed to do the trick 

```
adb devices -l
* daemon not running; starting now at tcp:5037
* daemon started successfully
List of devices attached
8f9794db               unauthorized usb:2-1 transport_id:1
```


### Rebooting into fastboot

* Running the first command

```
adb reboot bootloader
```

* This rebooted the phone so that is shows the fastboot screen with green "START" at the top
* Ran the next command

```
fastboot flashing unlock
```

* A text message appeared on the phone screen warning about unlocking the boot loader
* Pressed Volume+ to highlight the "Do unlock" option and pressed the power button
* The phone seemed to reboot a few times with warning messages about the boot loader being unlocked
* Eventually it gets to the "Hello" screen
* The `adb devices -l` command didn't show the phone anymore
* To get it to be seen again I had to reboot the phone holding the "power" and "Vol-" buttons
* Got a new menu with smaller text 
* Ran the following command

```
fastboot flashing unlock_critical

< waiting for any device >
```

* The command sat and waited 
* From the small text, on screen menu I needed to select "Reboot to bootloader"
* Now the command completed and the phone asked to confirm to unlock the boot loader again
* Used "Vol+" and then pressed "power"


### Actual Install

* Powered off the phone
* Unzipped the archive

```
cd ~/Downloads
mkdir Android
cd Android
unzip ../IMG-e-2.7-t-20250111460957-official-FP5.zip
```

* Powered on the phone pressing "Vol-" and "Power"
* Booted into fastboot
* Running the command as per the instructions

```
chmod +x flash_FP5_factory.sh && ./flash_FP5_factory.sh
```

* The script started copying over alot of files
* The phone rebooted itself and seems to be running `/e/OS` :D
* Powered it off again and it rebooted!
* Going to reboot into fastboot with "Power" abd "Vol-"

### Locking Bootloader

* With the phone in the fastboot screen

```
fastboot flashing lock_critical
```

* Had to use "Vol+" then press "Power" to accept this
* Phone seemed to reboot a few times and eventually to /e/OS
* Powered off again and rebooted with "Vol-" and "Power" buttons together into fastboot

```
fastboot flashing lock
```

* Had to approve again with "Vol+" then "Power"


## Murena Workspace Setup






## References

### e/OS

* [Fairphone 5 official instructions](https://doc.e.foundation/devices/FP5/install)
* [DIfferences between official, community builds](https://doc.e.foundation/build-status)
* [Install adb](https://doc.e.foundation/pages/install-adb)
* [FairPhone e/OS gitlab](https://gitlab.e.foundation/e/devices/android_device_fairphone_fp5)

### Android 

* [Android platform tools](https://developer.android.com/tools/releases/platform-tools)


### Murena Services

* [Murena.io outage](https://community.e.foundation/t/update-on-murena-io-service-outage/61781)


### F-Droid

* [How to install F-Droid on /e/OS](https://community.e.foundation/t/howto-install-f-droid-app/432)

