## Overview 

Problems related to config on MacOS laptop/desktop


## Apple M1 Mac

Mac Studio, Running Monterey


### Running git clone - implemented in both errors

I just ran `git clone` on a `https` URL and got a load of errors

```
git clone https://github.com/elegooofficial/ELEGOO-Penguin-Bot-V2.0.git
objc[64062]: Class AppleTypeCRetimerRestoreInfoHelper is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01eb0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a04f8). One of the two will be used. Which one is undefined.
objc[64062]: Class AppleTypeCRetimerFirmwareAggregateRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01f00) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a0548). One of the two will be used. Which one is undefined.
objc[64062]: Class AppleTypeCRetimerFirmwareRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01f50) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a0598). One of the two will be used. Which one is undefined.
objc[64062]: Class ATCRTRestoreInfoFTABFile is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01fa0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a05e8). One of the two will be used. Which one is undefined.
objc[64062]: Class AppleTypeCRetimerFirmwareCopier is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01ff0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a0638). One of the two will be used. Which one is undefined.
objc[64062]: Class ATCRTRestoreInfoFTABSubfile is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d02040) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x1042a0688). One of the two will be used. Which one is undefined.
objc[64064]: Class AppleTypeCRetimerRestoreInfoHelper is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01eb0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e404f8). One of the two will be used. Which one is undefined.
objc[64064]: Class AppleTypeCRetimerFirmwareAggregateRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01f00) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e40548). One of the two will be used. Which one is undefined.
objc[64064]: Class AppleTypeCRetimerFirmwareRequestCreator is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01f50) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e40598). One of the two will be used. Which one is undefined.
objc[64064]: Class ATCRTRestoreInfoFTABFile is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01fa0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e405e8). One of the two will be used. Which one is undefined.
objc[64064]: Class AppleTypeCRetimerFirmwareCopier is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d01ff0) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e40638). One of the two will be used. Which one is undefined.
objc[64064]: Class ATCRTRestoreInfoFTABSubfile is implemented in both /usr/lib/libauthinstall.dylib (0x1f7d02040) and /Library/Apple/System/Library/PrivateFrameworks/MobileDevice.framework/Versions/A/MobileDevice (0x107e40688). One of the two will be used. Which one is undefined.
2022-04-25 12:35:16.263 xcodebuild[64064:13599912] Requested but did not find extension point with identifier Xcode.IDEKit.ExtensionSentinelHostApplications for extension Xcode.DebuggerFoundation.AppExtensionHosts.watchOS of plug-in com.apple.dt.IDEWatchSupportCore
2022-04-25 12:35:16.263 xcodebuild[64064:13599912] Requested but did not find extension point with identifier Xcode.IDEKit.ExtensionPointIdentifierToBundleIdentifier for extension Xcode.DebuggerFoundation.AppExtensionToBundleIdentifierMap.watchOS of plug-in com.apple.dt.IDEWatchSupportCore
Cloning into 'ELEGOO-Penguin-Bot-V2.0'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 8 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (8/8), 2.08 MiB | 6.79 MiB/s, done.
```

Ran the second time (slightly different command) and it worked fine

```
git clone https://github.com/elegooofficial/ELEGOO-Penguin-Bot-V2.0.git test2
Cloning into 'test2'...
remote: Enumerating objects: 8, done.
remote: Counting objects: 100% (8/8), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 8 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (8/8), 2.08 MiB | 14.81 MiB/s, done.
```

Looking for solutions

* [Seems this may be due to xcode-select](https://developer.apple.com/forums/thread/698628) or that can certainly be used as a "fix"


### File Vault Enable won't accept passwords

Trying to use the settings gui to enable File Vault.  My user does not 
have admin rights.  Entering the admin user name and password and it's 
rejected.  Tried full name and that didn't work either.

Same problem whether logged in as my standard user or admin user on the machine.

I'm not sure if the problem existed on Monterey or has happened because of the 
upgrade to Ventura.  Searching for more information

* [Issues with file vault and login](https://discussions.apple.com/thread/253478127)
* [Discussion of FileVault on M1 Macs](https://discussions.apple.com/thread/253653178)
* [Information about fdesetup](https://derflounder.wordpress.com/2019/07/03/managing-macos-mojaves-filevault-2-with-fdesetup/)
* [Apple docs mentioning filevault](https://support.apple.com/en-gb/guide/deployment/dep0a2cb7686/web)

Trying to enable as admin 

```
sudo fdesetup enable 
Password:
Enter the user name:myuser
Enter the password for user 'myuser':
2022-11-20 20:19:22.959 fdesetup[5791:108120] set accountPolicies error :Error Domain=com.apple.OpenDirectory Code=4001 "Operation was denied because the current credentials do not have the appropriate privileges." UserInfo={NSLocalizedDescription=Operation was denied because the current credentials do not have the appropriate privileges., NSLocalizedFailureReason=Operation was denied because the current credentials do not have the appropriate privileges.}
Recovery key = '......'
```

Disables again just in case

```
sudo fdesetup disable
Password:
Sorry, try again.
Password:
Enter the user name:myuser
Enter the password for user 'myuser':
2022-11-20 20:26:24.452 fdesetup[5906:112924] set accountPolicies error :Error Domain=com.apple.OpenDirectory Code=4001 "Operation was denied because the current credentials do not have the appropriate privileges." UserInfo={NSLocalizedDescription=Operation was denied because the current credentials do not have the appropriate privileges., NSLocalizedFailureReason=Operation was denied because the current credentials do not have the appropriate privileges.}
FileVault has been disabled.
```

In the end ran the following

```
sudo fdesetup enable -user administrator -usertoadd myuser
Enter the password for user 'administrator':
Enter the password for the added user 'myuser':
Recovery key = '...'
```

This seemed to do the trick.  No offer to save the recovery key to iCloud 
though so that it could be unlocked with Apple ID.

Logged out then back into machine and all seems to be working.

