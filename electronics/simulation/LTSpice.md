## Overview

This is freeware software from [Analog devices](https://www.analog.com/en/index.html)


## Installation

Installers for Windows or Mac can be downloaded from [here](https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html)

### MacOS M1 Mac

According to this [site](https://isapplesiliconready.com/app/LTspice) LTSpice is supported on M1 Mac with [Rosetta 2](https://support.apple.com/en-gb/HT211861) only.

Downloaded version 17.0.42 of the MacOS 10.10 and forward installer package.

Following installer prompts.  It asks for access to Documents folder.  Seems to create an LTSpice folder with examples folder.

Running the program up and all seems relatively fine.  Tried some of the examples and there didn't seem to be any issues.




