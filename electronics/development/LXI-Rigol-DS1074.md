# LXI Setup on Rigol DS1074

## Overview


## References

* [LXI Tools repo](https://github.com/lxi-tools/lxi-tools)
* [LXI Protocols](https://www.lxistandard.org/About/LXI-Protocols.aspx)
* [LXI Discovery tool](https://www.lxistandard.org/Resources/LXIDiscoveryTool.aspx)
* [Multicast DNS](https://en.wikipedia.org/wiki/Multicast_DNS)
* [LXI Wikipedia](https://en.wikipedia.org/wiki/LAN_eXtensions_for_Instrumentation)


