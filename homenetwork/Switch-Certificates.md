
[//]: #TOCStart


## Table of Contents

  - [Overview](#overview)
  - [Quickstart](#quickstart)
  - [Creating Certificates](#creating-certificates)
    - [CA](#ca)
    - [Switch](#switch)
  - [Setup](#setup)
    - [Easyrsa](#easyrsa)
      - [From Homebrew](#from-homebrew)
      - [From Github](#from-github)
  - [Notes](#notes)
    - [OpenSSL](#openssl)


[//]: #TOCEnd


## Overview

Using Easyrsa to create certification authority (CA) and SSL certificates
for home network switches


## Quickstart

* Install `easyrsa`
* Create new directory for CA 
* Create CA PKI 
* Create individual switch directories
* Create switch PKI


## Creating Certificates

### Common Steps

```
cd <base location for certs>
mkdir <Directory for CA or switch/server>
cd <Directory>

easyrsa --pki-dir=$PWD/pki init-pki
```


### CA

Having created the initial directory setup the CA with this command

```
easyrsa --pki-dir=$PWD/pki build-ca
```

Sample output

```
Enter New CA Key Passphrase: 

Confirm New CA Key Passphrase: 
.........+.+.....+.+........+++++++++++++++++++++++++++++++++++++++*........+....+++++++++++++++++++++++++++++++++++++++*.....+....+..+.+............+........+...+.+...+......+.....+......+.+........+......+......+...+.+.....+.+..+......+..................
..+++++++++++++++++++++++++++++++++++++++*...+...+....+++++++++++++++++++++++++++++++++++++++*...................++++++
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:homeNetworkCA

Notice
------
CA creation complete. Your new CA certificate is at:
* /Users/fiona/HomeNetworkCerts/CA/pki/ca.crt
```

Check details with this command

```
easyrsa --pki-dir=$PWD/pki show-ca 
```

This gives output like this

```
Notice
------
Showing details for CA certificate, at:
* /Users/fiona/HomeNetworkCerts/CA/pki/ca.crt


Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            ef:d6:89:19:29:14:74:40:12:34:ab:cd:34:a0:1f:ea::78:12:2f
        Signature Algorithm: sha256WithRSAEncryption
        Issuer:
            commonName                = homeNetworkCA
        Validity
            Not Before: Sep 05 10:52:01 2000 GMT
            Not After : Sep 03 10:52:01 2015 GMT
        Subject:
            commonName                = homeNetworkCA
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:TRUE
            X509v3 Subject Key Identifier: 
                89:0C:14:EF:6B:C3:38:59:37:A1:9A:6B:9E:42:60:64:AC:47:36:0E
            X509v3 Authority Key Identifier: 
                keyid:12:0C:14:EF:6B:56:38:59:CA:34:9A:6B:9E:10:FE:64:AC:47:36:0E
                DirName:/CN=homeNetworkCA
                serial:78:D6:1A:19:29:89:34:67:06:F6:51:8A:74:AB:FF:EA:E9:29:12:2E
            X509v3 Key Usage: 
                Certificate Sign, CRL Sign

```


### Switch/Server








## Setup

### Easyrsa

#### From Homebrew

```
brew install easyrsa
```


#### From Github

This is available on [[https://github.com/OpenVPN/easy-rsa|github]]

Cloned using the following

```
cd ~/wd/3rdparty
git clone https://github.com/OpenVPN/easy-rsa.git
```

Add to the PATH environment variable

```
cd easy-rsa/easyrsa3
export PATH=$PATH:$PWD
```



## Notes

### OpenSSL

While using the github approach for installing `easyrsa` I hit the problems detailed here 
but these shouldn't impact anyone that installs using Homebrew or on a Linux system.

It looks like the upgrade to MacOS Monterey has meant that the SSL library is now [[https://www.libressl.org|libressl]] version `2.8.3` currently (which seems a little old) and this seems to be preventing EasyRSA from working.  Get the following output when trying to build a CA

```
easyrsa init-pki 

init-pki complete; you may now create a CA or requests.
Your newly created PKI dir is: /Volumes/GoogleDrive/Shared drives/HiklasInfrastructure/SwitchCerts/CA/pki


fiona@alex CA % easyrsa build-ca
Using SSL: openssl LibreSSL 2.8.3

Enter New CA Key Passphrase: 
Re-Enter New CA Key Passphrase: 
usage: genrsa [args] [numbits]
 -des            encrypt the generated key with DES in cbc mode
 -des3           encrypt the generated key with DES in ede cbc mode (168 bit key)
 -aes128, -aes192, -aes256
                 encrypt PEM output with cbc aes
 -camellia128, -camellia192, -camellia256
                 encrypt PEM output with cbc camellia
 -out file       output the key to 'file
 -passout arg    output file pass phrase source
 -f4             use F4 (0x10001) for the E value
 -3              use 3 for the E value

Easy-RSA error:

Failed create CA private key
```

Looks like some incompatibility regarding the `genrsa` command.

Going to install OpenSSL to hopefully avoid this issue 

Downloading from [[https://www.openssl.org/source/|OpenSSL site]]

Running the following to build

```
cd ~/wd/3rdparty
tar -zxf ~/Downloads/openssl-1.1.1m.tar.gz
cd openssl-1.1.1m
./config
make
make test
sudo make install
```


## References

### Yubikey 

* [Build Tiny CA Host with Yubikey](https://smallstep.com/blog/build-a-tiny-ca-with-raspberry-pi-yubikey/)
* [Yubikey PKCS11](https://developers.yubico.com/yubico-piv-tool/YKCS11/Supported_applications/openssl_engine.html)

### Easyrsa

* [Github Repo](https://github.com/OpenVPN/easy-rsa)
* [Example CA setup](https://github.com/fionahiklas/enterprise_mongo_ca)


