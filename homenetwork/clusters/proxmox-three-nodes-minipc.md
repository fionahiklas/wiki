# Proxmox Three Node Mini PC Cluster

## Overview

Installing Proxmox on a three node cluster of Asus Mini PCs.  The spec for these is roughly

* Ryzen 7 5800 - 8cores/16threads
* 64Gb RAM (one node has only 32Gb RAM currently)
* 1Tb NVMe drives for the main install
* 1Tb SATA SSD as extra storage
* 1Gbs ethernet 

The nodes are named as follows (IP address in parentheses)

* Gaspode (.20)
* Nobby (.40)
* Colon (.60)


## Setup

### Initial Install

* Downloaded the ISO installer
* Wrote to USB Memory stick using the following command

```
dd if=proxmox-ve_8.3-1.iso of=/dev/sda bs=1M
```

* Inserted memory stick into USB slot
* Had to boot into UEFI BIOS to change boot priority so that the memory stick was picked up
* Followed Graphical Install to get Proxmox installed


### Setting up Cluster

* Following the [instructions](https://www.wundertech.net/how-to-set-up-a-cluster-in-proxmox/)
* Adding each node to Gaspode
* Didn't get the firewall changes done until I had created the cluster
* Making the changes now
  * Using the subnet with `/24` for the source address
  * Adding UDP rules for 5404 and 5405
  * Adding TCP rule for port 22



## References

### Proxmox

* [Downdloads](https://www.proxmox.com/en/downloads/proxmox-virtual-environment/iso)
* [8.3 ISO installer image](https://www.proxmox.com/en/downloads/proxmox-virtual-environment/iso/proxmox-ve-8-3-iso-installer)


### Tutorials

* [How to create a Proxmox cluster](https://www.wundertech.net/how-to-set-up-a-cluster-in-proxmox/)
* [How to install Proxmox nodes](https://www.wundertech.net/how-to-install-proxmox-ve-setup-tutorial/)
