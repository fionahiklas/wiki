# Picoputer

## Overview

Implementation of Transputer on Raspberry Pico


## References

### Software

* [Clustered Pico Transputer article](https://www.theregister.com/2022/05/06/pi_pico_transputer_code/)
* [Emulator on Pico](https://github.com/blackjetrock/picoputer)
* [Original emulator](https://github.com/pahihu/t4)
* [Geekdot Transputer tools](https://www.geekdot.com/category/software/transputer-software/)
* [Helios-NG Cluster OS](https://www.theregister.com/2021/12/06/heliosng/)


### Hardware

* [Turing Pi 2](https://www.theregister.com/2021/12/02/turing_pi_2/)
* [Jetson Nano SDK](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)


### Documentation

* [Transputer assembly language programming](https://www.transputer.net/iset/pdf/transbook.pdf)
* [Transputer handbook](https://www.transputer.net/iset/isbn-013929134-2/tthb.pdf)
* [Inside the transputer](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=a1544508e38404de271fb952e9036e1a9328e8ff)



