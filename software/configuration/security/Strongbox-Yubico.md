# Strongbox and Yubikey

## Overview

Using a Yubikey to unlock KeePass archives


## Commands

Create a base32 encoded passphrase

```
echo "Mypassphrase" | base32
```

Set the OTP value (you will be prompted by the command)

```
ykman otp chalresp --touch 1
```

Test the calculation of a challenge and response

```
ykman otp calculate 1
```


## References

* [Creating Yubikey-protected Strongbox](https://strongboxsafe.com/support/#reamaze#0#/kb/yubikey/how-do-i-create-a-yubikey-protected)
* [ykman otp command](https://docs.yubico.com/software/yubikey/tools/ykman/OTP_Commands.html#ykman-otp-chalresp-options-1-2-key)
* [ykman otp chalresp command](https://fig.io/manual/ykman/otp/chalresp)
* [Using Yubikey with USB-C iPhone/iPad](https://strongbox.reamaze.com/kb/yubikey/what-yubikey-devices-will-work-with-my-ios-device)

