[//]: #TOCStart

[//]: #TOCEnd

<<TableOfContents>>

== Overview ==

Installing UniFi Controller on the machine called `gingy`

== Setup ==

=== Network ===

Deleted the other files under `/etc/netplan` and added this one `/etc/netplan/01-enp1s0.yaml`

{{{{
network:
  version: 2
  renderer: networkd
  ethernets:
    enp1s0:
      dhcp4: no
      addresses: [192.168.123.8/24]
      gateway4: 192.168.123.4
      nameservers:
        addresses: [192.168.123.4]
}}}}


=== System Config ===

Seems that the swapiness value is set to 60 by default, changing this as per [[https://sites.google.com/site/guidetoopensuse/home/lowering-swappiness-and-preparing-a-ssd|instructions]] to 1.
 


=== UniFi Controller Software ===

Following these [[https://help.ui.com/hc/en-us/articles/220066768-UniFi-Network-How-to-Install-and-Update-via-APT-on-Debian-or-Ubuntu|instructions]]

Running the following commands

{{{{
sudo apt-get update && sudo apt-get install ca-certificates apt-transport-https
echo 'deb https://www.ui.com/downloads/unifi/debian stable ubiquiti' | sudo tee /etc/apt/sources.list.d/100-ubnt-unifi.list
sudo wget -O /etc/apt/trusted.gpg.d/unifi-repo.gpg https://dl.ui.com/unifi/unifi-repo.gpg 
}}}}

Holding the Java version at JDK 8

{{{{
sudo apt-mark hold openjdk-11-*
}}}}

Switching to these [[https://gist.github.com/davecoutts/5ccb403c3d90fcf9c8c4b1ea7616948d|instructions]] to ensure that a headless version of JDK gets installed

{{{{
sudo apt update
sudo apt install openjdk-8-jdk-headless unifi
}}}}

=== Controller Config ===

Connecting to [[https://gingy.home.hiklas.com:8443/|Controller URL]] and had to accept the self-signed cert

 * Named the controller "gingy"


== References ==

 * [[https://help.ui.com/hc/en-us/articles/220066768-UniFi-Network-How-to-Install-and-Update-via-APT-on-Debian-or-Ubuntu|Official install docs]]
 * [[https://gist.github.com/davecoutts/5ccb403c3d90fcf9c8c4b1ea7616948d|Gist install script using headless]]
 * [[https://www.oracle.com/java/technologies/java-se-support-roadmap.html|Java version support]]
 * [[https://help.ui.com/hc/en-us/articles/115012240067-UniFi-How-to-Enable-Cloud-Access-for-Remote-Management|Enabling remote access in UniFI]]
 * [[https://github.com/fionahiklas/inside_cctv_setup/blob/0e369a5b8d4414e3646d32334b890af2e3c5aecb/server/etc/netplan/01-enp6s0.yaml|Example netplan file]]
