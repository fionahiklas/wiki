# GPG Setup

## Overview

Setup and tips/tricks for gpg 


## GPG and keys

### Generating new key

* Following these [instructions](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key)
* Running this command

```
gpg --full-generate-key
```

* Selected 9 - Sign and Encrypt
* Accepted default "Curve 25519" 
* Set an expriy time
* Entered my email and name


### Exporting Public Key Block

* For use in key signing the public key block is needed
* Run this command to output it to the terminal

```
gpg --armor --export <email address for the key>
```

* More details in the [man page](https://www.gnupg.org/gph/en/manual/x56.html)


## GPG Agent

### Kill Agent

I found that, deleting the keys while a `gpg-agent` was still running gave me 
[errors](https://unix.stackexchange.com/questions/318385/no-such-file-or-directory-when-generating-a-gpg-key)
when I tried to create new keys.  The solution was to kill the current agent

```
gpgconf --kill gpg-agent
```




## References

### GPG 

* [Generate new key pair](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key)
* [Elliptic Curve algorithm](https://security.stackexchange.com/questions/160311/which-elliptic-curve-cryptography-ecc-algorithm-to-choose-with-gpg)
* [No such file or directory when generating keys](https://unix.stackexchange.com/questions/318385/no-such-file-or-directory-when-generating-a-gpg-key)
* [GPG Manual, exchanging keys](https://www.gnupg.org/gph/en/manual/x56.html)

### GPG Agent

* [Agent configuration](https://www.gnupg.org/gph/en/manual/x56.html)

