# Aggy setup

## Overview

Using a Mele 4C Mini PC with an N100 CPU 



## Initial Install

### Initial Install 

* Downloading Alpine 3.21.0 from [here](https://www.alpinelinux.org/downloads/)
* Wrote to USB memory stick using the following command (this was on a Mac but the Linux version is similar)

```
dd if=alpine-extended-3.21.0-x86_64.iso of=/dev/rdisk4 bs=1M status=progress
```

* Inserted SD card and powered on device
* Drops to login prompt, enter `root` and hit enter
* Run 

```
setup-alpine
```

* Selected `gb` on first keyboard question
* Selected `gb-mac` on second question
* Hostname: `aggy`
* Accepted defaults to configure `eth0` with `dhcp`
* Set simple root password
* Set timezone to `Europe/London`
* Accepted defaults for proxy (none)
* Accepted default for NTP (chrony)
* Accepted default for finding fastest mirror
* Setting up a user called `fiona`
* Accepted default for SSH server setup
* Selected `nvme0n1` disk as `sys`
* Allow partitioning and then enter `reboot`
* Remove USB stick on reboot 


### Post Install

* Edit the `/etc/apk/repositories` file and uncomment the community line
* Install desktop

```
setup-desktop
```

* Choosing XFCE
* Also installing a bunch of packages

```
apk add py3-pip python3-dev emacs kubectl k3s podman xfce4 wireshark thonny \
        texlive-full wireshark slirp4netns screen git emacs-x11 direnv \
		adw-gtk3 adwaita-icon-theme adwaita-xfce-icon-theme xfce4-screensaver \
		sed attr dialog bash bash-completion grep util-linux pciutils \
		usbutils binutils findutils readline lsof less nano curl \
		man-pages mandoc docs gnupg gpg-agent pinentry-curses-ss \
		pinentry-gtk pinentry-qt gnupg-scdaemon gnupg-dirmngr yubikey-manager \
		
```




## Notes

### Install from SD card

This did NOT work - Alpine booted but complained about missing keymap packages




## References

### Alpine

* [Downloads](https://www.alpinelinux.org/downloads/)
* [Setup-alpine](https://docs.alpinelinux.org/user-handbook/0.1a/Installing/setup_alpine.html)
* 

