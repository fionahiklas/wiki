## Overview 

I wanted to be able to add unicode characters for the [notes](./music:-learning:-piano:-Home) I'm taking while learning to play piano.  I started with the following links

* [Unicode Musical Notes and Symbols](https://unicode.org/charts/PDF/U1D100.pdf)
* [Unicode Musical Note Browser Support Test Page](http://www.alanwood.net/unicode/musical_symbols.html)


## Fonts

The musical symbols I need, semibrieve, minim/half note, and crotchet won't 
display in, for example, Character Map on the Mac using the installed fonts.

Safari also failed to show the characters correctly.

Installed Firefox from [download site](https://www.mozilla.org/en-GB/firefox/new/) this still didn't make a difference either.

Had to look for fonts that support the following unicode characters

* 1D15D - Semibrieve: &#x1D15D;
* 1D15E - Minim: &#x1D15E; 
* 1D15F - Crotchet: &#x1D15F;

Found the following information

* [List of Unicode fonts supporting SMP characters](https://en.wikipedia.org/wiki/Unicode_font#List_of_SMP_Unicode_fonts)
* [GNU FreeFont](https://en.wikipedia.org/wiki/GNU_FreeFont) which has an [official site](https://www.gnu.org/software/freefont/) and [download site](https://ftp.gnu.org/gnu/freefont/)
* [GNU Unifont, bitmap font](https://en.wikipedia.org/wiki/GNU_Unifont) which has an [official site](https://savannah.gnu.org/projects/unifont/) 

Downloading and installing the [latest version of GNU FreeFont](https://ftp.gnu.org/gnu/freefont/freefont-ttf-20120503.zip) seemed to help Firefox though and eventually seemed to be pcike dup by Character Map.

Safari still doesn't seem to be using this font to render unicode musical notes.


## References

* [Emacs Unicode character entry](https://www.emacswiki.org/emacs/UnicodeEncoding)
* [Mac Unicode Input](https://eshop.macsales.com/blog/55336-unicode-adding-thousands-of-characters-to-your-mac/)
* [How to use musical symbols on a Mac](https://discussions.apple.com/thread/2801334)
* [How unicode is supported in the Mac](https://www.macworld.com/article/563410/unicode-characters.html)
* [Entering Unicode in Markdown](https://stackoverflow.com/questions/34538879/unicode-in-github-markdown)


