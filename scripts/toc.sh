#!/usr/bin/env bash
#
# -*- mode: shell-script-mode; -*-
#

if [ "$1" == "" ]
then
    echo "Usage: toc.sh <filename>"
    exit 1
fi

script_path=$(dirname $0)
markdown_filename=$1
toc_filename=$(mktemp -t toc)
backup_filename="${markdown_filename}.bak"

echo "Generating TOC for: $markdown_filename"

$script_path/markdown-toc.sh $markdown_filename > $toc_filename

echo "Making backup: $backup_filename"
cp $markdown_filename $backup_filename

echo "Inserting TOC in file"

# So that Perl can pick it up
export toc_filename

perl -s -l012 -e '
$startToc="[//]: #TOCStart";
$endToc="[//]: #TOCEnd";
$startTocRE=qr/\[\/\/\]: \#TOCStart/;
$endTocRE=qr/\[\/\/\]: \#TOCEnd/;
$inBlock=0;
while(<>){
  chomp($_);
  if($_ =~ /^\`\`\`/ ) {
    print;
    $inBlock=!$inBlock; }
  elsif($_=~ $startTocRE){ 
    print $startToc . "\n\n";
    system("cat $ENV{toc_filename}");
    print "\n\n" . $endToc . "\n";
    $inToc=1;}
  elsif($_ =~ $endTocRE){ $inToc=0;}
  elsif(!$inToc){print;}
}' $backup_filename > $markdown_filename

echo "Cleaning up"
rm $toc_filename

echo "Done"

