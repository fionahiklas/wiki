[//]: #TOCStart

[//]: #TOCEnd

<<TableOfContents>>



== Overview ==


== Netgear ==

=== GS105PE ===

PoE powered 

==== Firmware Upgrade ====

Find download from [[https://www.netgear.com/support/product/GS105PE.aspx|GS105PE Product page]], at time of writing the version is '''1.6.0.10''', see [[https://kb.netgear.com/000063990/GS105PE-Firmware-Version-1-6-0-10?article=000063990|release notes]] for more information.

Under Maintenance->Firmware you can put the device into upgrade mode.  Then specify the firmware location as follows

 * TFTP server address `192.168.123.4` 
 * Firmware image name `GS105PE_V1.6.0.10.bin`



=== GS308EPP ===

Two of these, I think they need upgrading due to the DHCP behaviour described in ../WeirdNetGearGs308EppIssues 

NOTE: These MUST use static IP addresses from now on to try and avoid this problem again
