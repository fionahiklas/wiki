# k8s on Vagrant VMs

## Overview 

Following these [instructions](https://devopscube.com/kubernetes-cluster-vagrant/)

Running on AMD Phenom II x6 with 16Gb memory running Ubuntu 22.04


## Steps

* Cloning the repo

```
git clone https://github.com/techiescamp/vagrant-kubeadm-kubernetes
```

* Running to get the VMs up

```
cd vagrant-kubeadm-kubernetes
vagrant up
```

* Failed with this output

```
Bringing machine 'master' up with 'virtualbox' provider...
Bringing machine 'node01' up with 'virtualbox' provider...
Bringing machine 'node02' up with 'virtualbox' provider...
==> master: Box 'bento/ubuntu-22.04' could not be found. Attempting to find and install...
    master: Box Provider: virtualbox
    master: Box Version: >= 0
==> master: Loading metadata for box 'bento/ubuntu-22.04'
    master: URL: https://vagrantcloud.com/bento/ubuntu-22.04
==> master: Adding box 'bento/ubuntu-22.04' (v202206.13.0) for provider: virtualbox
    master: Downloading: https://vagrantcloud.com/bento/boxes/ubuntu-22.04/versions/202206.13.0/providers/virtualbox.box
==> master: Successfully added box 'bento/ubuntu-22.04' (v202206.13.0) for 'virtualbox'!
==> master: Importing base box 'bento/ubuntu-22.04'...
==> master: Matching MAC address for NAT networking...
==> master: Checking if box 'bento/ubuntu-22.04' version '202206.13.0' is up to date...
==> master: Setting the name of the VM: vagrant-kubeadm-kubernetes_master_1663680166636_25206
==> master: Clearing any previously set network interfaces...
The IP address configured for the host-only network is not within the
allowed ranges. Please update the address used to be within the allowed
ranges and run the command again.

  Address: 10.0.0.10
  Ranges: 192.168.56.0/21

Valid ranges can be modified in the /etc/vbox/networks.conf file. For
more information including valid format see:

  https://www.virtualbox.org/manual/ch06.html#network_hostonly
```

* Attempting to fix this following [instructions](https://www.virtualbox.org/manual/ch06.html) 
  in the host-only networking section it suggests creating `/etc/vbox/networks.conf` with this contents

```
* 10.0.0.0/8
* 10.0.0.0/24
```

__NOTE:__ The leading `*` characters are required

* This seemed to do the trick 
* Everything started up and ran checks 

```
vagrant ssh master
kubectl get pods -n kube-system
```

## Remote Access

Accessing server from MacBook

* Setup config

```
scp fiona@linuxserver:wd/3rdparty/vagrant-kubeadm-kubernetes/configs/config ~/.kube/config-linuxserver
```

* It's possible to [merge kube configs](https://medium.com/@jacobtomlinson/how-to-merge-kubernetes-kubectl-config-files-737b61bd517d) or just use `KUBECONFIG` environment variable as below
* Redirect port 

```
ssh -D 12345 fiona@linuxserver
```

* In a separate terminal connect using kubectl 
* Information on proxying from [this post](https://www.reddit.com/r/kubernetes/comments/avonc2/how_to_connect_to_a_remote_kubernetes_cluster/)

```
export KUBECONFIG=$HOME/.kube/config:$HOME/.kube/config-linuxserver
export https_proxy=socks5://localhost:12345
kubctx kubernetes-admin@kubernetes
kubectl get namespaces
```

* The output should be like this

```
NAME                   STATUS   AGE
default                Active   4h32m
kube-node-lease        Active   4h32m
kube-public            Active   4h32m
kube-system            Active   4h32m
kubernetes-dashboard   Active   4h32m
```

* Running `kubectl proxy` in a terminal listens on `127.0.0.1:8001` and allows connection to the Dashboard
* Connect using this URL

```
http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#/login
```

* The token is stored under in `configs/token` under the cloned directory for the project

 


