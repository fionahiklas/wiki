# RAID NAS Setup

## Overview

Setup for RAID NAS server


## Config files

### /etc/mdadm/mdadm.conf

```
# mdadm.conf
#
# !NB! Run update-initramfs -u after updating this file.
# !NB! This will ensure that initramfs has an uptodate copy.
#
# Please refer to mdadm.conf(5) for information about this file.
#

# by default (built-in), scan all partitions (/proc/partitions) and all
# containers for MD superblocks. alternatively, specify devices to scan, using
# wildcards if desired.
#DEVICE partitions containers

# automatically tag new arrays as belonging to the local system
HOMEHOST <system>

# instruct the monitoring daemon where to send mail alerts
MAILADDR root

# definitions of existing MD arrays

# This configuration was auto-generated on Wed, 15 Dec 2021 21:09:39 +0000 by mkconf
```


## Configuration Commands 

### Transferring Drives to another system

```
cat /proc/mdstat 
mkdir /mnt/md0
mount /dev/md/shrek\:0 /mnt/md0/
ls /mnt/md0/
```

### Debugging Commands

```
cat /proc/mdstat 
dmesg
lsblk
dmesg
lsblk
dmesg
smartctl -a /dev/sdc
dmesg
lsusb
```


