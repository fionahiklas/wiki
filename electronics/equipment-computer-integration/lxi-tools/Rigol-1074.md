# Rigol 1074

## Overview

Using LXI tools on Linux to capture a screenshot of the scope display at first.
Will try and expand with more details of other commands.


## Setup

### Network

#### Linux

On my Raspberry Pi I needed to add an input rule to allow the screenshot RPC to work

```
iptables -I INPUT -p tcp -m tcp --sport 618 -m state --state NEW,ESTABLISHED -j ACCEPT
```

## Commands

### Take screenshot

Running as a normal user on my Raspberry Pi machine, this command captures a screenshot

```
lxi screenshot -a 192.168.143.181 -p rigol-1000z 
```


## References

* [Rigol 1000Z-E Series Product Page](https://www.rigol.eu/products/oscillosopes/DS1000Z-E%20series.html)
* [Rigol 1000Z Support Page](https://www.rigol.eu/En/Index/listView/catid/27/tp/1/wd/DS1000)
* [Rigol 1000Z User Manual](https://www.rigol.eu/Public/Uploads/uploadfile/files/ftp/DS/DS1000Z-E_UserGuide_EN.pdf)
* [About LXI tools with telnet to Rigol example](https://www.testandmeasurementtips.com/basics-of-lan-extensions-for-instrumentation-lxi-and-scpi-faq/)
