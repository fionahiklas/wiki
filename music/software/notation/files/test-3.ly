{
  \time 2/4
  \clef treble
  c' d' e' f' g' a' b' c'' d'' e''
}

{
  \time 2/4
  \clef bass
  a, b, c d e f g a b c'
}

