[//]: #TOCStart

[//]: #TOCEnd

## Overview

Details about the home network setup


## Setup

### Security

* [SwitchCertificates](./homenetwork:-Switch-Certificates)


### Monitoring

* [Prometheus Setup](./homenetwork:-Prometheus-Setup)
* [Grafana Loki Setup](./homenetwork:-Grafana-Loki-Setup)
* [Grafana Setup](./homenetwork:-Grafana-Setup)


### PC Engines

* [Install Ubuntu Over Serial Line](./homenetwork:-Install-Ubuntu-Over-Serial-Line)
* [Apu1d Failed To Write Entry Errors](./homenetwork:-Apu1d-Failed-To-Write-Entry-Errors)


### Ubiquiti

* [Research And Information](./homenetwork-Research-and-Information]
* [UniFi Controller Installation](./homenetwork:-UniFi-Controller-Installation]
* [UniFi Controller Setup](./homenetwork:-UniFi-Controller-Setup]
* [UniFi Wifi Setup](./homenetwork:-UniFiWifiSetup)

## Hardware

* [Wireless Access Points Hardware](./homenetwork:-Wireless-Access-Point-Hardware)
* [Switch Hardware](./homenetwork:-Switch-Hardware)


## Information

### Networking

* [NetworkTestingCommands](./homenetwork:-Network-Testing-Commands)
* [RaspberryPiNetworking](./homenetwork:-Raspberry-Pi-Networking)


## Troubleshooting

* [OpenVPN Certificate Error](./homenetwork:-troubleshooting:-OpenVPN-Certificate-Error)
