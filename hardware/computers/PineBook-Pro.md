# PineBook Pro

## Overview

2022 [PineBook Pro](https://www.pine64.org/pinebook-pro/) device purchased direct from [Pine64](https://www.pine64.org)

This is actually a US keyboard model but that actually works out better as the layout is similar to 
the Mac Keyboard that I'm more use to.  Also, frankly, I don't use the GBP symbol.


## Initial Setup

Originally this came with Manjaro Linux pre-installed.  The desktop is KDE which I'm not really a fan of
and I also wanted to install a clean OS from scratch.

Downloading the latest [Manjaro ARM, PineBook Pro image](https://github.com/manjaro-arm/pbpro-images),
specifically this is [release 22.06](https://github.com/manjaro-arm/pbpro-images/releases/tag/22.06) and
picked the [XFCE image](https://github.com/manjaro-arm/pbpro-images/releases/download/22.06/Manjaro-ARM-xfce-pbpro-22.06.img.xz) 

* Trying a naive install/setup of this by running the following

```
cd Download
xz -d Manjaro-ARM-xfce-pbpro-22.06.img.xz
sudo dd if=Manjaro-ARM-xfce-pbpro-22.06.img.xz of=/dev/mmcblk1 bs=1M
```

* Rebooting the machine. 
* Seemed to do something and shows the Manjaro green M icon, then cursor that didn't blink and then blank screen
* The took a few minutes and eventually the desktop appeared.
* Appeared to be running a little slow with lots of hourglass mouse cursors showing
* Setup dialog appeared, selected locale, keyboard, users
* Now following the solution [here](https://forum.manjaro.org/t/installing-manjaro-from-sd-to-emmc-in-pinebook/25356) for install on eMMC


## External Monitor

Trying to connect over HDMI with a Uni 8-in-1 dock, I think this is the 
[hub device](https://uniaccessories.com/products/usb-c_8in1_hub)

Found a [USB-C breakout module](https://shop.pimoroni.com/products/usb-type-c-plug-breakout-usb-3-0?variant=32341340979283)
which mentions a [USB-C application note](http://ww1.microchip.com/downloads/en/appnotes/00001953a.pdf)

This page talks about the [USB-C Billboard device](http://www.technical-direct.com/en/typec_billboard/)

There is a section on [USB-C Alternate Mode devices](https://wiki.pine64.org/wiki/Pinebook_Pro_Hardware_Accessory_Compatibility#USB_C_alternate_mode_DP) on the PineBook Pro Wiki.

Ordered the [UGreen 6-in-1 70410 hub](https://www.amazon.co.uk/gp/product/B07RRPS5NT/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

Looks like this will never work in Manjaro because of a missing [patch](https://forum.manjaro.org/t/pinebook-pro-type-c-dp-alt-mode-is-missing-a-patch/120925)

Pine64 [wiki page about supported DP-Alt mode](https://wiki.pine64.org/wiki/Pinebook_Pro_Hardware_Accessory_Compatibility#USB_C_alternate_mode_DP)


## Alternative OS

### Armbian

* DP altmode armbian [kernel patch](https://github.com/armbian/build/blob/master/patch/kernel/archive/rockchip64-5.19/board-pbp-add-dp-alt-mode.patch)
* Kernel [fork for Pinebook Pro and other devices](https://github.com/megous/linux)
* Explanatory [post](https://forum.manjaro.org/t/pinebook-pro-type-c-dp-alt-mode-is-missing-a-patch/120925/3) about Manjaro using mainline which doesn't include DP-Alt patch



## References

* [Dual-screen Pinebook Pro](https://www.pine64.org/2021/03/19/dual-screen-pinebook-pro/)
* [Pinebook Pro Debian Installer](https://wiki.pine64.org/index.php/Pinebook_Pro_Debian_Installer)

