# Candle on Linux

## Overview


## Compiling on Ubuntu

Following some of the notes [here](https://github.com/Denvi/Candle/wiki/Compiling-for-Raspberry-Pi-4)

Installing packages

```
apt install qtcreator qt5-qmake libqt5serialport5-dev
```



## References


### Candle 

* [Github Repo](https://github.com/Denvi/Candle)
* [Candle on Rpi4](https://github.com/pihnat/rpi-Candle)
* [Candle 2](https://github.com/Schildkroet/Candle2)
* [Candle 1.2b on Linux](https://adrian.siemieniak.net/portal/candle-1-2b-for-linux/)


### GRBL

* [GRBL github repo](https://github.com/gnea/grbl)


### UGS

* [Universal GCode Sender](http://winder.github.io/ugs_website/)
* [UGS repo](https://github.com/winder/Universal-G-Code-Sender)

