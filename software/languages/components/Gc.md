# Garbage Collection

## Pauses in GC

### JDK GC

* [Post about < 1ms GC in JDK >= 16](https://stackoverflow.com/questions/40318257/why-go-can-lower-gc-pauses-to-sub-1ms-and-jvm-has-not)


### Go 

* [Is Go GC slowing your program](https://medium.com/@sivssdn/is-your-garbage-collector-making-program-slow-go-lang-293a23b4c8d9)
* [Unexpected GC pauses](http://big-elephants.com/2018-09/unexpected-gc-pauses/)
* [Go lang pause due to GC, search](https://www.google.com/search?client=safari&rls=en&q=Go+lang+pause+due+to+garbage+collection&ie=UTF-8&oe=UTF-8)
* [Optimising the Go GC](https://concertio.com/blog/optimizing-the-go-garbage-collector-and-concurrency/)

