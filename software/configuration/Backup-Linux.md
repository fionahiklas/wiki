Command that deja-dup runs

```
/usr/bin/python3.9 /usr/bin/duplicity \
--include=/home/fiona/.cache/deja-dup/metadata \ 
--exclude=/home/fiona/snap/*/*/.cache \
--exclude=/home/fiona/.var/app/*/cache \
--exclude=/home/fiona/Downloads \
--exclude=/home/fiona/.cache/deja-dup/tmp \
--exclude=/home/fiona/.xsession-errors \
--exclude=/home/fiona/.cache/deja-dup \
--exclude=/home/fiona/.cache \
--include=/home/fiona \
--exclude=/sys \
--exclude=/run \
--exclude=/proc \
--exclude=/dev \
--exclude=/var/tmp \
--exclude=/tmp \
--exclude=/media/fiona/498da858-3c10-430a-9e02-09c52731000d1/florrie \
--exclude=** \
--exclude-if-present=CACHEDIR.TAG \
--exclude-if-present=.deja-dup-ignore \
--dry-run --volsize=50 / gio+file:///media/fiona/498da858-3c10-430a-9e02-09c52731000d1/florrie \
--no-encryption --verbosity=9 --timeout=120 \
--archive-dir=/home/fiona/.cache/deja-dup \
--tempdir=/home/fiona/.cache/deja-dup/tmp --log-fd=20
```

Getting [Déja Dup settings](https://askubuntu.com/questions/145698/where-does-deja-dup-keep-its-config-files)
