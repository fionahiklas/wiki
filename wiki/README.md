# Wiki

## Overview

This is an attempt at a "wiki" for me to dump all sorts of notes into a central place.

I've had the following problems with the git repo wiki options

1. Github and Gitea don't seem to allow pages in nested directories
2. I've tried keeping a github and gitea copy in sync but the naming for individual files isn't consistent
3. Really I don't need a wiki per se, just a repo of markdown files is fine

A minor niggle is that the repos for the Wiki's uses "master" instead of "main".



## Notes

### Convert from Home.md to README.md

Ran this to rename the files

```
find . -name Home.md -print | xargs -I{} sh -c '(pathname=`dirname {}` ; echo "moving {} $pathname/README.md" ; mv {} $pathname/README.md )'
```

