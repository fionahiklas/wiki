# OpenVPN Certificate Error

## Overview

* Using OpenVPN server on pfSense
* Using OpenVPN iOS client, version 3.3.0

Getting failures on client with these logs

```
92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:12:12 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:12:12 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:12:16 OS Event: SLEEP

2022-08-18 11:12:16 EVENT: PAUSE

2022-08-18 11:12:46 OS Event: WAKEUP

2022-08-18 11:12:49 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:12:49 STANDARD RESUME

2022-08-18 11:12:49 EVENT: RESUME

2022-08-18 11:12:49 EVENT: RECONNECTING

2022-08-18 11:12:49 EVENT: RESOLVE

2022-08-18 11:12:49 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:12:49 EVENT: WAIT

2022-08-18 11:12:49 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:12:49 EVENT: CONNECTING

2022-08-18 11:12:49 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:12:49 Creds: Username/Password

2022-08-18 11:12:49 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:12:49 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:12:49 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:12:54 OS Event: SLEEP

2022-08-18 11:12:54 EVENT: PAUSE

2022-08-18 11:13:15 OS Event: WAKEUP

2022-08-18 11:13:18 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:13:18 STANDARD RESUME

2022-08-18 11:13:18 EVENT: RESUME

2022-08-18 11:13:18 EVENT: RECONNECTING

2022-08-18 11:13:18 EVENT: RESOLVE

2022-08-18 11:13:18 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:13:18 EVENT: WAIT

2022-08-18 11:13:18 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:13:18 EVENT: CONNECTING

2022-08-18 11:13:18 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:13:18 Creds: Username/Password

2022-08-18 11:13:18 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:13:19 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:13:19 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:13:29 OS Event: SLEEP

2022-08-18 11:13:29 EVENT: PAUSE

2022-08-18 11:13:37 OS Event: WAKEUP

2022-08-18 11:13:40 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:13:40 STANDARD RESUME

2022-08-18 11:13:40 EVENT: RESUME

2022-08-18 11:13:40 EVENT: RECONNECTING

2022-08-18 11:13:40 EVENT: RESOLVE

2022-08-18 11:13:40 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:13:40 EVENT: WAIT

2022-08-18 11:13:40 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:13:40 EVENT: CONNECTING

2022-08-18 11:13:40 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:13:40 Creds: Username/Password

2022-08-18 11:13:40 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:13:40 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:13:40 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:13:53 OS Event: SLEEP

2022-08-18 11:13:53 EVENT: PAUSE

2022-08-18 11:14:19 OS Event: WAKEUP

2022-08-18 11:14:22 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:14:22 STANDARD RESUME

2022-08-18 11:14:22 EVENT: RESUME

2022-08-18 11:14:22 EVENT: RECONNECTING

2022-08-18 11:14:22 EVENT: RESOLVE

2022-08-18 11:14:22 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:14:22 EVENT: WAIT

2022-08-18 11:14:22 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:14:22 EVENT: CONNECTING

2022-08-18 11:14:22 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:14:22 Creds: Username/Password

2022-08-18 11:14:22 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:14:22 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:14:22 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:14:27 OS Event: SLEEP

2022-08-18 11:14:27 EVENT: PAUSE

2022-08-18 11:14:28 OS Event: WAKEUP

2022-08-18 11:14:31 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:14:31 STANDARD RESUME

2022-08-18 11:14:31 EVENT: RESUME

2022-08-18 11:14:31 EVENT: RECONNECTING

2022-08-18 11:14:31 EVENT: RESOLVE

2022-08-18 11:14:31 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:14:31 EVENT: WAIT

2022-08-18 11:14:31 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:14:31 EVENT: CONNECTING

2022-08-18 11:14:31 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:14:31 Creds: Username/Password

2022-08-18 11:14:31 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:14:32 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:14:32 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:14:47 OS Event: SLEEP

2022-08-18 11:14:47 EVENT: PAUSE

2022-08-18 11:14:47 OS Event: WAKEUP

2022-08-18 11:14:50 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:14:50 STANDARD RESUME

2022-08-18 11:14:50 EVENT: RESUME

2022-08-18 11:14:51 EVENT: RECONNECTING

2022-08-18 11:14:51 EVENT: RESOLVE

2022-08-18 11:14:51 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:14:51 EVENT: WAIT

2022-08-18 11:14:51 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:14:51 EVENT: CONNECTING

2022-08-18 11:14:51 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:14:51 Creds: Username/Password

2022-08-18 11:14:51 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:14:51 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:14:51 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:14:56 OS Event: SLEEP

2022-08-18 11:14:56 EVENT: PAUSE

2022-08-18 11:15:21 OS Event: WAKEUP

2022-08-18 11:15:24 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:15:24 STANDARD RESUME

2022-08-18 11:15:24 EVENT: RESUME

2022-08-18 11:15:24 EVENT: RECONNECTING

2022-08-18 11:15:24 EVENT: RESOLVE

2022-08-18 11:15:24 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:15:24 EVENT: WAIT

2022-08-18 11:15:24 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:15:24 EVENT: CONNECTING

2022-08-18 11:15:24 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:15:24 Creds: Username/Password

2022-08-18 11:15:24 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:15:24 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:15:24 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:15:29 OS Event: SLEEP

2022-08-18 11:15:29 EVENT: PAUSE

2022-08-18 11:15:52 OS Event: WAKEUP

2022-08-18 11:15:55 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:15:55 STANDARD RESUME

2022-08-18 11:15:55 EVENT: RESUME

2022-08-18 11:15:55 EVENT: RECONNECTING

2022-08-18 11:15:55 EVENT: RESOLVE

2022-08-18 11:15:55 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:15:55 EVENT: WAIT

2022-08-18 11:15:55 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:15:55 EVENT: CONNECTING

2022-08-18 11:15:55 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:15:55 Creds: Username/Password

2022-08-18 11:15:55 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:15:55 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:15:55 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:16:00 OS Event: SLEEP

2022-08-18 11:16:00 EVENT: PAUSE

2022-08-18 11:16:23 OS Event: WAKEUP

2022-08-18 11:16:26 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:16:26 STANDARD RESUME

2022-08-18 11:16:26 EVENT: RESUME

2022-08-18 11:16:26 EVENT: RECONNECTING

2022-08-18 11:16:26 EVENT: RESOLVE

2022-08-18 11:16:26 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:16:26 EVENT: WAIT

2022-08-18 11:16:26 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:16:26 EVENT: CONNECTING

2022-08-18 11:16:26 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:16:26 Creds: Username/Password

2022-08-18 11:16:26 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:16:26 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:16:26 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:17:06 Session invalidated: KEEPALIVE_TIMEOUT

2022-08-18 11:17:06 Client terminated, restarting in 2000 ms...

2022-08-18 11:17:08 EVENT: RECONNECTING

2022-08-18 11:17:08 EVENT: RESOLVE

2022-08-18 11:17:08 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:17:08 EVENT: WAIT

2022-08-18 11:17:08 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:17:08 EVENT: CONNECTING

2022-08-18 11:17:08 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:17:08 Creds: Username/Password

2022-08-18 11:17:08 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:17:08 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:17:08 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:17:10 OS Event: SLEEP

2022-08-18 11:17:10 EVENT: PAUSE

2022-08-18 11:17:11 OS Event: WAKEUP

2022-08-18 11:17:14 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:17:14 STANDARD RESUME

2022-08-18 11:17:14 EVENT: RESUME

2022-08-18 11:17:14 EVENT: RECONNECTING

2022-08-18 11:17:14 EVENT: RESOLVE

2022-08-18 11:17:14 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:17:14 EVENT: WAIT

2022-08-18 11:17:14 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:17:14 EVENT: CONNECTING

2022-08-18 11:17:14 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:17:14 Creds: Username/Password

2022-08-18 11:17:14 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:17:14 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:17:14 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:17:19 OS Event: SLEEP

2022-08-18 11:17:19 EVENT: PAUSE

2022-08-18 11:17:25 OS Event: WAKEUP

2022-08-18 11:17:28 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:17:28 STANDARD RESUME

2022-08-18 11:17:28 EVENT: RESUME

2022-08-18 11:17:28 EVENT: RECONNECTING

2022-08-18 11:17:28 EVENT: RESOLVE

2022-08-18 11:17:28 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:17:28 EVENT: WAIT

2022-08-18 11:17:28 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:17:28 EVENT: CONNECTING

2022-08-18 11:17:28 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:17:28 Creds: Username/Password

2022-08-18 11:17:28 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:17:28 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:17:28 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:17:33 OS Event: SLEEP

2022-08-18 11:17:33 EVENT: PAUSE

2022-08-18 11:17:37 OS Event: WAKEUP

2022-08-18 11:17:40 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:17:40 STANDARD RESUME

2022-08-18 11:17:40 EVENT: RESUME

2022-08-18 11:17:40 EVENT: RECONNECTING

2022-08-18 11:17:40 EVENT: RESOLVE

2022-08-18 11:17:40 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:17:40 EVENT: WAIT

2022-08-18 11:17:40 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:17:40 EVENT: CONNECTING

2022-08-18 11:17:40 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:17:40 Creds: Username/Password

2022-08-18 11:17:40 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:17:40 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:17:40 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:18:04 OS Event: SLEEP

2022-08-18 11:18:04 EVENT: PAUSE

2022-08-18 11:18:27 OS Event: WAKEUP

2022-08-18 11:18:30 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:18:30 STANDARD RESUME

2022-08-18 11:18:30 EVENT: RESUME

2022-08-18 11:18:30 EVENT: RECONNECTING

2022-08-18 11:18:30 EVENT: RESOLVE

2022-08-18 11:18:30 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:18:30 EVENT: WAIT

2022-08-18 11:18:30 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:18:30 EVENT: CONNECTING

2022-08-18 11:18:30 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:18:30 Creds: Username/Password

2022-08-18 11:18:30 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:18:30 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:18:30 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:18:45 OS Event: SLEEP

2022-08-18 11:18:45 EVENT: PAUSE

2022-08-18 11:18:58 OS Event: WAKEUP

2022-08-18 11:19:01 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:19:01 STANDARD RESUME

2022-08-18 11:19:01 EVENT: RESUME

2022-08-18 11:19:01 EVENT: RECONNECTING

2022-08-18 11:19:01 EVENT: RESOLVE

2022-08-18 11:19:01 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:19:01 EVENT: WAIT

2022-08-18 11:19:01 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:19:01 EVENT: CONNECTING

2022-08-18 11:19:01 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:19:01 Creds: Username/Password

2022-08-18 11:19:01 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:19:01 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:19:01 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:19:06 OS Event: SLEEP

2022-08-18 11:19:06 EVENT: PAUSE

2022-08-18 11:19:06 OS Event: WAKEUP

2022-08-18 11:19:09 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:19:09 STANDARD RESUME

2022-08-18 11:19:09 EVENT: RESUME

2022-08-18 11:19:09 EVENT: RECONNECTING

2022-08-18 11:19:09 EVENT: RESOLVE

2022-08-18 11:19:09 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:19:09 EVENT: WAIT

2022-08-18 11:19:09 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:19:10 EVENT: CONNECTING

2022-08-18 11:19:10 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:19:10 Creds: Username/Password

2022-08-18 11:19:10 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:19:10 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:19:10 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:19:14 OS Event: SLEEP

2022-08-18 11:19:14 EVENT: PAUSE

2022-08-18 11:19:29 OS Event: WAKEUP

2022-08-18 11:19:32 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:19:32 STANDARD RESUME

2022-08-18 11:19:32 EVENT: RESUME

2022-08-18 11:19:32 EVENT: RECONNECTING

2022-08-18 11:19:32 EVENT: RESOLVE

2022-08-18 11:19:32 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:19:32 EVENT: WAIT

2022-08-18 11:19:32 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:19:32 EVENT: CONNECTING

2022-08-18 11:19:32 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:19:32 Creds: Username/Password

2022-08-18 11:19:32 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:19:32 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:19:32 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:19:42 OS Event: SLEEP

2022-08-18 11:19:42 EVENT: PAUSE

2022-08-18 11:20:00 OS Event: WAKEUP

2022-08-18 11:20:03 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:20:03 STANDARD RESUME

2022-08-18 11:20:03 EVENT: RESUME

2022-08-18 11:20:03 EVENT: RECONNECTING

2022-08-18 11:20:03 EVENT: RESOLVE

2022-08-18 11:20:03 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:20:03 EVENT: WAIT

2022-08-18 11:20:03 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:20:03 EVENT: CONNECTING

2022-08-18 11:20:03 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:20:03 Creds: Username/Password

2022-08-18 11:20:03 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:20:03 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:20:03 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:20:08 OS Event: SLEEP

2022-08-18 11:20:08 EVENT: PAUSE

2022-08-18 11:20:13 OS Event: WAKEUP

2022-08-18 11:20:16 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:20:16 STANDARD RESUME

2022-08-18 11:20:16 EVENT: RESUME

2022-08-18 11:20:16 EVENT: RECONNECTING

2022-08-18 11:20:16 EVENT: RESOLVE

2022-08-18 11:20:16 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:20:16 EVENT: WAIT

2022-08-18 11:20:16 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:20:17 EVENT: CONNECTING

2022-08-18 11:20:17 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:20:17 Creds: Username/Password

2022-08-18 11:20:17 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:20:17 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:20:17 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:20:32 OS Event: SLEEP

2022-08-18 11:20:32 EVENT: PAUSE

2022-08-18 11:20:41 OS Event: WAKEUP

2022-08-18 11:20:44 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:20:44 STANDARD RESUME

2022-08-18 11:20:44 EVENT: RESUME

2022-08-18 11:20:44 EVENT: RECONNECTING

2022-08-18 11:20:44 EVENT: RESOLVE

2022-08-18 11:20:44 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:20:44 EVENT: WAIT

2022-08-18 11:20:44 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:20:44 EVENT: CONNECTING

2022-08-18 11:20:44 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:20:44 Creds: Username/Password

2022-08-18 11:20:44 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:20:44 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:20:44 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:20:51 OS Event: SLEEP

2022-08-18 11:20:51 EVENT: PAUSE

2022-08-18 11:21:02 OS Event: WAKEUP

2022-08-18 11:21:05 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:21:05 STANDARD RESUME

2022-08-18 11:21:05 EVENT: RESUME

2022-08-18 11:21:05 EVENT: RECONNECTING

2022-08-18 11:21:05 EVENT: RESOLVE

2022-08-18 11:21:05 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:21:05 EVENT: WAIT

2022-08-18 11:21:05 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:21:05 EVENT: CONNECTING

2022-08-18 11:21:05 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:21:05 Creds: Username/Password

2022-08-18 11:21:05 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:21:06 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:21:06 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:21:10 OS Event: SLEEP

2022-08-18 11:21:10 EVENT: PAUSE

2022-08-18 11:21:33 OS Event: WAKEUP

2022-08-18 11:21:36 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:21:36 STANDARD RESUME

2022-08-18 11:21:36 EVENT: RESUME

2022-08-18 11:21:36 EVENT: RECONNECTING

2022-08-18 11:21:36 EVENT: RESOLVE

2022-08-18 11:21:36 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:21:36 EVENT: WAIT

2022-08-18 11:21:36 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:21:37 EVENT: CONNECTING

2022-08-18 11:21:37 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:21:37 Creds: Username/Password

2022-08-18 11:21:37 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:21:37 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:21:37 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:21:41 OS Event: SLEEP

2022-08-18 11:21:41 EVENT: PAUSE

2022-08-18 11:22:05 OS Event: WAKEUP

2022-08-18 11:22:08 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:22:08 STANDARD RESUME

2022-08-18 11:22:08 EVENT: RESUME

2022-08-18 11:22:08 EVENT: RECONNECTING

2022-08-18 11:22:08 EVENT: RESOLVE

2022-08-18 11:22:08 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:22:08 EVENT: WAIT

2022-08-18 11:22:08 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:22:08 EVENT: CONNECTING

2022-08-18 11:22:08 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:22:08 Creds: Username/Password

2022-08-18 11:22:08 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:22:08 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:22:08 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:22:13 OS Event: SLEEP

2022-08-18 11:22:13 EVENT: PAUSE

2022-08-18 11:22:37 OS Event: WAKEUP

2022-08-18 11:22:40 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:22:40 STANDARD RESUME

2022-08-18 11:22:40 EVENT: RESUME

2022-08-18 11:22:40 EVENT: RECONNECTING

2022-08-18 11:22:40 EVENT: RESOLVE

2022-08-18 11:22:40 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:22:40 EVENT: WAIT

2022-08-18 11:22:40 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:22:41 EVENT: CONNECTING

2022-08-18 11:22:41 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:22:41 Creds: Username/Password

2022-08-18 11:22:41 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:22:41 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:22:41 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:22:45 OS Event: SLEEP

2022-08-18 11:22:45 EVENT: PAUSE

2022-08-18 11:23:09 OS Event: WAKEUP

2022-08-18 11:23:12 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:23:12 STANDARD RESUME

2022-08-18 11:23:12 EVENT: RESUME

2022-08-18 11:23:12 EVENT: RECONNECTING

2022-08-18 11:23:12 EVENT: RESOLVE

2022-08-18 11:23:12 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:23:12 EVENT: WAIT

2022-08-18 11:23:12 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:23:12 EVENT: CONNECTING

2022-08-18 11:23:12 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:23:12 Creds: Username/Password

2022-08-18 11:23:12 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:23:12 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:23:12 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:23:52 Session invalidated: KEEPALIVE_TIMEOUT

2022-08-18 11:23:52 Client terminated, restarting in 2000 ms...

2022-08-18 11:23:54 EVENT: RECONNECTING

2022-08-18 11:23:54 EVENT: RESOLVE

2022-08-18 11:23:54 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:23:54 EVENT: WAIT

2022-08-18 11:23:54 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:23:54 EVENT: CONNECTING

2022-08-18 11:23:54 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:23:54 Creds: Username/Password

2022-08-18 11:23:54 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:23:55 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:23:55 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:24:06 OS Event: SLEEP

2022-08-18 11:24:06 EVENT: PAUSE

2022-08-18 11:24:11 OS Event: WAKEUP

2022-08-18 11:24:14 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:24:14 STANDARD RESUME

2022-08-18 11:24:14 EVENT: RESUME

2022-08-18 11:24:14 EVENT: RECONNECTING

2022-08-18 11:24:14 EVENT: RESOLVE

2022-08-18 11:24:14 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:24:14 EVENT: WAIT

2022-08-18 11:24:14 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:24:14 EVENT: CONNECTING

2022-08-18 11:24:14 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:24:14 Creds: Username/Password

2022-08-18 11:24:14 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:24:14 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:24:14 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:24:38 OS Event: SLEEP

2022-08-18 11:24:38 EVENT: PAUSE

2022-08-18 11:24:42 OS Event: WAKEUP

2022-08-18 11:24:45 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:24:45 STANDARD RESUME

2022-08-18 11:24:45 EVENT: RESUME

2022-08-18 11:24:45 EVENT: RECONNECTING

2022-08-18 11:24:45 EVENT: RESOLVE

2022-08-18 11:24:45 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:24:45 EVENT: WAIT

2022-08-18 11:24:45 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:24:45 EVENT: CONNECTING

2022-08-18 11:24:45 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:24:45 Creds: Username/Password

2022-08-18 11:24:45 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:24:45 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:24:45 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:24:50 OS Event: SLEEP

2022-08-18 11:24:50 EVENT: PAUSE

2022-08-18 11:25:14 OS Event: WAKEUP

2022-08-18 11:25:17 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:25:17 STANDARD RESUME

2022-08-18 11:25:17 EVENT: RESUME

2022-08-18 11:25:17 EVENT: RECONNECTING

2022-08-18 11:25:17 EVENT: RESOLVE

2022-08-18 11:25:17 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:25:17 EVENT: WAIT

2022-08-18 11:25:17 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:25:17 EVENT: CONNECTING

2022-08-18 11:25:17 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:25:17 Creds: Username/Password

2022-08-18 11:25:17 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:25:18 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:25:18 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:25:22 OS Event: SLEEP

2022-08-18 11:25:22 EVENT: PAUSE

2022-08-18 11:25:46 OS Event: WAKEUP

2022-08-18 11:25:49 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:25:49 STANDARD RESUME

2022-08-18 11:25:49 EVENT: RESUME

2022-08-18 11:25:49 EVENT: RECONNECTING

2022-08-18 11:25:49 EVENT: RESOLVE

2022-08-18 11:25:49 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:25:49 EVENT: WAIT

2022-08-18 11:25:49 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:25:49 EVENT: CONNECTING

2022-08-18 11:25:49 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:25:49 Creds: Username/Password

2022-08-18 11:25:49 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:25:49 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:25:49 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:26:30 Session invalidated: KEEPALIVE_TIMEOUT

2022-08-18 11:26:30 Client terminated, restarting in 2000 ms...

2022-08-18 11:26:32 EVENT: RECONNECTING

2022-08-18 11:26:32 EVENT: RESOLVE

2022-08-18 11:26:32 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:26:32 EVENT: WAIT

2022-08-18 11:26:32 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:26:33 EVENT: CONNECTING

2022-08-18 11:26:33 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:26:33 Creds: Username/Password

2022-08-18 11:26:33 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:26:34 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:26:34 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:26:49 OS Event: SLEEP

2022-08-18 11:26:49 EVENT: PAUSE

2022-08-18 11:26:50 OS Event: WAKEUP

2022-08-18 11:26:53 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:26:53 STANDARD RESUME

2022-08-18 11:26:53 EVENT: RESUME

2022-08-18 11:26:53 EVENT: RECONNECTING

2022-08-18 11:26:53 EVENT: RESOLVE

2022-08-18 11:26:53 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:26:53 EVENT: WAIT

2022-08-18 11:26:53 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:26:54 EVENT: CONNECTING

2022-08-18 11:26:54 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:26:54 Creds: Username/Password

2022-08-18 11:26:54 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:26:54 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:26:54 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:27:27 OS Event: SLEEP

2022-08-18 11:27:27 EVENT: PAUSE

2022-08-18 11:27:34 OS Event: WAKEUP

2022-08-18 11:27:37 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:27:37 STANDARD RESUME

2022-08-18 11:27:37 EVENT: RESUME

2022-08-18 11:27:37 EVENT: RECONNECTING

2022-08-18 11:27:37 EVENT: RESOLVE

2022-08-18 11:27:37 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:27:37 EVENT: WAIT

2022-08-18 11:27:37 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:27:37 EVENT: CONNECTING

2022-08-18 11:27:37 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:27:37 Creds: Username/Password

2022-08-18 11:27:37 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:27:37 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:27:37 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:27:46 OS Event: SLEEP

2022-08-18 11:27:46 EVENT: PAUSE

2022-08-18 11:28:05 OS Event: WAKEUP

2022-08-18 11:28:08 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:28:08 STANDARD RESUME

2022-08-18 11:28:08 EVENT: RESUME

2022-08-18 11:28:08 EVENT: RECONNECTING

2022-08-18 11:28:08 EVENT: RESOLVE

2022-08-18 11:28:08 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:28:08 EVENT: WAIT

2022-08-18 11:28:08 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:28:08 EVENT: CONNECTING

2022-08-18 11:28:08 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:28:08 Creds: Username/Password

2022-08-18 11:28:08 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:28:09 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:28:09 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:28:17 OS Event: SLEEP

2022-08-18 11:28:17 EVENT: PAUSE

2022-08-18 11:28:37 OS Event: WAKEUP

2022-08-18 11:28:40 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:28:40 STANDARD RESUME

2022-08-18 11:28:40 EVENT: RESUME

2022-08-18 11:28:40 EVENT: RECONNECTING

2022-08-18 11:28:40 EVENT: RESOLVE

2022-08-18 11:28:40 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:28:40 EVENT: WAIT

2022-08-18 11:28:40 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:28:40 EVENT: CONNECTING

2022-08-18 11:28:40 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:28:40 Creds: Username/Password

2022-08-18 11:28:40 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:28:40 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:28:40 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:28:45 OS Event: SLEEP

2022-08-18 11:28:45 EVENT: PAUSE

2022-08-18 11:29:08 OS Event: WAKEUP

2022-08-18 11:29:11 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:29:11 STANDARD RESUME

2022-08-18 11:29:11 EVENT: RESUME

2022-08-18 11:29:11 EVENT: RECONNECTING

2022-08-18 11:29:11 EVENT: RESOLVE

2022-08-18 11:29:11 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:29:11 EVENT: WAIT

2022-08-18 11:29:11 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:29:11 EVENT: CONNECTING

2022-08-18 11:29:11 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:29:11 Creds: Username/Password

2022-08-18 11:29:11 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:29:11 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:29:11 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:29:24 OS Event: SLEEP

2022-08-18 11:29:24 EVENT: PAUSE

2022-08-18 11:29:39 OS Event: WAKEUP

2022-08-18 11:29:42 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:29:42 STANDARD RESUME

2022-08-18 11:29:42 EVENT: RESUME

2022-08-18 11:29:42 EVENT: RECONNECTING

2022-08-18 11:29:42 EVENT: RESOLVE

2022-08-18 11:29:42 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:29:42 EVENT: WAIT

2022-08-18 11:29:42 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:29:42 EVENT: CONNECTING

2022-08-18 11:29:42 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:29:42 Creds: Username/Password

2022-08-18 11:29:42 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:29:43 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:29:43 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:29:47 OS Event: SLEEP

2022-08-18 11:29:47 EVENT: PAUSE

2022-08-18 11:30:11 OS Event: WAKEUP

2022-08-18 11:30:14 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:30:14 STANDARD RESUME

2022-08-18 11:30:14 EVENT: RESUME

2022-08-18 11:30:14 EVENT: RECONNECTING

2022-08-18 11:30:14 EVENT: RESOLVE

2022-08-18 11:30:14 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:30:14 EVENT: WAIT

2022-08-18 11:30:14 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:30:14 EVENT: CONNECTING

2022-08-18 11:30:14 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:30:14 Creds: Username/Password

2022-08-18 11:30:14 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:30:14 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:30:14 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:30:19 OS Event: SLEEP

2022-08-18 11:30:19 EVENT: PAUSE

2022-08-18 11:30:43 OS Event: WAKEUP

2022-08-18 11:30:46 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:30:46 STANDARD RESUME

2022-08-18 11:30:46 EVENT: RESUME

2022-08-18 11:30:46 EVENT: RECONNECTING

2022-08-18 11:30:46 EVENT: RESOLVE

2022-08-18 11:30:46 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:30:46 EVENT: WAIT

2022-08-18 11:30:46 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:30:46 EVENT: CONNECTING

2022-08-18 11:30:46 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:30:46 Creds: Username/Password

2022-08-18 11:30:46 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:30:47 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:30:47 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:30:51 OS Event: SLEEP

2022-08-18 11:30:51 EVENT: PAUSE

2022-08-18 11:31:14 OS Event: WAKEUP

2022-08-18 11:31:17 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:31:17 STANDARD RESUME

2022-08-18 11:31:17 EVENT: RESUME

2022-08-18 11:31:17 EVENT: RECONNECTING

2022-08-18 11:31:17 EVENT: RESOLVE

2022-08-18 11:31:17 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:31:17 EVENT: WAIT

2022-08-18 11:31:17 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:31:17 EVENT: CONNECTING

2022-08-18 11:31:17 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:31:17 Creds: Username/Password

2022-08-18 11:31:17 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:31:17 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:31:17 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:31:43 OS Event: SLEEP

2022-08-18 11:31:43 EVENT: PAUSE

2022-08-18 11:31:44 OS Event: WAKEUP

2022-08-18 11:31:47 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:31:47 STANDARD RESUME

2022-08-18 11:31:47 EVENT: RESUME

2022-08-18 11:31:47 EVENT: RECONNECTING

2022-08-18 11:31:47 EVENT: RESOLVE

2022-08-18 11:31:47 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:31:47 EVENT: WAIT

2022-08-18 11:31:47 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:31:47 EVENT: CONNECTING

2022-08-18 11:31:47 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:31:47 Creds: Username/Password

2022-08-18 11:31:47 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:31:47 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:31:47 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:31:59 OS Event: SLEEP

2022-08-18 11:31:59 EVENT: PAUSE

2022-08-18 11:32:16 OS Event: WAKEUP

2022-08-18 11:32:19 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:32:19 STANDARD RESUME

2022-08-18 11:32:19 EVENT: RESUME

2022-08-18 11:32:19 EVENT: RECONNECTING

2022-08-18 11:32:19 EVENT: RESOLVE

2022-08-18 11:32:19 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:32:19 EVENT: WAIT

2022-08-18 11:32:19 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:32:19 EVENT: CONNECTING

2022-08-18 11:32:19 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:32:19 Creds: Username/Password

2022-08-18 11:32:19 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:32:19 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:32:19 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:32:24 OS Event: SLEEP

2022-08-18 11:32:24 EVENT: PAUSE

2022-08-18 11:32:27 OS Event: WAKEUP

2022-08-18 11:32:30 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:32:30 STANDARD RESUME

2022-08-18 11:32:30 EVENT: RESUME

2022-08-18 11:32:30 EVENT: RECONNECTING

2022-08-18 11:32:30 EVENT: RESOLVE

2022-08-18 11:32:30 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:32:30 EVENT: WAIT

2022-08-18 11:32:30 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:32:30 EVENT: CONNECTING

2022-08-18 11:32:30 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:32:30 Creds: Username/Password

2022-08-18 11:32:30 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:32:30 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:32:30 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:32:54 OS Event: SLEEP

2022-08-18 11:32:54 EVENT: PAUSE

2022-08-18 11:33:03 OS Event: WAKEUP

2022-08-18 11:33:06 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:33:06 STANDARD RESUME

2022-08-18 11:33:06 EVENT: RESUME

2022-08-18 11:33:06 EVENT: RECONNECTING

2022-08-18 11:33:06 EVENT: RESOLVE

2022-08-18 11:33:06 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:33:06 EVENT: WAIT

2022-08-18 11:33:06 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:33:06 EVENT: CONNECTING

2022-08-18 11:33:06 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:33:06 Creds: Username/Password

2022-08-18 11:33:06 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:33:06 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:33:06 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:33:11 OS Event: SLEEP

2022-08-18 11:33:11 EVENT: PAUSE

2022-08-18 11:33:11 OS Event: WAKEUP

2022-08-18 11:33:14 RESUME TEST: Internet:ReachableViaWiFi/-R -------

2022-08-18 11:33:14 STANDARD RESUME

2022-08-18 11:33:14 EVENT: RESUME

2022-08-18 11:33:14 EVENT: RECONNECTING

2022-08-18 11:33:14 EVENT: RESOLVE

2022-08-18 11:33:14 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:33:14 EVENT: WAIT

2022-08-18 11:33:14 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:33:14 EVENT: CONNECTING

2022-08-18 11:33:14 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:33:14 Creds: Username/Password

2022-08-18 11:33:14 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:33:14 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:33:14 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:33:54 Session invalidated: KEEPALIVE_TIMEOUT

2022-08-18 11:33:54 Client terminated, restarting in 2000 ms...

2022-08-18 11:33:56 EVENT: RECONNECTING

2022-08-18 11:33:56 EVENT: RESOLVE

2022-08-18 11:33:56 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 11:33:56 EVENT: WAIT

2022-08-18 11:33:56 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 11:33:56 EVENT: CONNECTING

2022-08-18 11:33:56 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 11:33:56 Creds: Username/Password

2022-08-18 11:33:56 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 11:33:57 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 11:33:57 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 11:34:14 EVENT: CONNECTION_TIMEOUT [ERR]

2022-08-18 11:34:14 Raw stats on disconnect:
  BYTES_IN : 184356
  BYTES_OUT : 1465091
  PACKETS_IN : 324
  PACKETS_OUT : 1570
  KEEPALIVE_TIMEOUT : 5
  CONNECTION_TIMEOUT : 1
  N_PAUSE : 48
  N_RECONNECT : 53

2022-08-18 11:34:14 Performance stats on disconnect:
  CPU usage (microseconds): 1110491
  Network bytes per CPU second: 1485331
  Tunnel bytes per CPU second: 0

2022-08-18 11:34:14 EVENT: DISCONNECTED

2022-08-18 11:34:14 Raw stats on disconnect:
  BYTES_IN : 184356
  BYTES_OUT : 1465091
  PACKETS_IN : 324
  PACKETS_OUT : 1570
  KEEPALIVE_TIMEOUT : 5
  CONNECTION_TIMEOUT : 1
  N_PAUSE : 48
  N_RECONNECT : 53

2022-08-18 11:34:14 Performance stats on disconnect:
  CPU usage (microseconds): 1122372
  Network bytes per CPU second: 1469608
  Tunnel bytes per CPU second: 0

2022-08-18 19:21:29 1

2022-08-18 19:21:29 ----- OpenVPN Start -----
OpenVPN core 3.git::58b92569 ios arm64 64-bit

2022-08-18 19:21:29 OpenVPN core 3.git::58b92569 ios arm64 64-bit

2022-08-18 19:21:29 Frame=512/2048/512 mssfix-ctrl=1250

2022-08-18 19:21:29 UNUSED OPTIONS
0 [persist-tun] 
1 [persist-key] 
4 [tls-client] 
7 [verify-x509-name] [88.97.10.81] [name] 

2022-08-18 19:21:29 EVENT: RESOLVE

2022-08-18 19:21:29 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 19:21:29 EVENT: WAIT

2022-08-18 19:21:29 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 19:21:29 EVENT: CONNECTING

2022-08-18 19:21:29 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 19:21:29 Creds: Username/Password

2022-08-18 19:21:29 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 19:21:29 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 19:21:29 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 19:22:09 Session invalidated: KEEPALIVE_TIMEOUT

2022-08-18 19:22:09 Client terminated, restarting in 2000 ms...

2022-08-18 19:22:12 EVENT: RECONNECTING

2022-08-18 19:22:12 EVENT: RESOLVE

2022-08-18 19:22:12 Contacting [88.97.10.81]:1194/UDP via UDP

2022-08-18 19:22:12 EVENT: WAIT

2022-08-18 19:22:12 Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

2022-08-18 19:22:12 EVENT: CONNECTING

2022-08-18 19:22:12 Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

2022-08-18 19:22:12 Creds: Username/Password

2022-08-18 19:22:12 Peer Info:
IV_VER=3.git::58b92569
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=2
IV_GUI_VER=net.openvpn.connect.ios_3.2.3-3760
IV_SSO=openurl


2022-08-18 19:22:12 VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca

2022-08-18 19:22:12 VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81

2022-08-18 19:22:30 EVENT: CONNECTION_TIMEOUT [ERR]

2022-08-18 19:22:30 Raw stats on disconnect:
  BYTES_IN : 6828
  BYTES_OUT : 121498
  PACKETS_IN : 12
  PACKETS_OUT : 122
  KEEPALIVE_TIMEOUT : 1
  CONNECTION_TIMEOUT : 1
  N_RECONNECT : 1

2022-08-18 19:22:30 Performance stats on disconnect:
  CPU usage (microseconds): 71777
  Network bytes per CPU second: 1787842
  Tunnel bytes per CPU second: 0

2022-08-18 19:22:30 EVENT: DISCONNECTED

2022-08-18 19:22:30 Raw stats on disconnect:
  BYTES_IN : 6828
  BYTES_OUT : 121498
  PACKETS_IN : 12
  PACKETS_OUT : 122
  KEEPALIVE_TIMEOUT : 1
  CONNECTION_TIMEOUT : 1
  N_RECONNECT : 1

2022-08-18 19:22:30 Performance stats on disconnect:
  CPU usage (microseconds): 81345
  Network bytes per CPU second: 1577552
  Tunnel bytes per CPU second: 0


[Aug 18, 2022, 19:38:05] START CONNECTION

[Aug 18, 2022, 19:38:05] ----- OpenVPN Start -----
OpenVPN core 3.git::081bfebe ios arm64 64-bit

[Aug 18, 2022, 19:38:05] OpenVPN core 3.git::081bfebe ios arm64 64-bit

[Aug 18, 2022, 19:38:05] Frame=512/2048/512 mssfix-ctrl=1250

[Aug 18, 2022, 19:38:05] UNUSED OPTIONS
0 [persist-tun]
1 [persist-key]
4 [tls-client]

[Aug 18, 2022, 19:38:05] EVENT: RESOLVE

[Aug 18, 2022, 19:38:05] Contacting 88.97.10.81:1194 via UDP

[Aug 18, 2022, 19:38:05] EVENT: WAIT

[Aug 18, 2022, 19:38:05] Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

[Aug 18, 2022, 19:38:05] EVENT: CONNECTING

[Aug 18, 2022, 19:38:05] Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

[Aug 18, 2022, 19:38:05] Creds: Username/Password

[Aug 18, 2022, 19:38:05] Peer Info:
IV_VER=3.git::081bfebe
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=30
IV_CIPHERS=AES-256-GCM:AES-128-GCM:CHACHA20-POLY1305:AES-256-CBC
IV_GUI_VER=net.openvpn.connect.ios_3.3.0-5047
IV_SSO=webauth,openurl,crtext


[Aug 18, 2022, 19:38:05] VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca, signature: RSA-SHA256

[Aug 18, 2022, 19:38:05] VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81, signature: RSA-SHA256

[Aug 18, 2022, 19:38:21] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:21] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:22] NIP: iOS reported network status available

[Aug 18, 2022, 19:38:22] OS Event: NET AVAILABLE (RESUME): Internet:ReachableViaWWAN/WR t------ allow=1

[Aug 18, 2022, 19:38:22] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:22] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:23] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:23] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:24] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:24] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:25] RECONNECT TEST: Internet:ReachableViaWWAN/WR t------

[Aug 18, 2022, 19:38:25] EARLY RECONNECT

[Aug 18, 2022, 19:38:25] Client terminated, reconnecting in 1...

[Aug 18, 2022, 19:38:25] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:25] UDP send exception: send: Broken pipe

[Aug 18, 2022, 19:38:25] EVENT: DISCONNECTED

[Aug 18, 2022, 19:38:25] EVENT: CORE_THREAD_DONE

[Aug 18, 2022, 19:38:25] EVENT: DISCONNECT_PENDING

[Aug 18, 2022, 19:38:25] Raw stats on disconnect:
  BYTES_IN : 3414
  BYTES_OUT : 36797
  PACKETS_IN : 6
  PACKETS_OUT : 37
  NETWORK_SEND_ERROR : 10


[Aug 18, 2022, 19:38:25] Performance stats on disconnect:
  CPU usage (microseconds): 51293
  Network bytes per CPU second: 783947
  Tunnel bytes per CPU second: 0

[Aug 18, 2022, 19:38:27] START CONNECTION

[Aug 18, 2022, 19:38:27] ----- OpenVPN Start -----
OpenVPN core 3.git::081bfebe ios arm64 64-bit

[Aug 18, 2022, 19:38:27] OpenVPN core 3.git::081bfebe ios arm64 64-bit

[Aug 18, 2022, 19:38:27] Frame=512/2048/512 mssfix-ctrl=1250

[Aug 18, 2022, 19:38:27] UNUSED OPTIONS
0 [persist-tun]
1 [persist-key]
4 [tls-client]

[Aug 18, 2022, 19:38:27] EVENT: RESOLVE

[Aug 18, 2022, 19:38:27] Contacting 88.97.10.81:1194 via UDP

[Aug 18, 2022, 19:38:27] EVENT: WAIT

[Aug 18, 2022, 19:38:27] Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

[Aug 18, 2022, 19:38:27] EVENT: CONNECTING

[Aug 18, 2022, 19:38:27] Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

[Aug 18, 2022, 19:38:27] Creds: Username/Password

[Aug 18, 2022, 19:38:27] Peer Info:
IV_VER=3.git::081bfebe
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=30
IV_CIPHERS=AES-256-GCM:AES-128-GCM:CHACHA20-POLY1305:AES-256-CBC
IV_GUI_VER=net.openvpn.connect.ios_3.3.0-5047
IV_SSO=webauth,openurl,crtext


[Aug 18, 2022, 19:38:27] VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca, signature: RSA-SHA256

[Aug 18, 2022, 19:38:27] VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81, signature: RSA-SHA256

[Aug 18, 2022, 19:39:07] Session invalidated: KEEPALIVE_TIMEOUT

[Aug 18, 2022, 19:39:07] Client terminated, restarting in 2000 ms...

[Aug 18, 2022, 19:39:09] EVENT: RECONNECTING

[Aug 18, 2022, 19:39:09] EVENT: RESOLVE

[Aug 18, 2022, 19:39:09] Contacting 88.97.10.81:1194 via UDP

[Aug 18, 2022, 19:39:09] EVENT: WAIT

[Aug 18, 2022, 19:39:09] Connecting to [88.97.10.81]:1194 (88.97.10.81) via UDPv4

[Aug 18, 2022, 19:39:10] EVENT: CONNECTING

[Aug 18, 2022, 19:39:10] Tunnel Options:V4,dev-type tun,link-mtu 1569,tun-mtu 1500,proto UDPv4,keydir 1,cipher AES-256-CBC,auth SHA256,keysize 256,tls-auth,key-method 2,tls-client

[Aug 18, 2022, 19:39:10] Creds: Username/Password

[Aug 18, 2022, 19:39:10] Peer Info:
IV_VER=3.git::081bfebe
IV_PLAT=ios
IV_NCP=2
IV_TCPNL=1
IV_PROTO=30
IV_CIPHERS=AES-256-GCM:AES-128-GCM:CHACHA20-POLY1305:AES-256-CBC
IV_GUI_VER=net.openvpn.connect.ios_3.3.0-5047
IV_SSO=webauth,openurl,crtext


[Aug 18, 2022, 19:39:10] VERIFY OK: depth=1, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=home-ca, signature: RSA-SHA256

[Aug 18, 2022, 19:39:10] VERIFY OK: depth=0, /C=GB/ST=County Durham/L=Bishop Auckland/O=Hiklas Ltd/emailAddress=admin@hiklas.co.uk/CN=88.97.10.81, signature: RSA-SHA256

[Aug 18, 2022, 19:39:27] EVENT: CONNECTION_TIMEOUT [ERR]

[Aug 18, 2022, 19:39:27] EVENT: DISCONNECTED

[Aug 18, 2022, 19:39:27] EVENT: CORE_THREAD_DONE

[Aug 18, 2022, 19:39:27] EVENT: DISCONNECT_PENDING

[Aug 18, 2022, 19:39:27] Raw stats on disconnect:
  BYTES_IN : 6828
  BYTES_OUT : 132589
  PACKETS_IN : 12
  PACKETS_OUT : 128
  KEEPALIVE_TIMEOUT : 1
  CONNECTION_TIMEOUT : 1
  N_RECONNECT : 1


[Aug 18, 2022, 19:39:27] Performance stats on disconnect:
  CPU usage (microseconds): 76535
  Network bytes per CPU second: 1821611
  Tunnel bytes per CPU second: 0

[Aug 18, 2022, 19:40:15] OS Event: SLEEP

[Aug 18, 2022, 19:40:27] OS Event: WAKEUP
```

On server seeing this

```

```


## Investigate

### Exporting CRL

* Can export from pfSense
* Running this command on the file 

```
openssl crl -text -in ~/Downloads/List+of+dead+certs-2.crl
```

Gave this output

```
Certificate Revocation List (CRL):
        Version 2 (0x1)
    Signature Algorithm: sha1WithRSAEncryption
        Issuer: /C=GB/ST=County/L=Wibble/O=Wibble Ltd/emailAddress=wibble@wibble.co.uk/CN=home-ca
        Last Update: Aug 18 19:00:24 2022 GMT
        Next Update: Jan  2 19:00:24 1950 GMT
        CRL extensions:
            X509v3 Authority Key Identifier: 
                keyid:3F:1D:F7:77:A8:9E:C5:87:E7:A2:CB:5A:71:35:29:7C:A5:FA:6E:EC
                DirName:/C=AB/ST=County/L=Wibble/O=Wibble Ltd/emailAddress=wibble@wibble.co.uk/CN=home-ca
                serial:00

            X509v3 CRL Number: 
                10001
Revoked Certificates:
    Serial Number: 06
        Revocation Date: Aug 27 05:31:41 2016 GMT
        CRL entry extensions:
            X509v3 CRL Reason Code: 
                Key Compromise
    Signature Algorithm: sha1WithRSAEncryption
         9b:ca:75:35:95:c6:0a:38:ac:70:d9:95:8c:b7:4d:56:5c:d8:
                            :
                            :
         0d:56:73:7f
-----BEGIN X509 CRL-----
MIICxDCCAawCAQEwDQYJKoZIhvcNAQEFBQAwgYkxCzAJBgNVBAYTAkdCMRYwFAYD
    :
    :
Tv6iWd1On+tyfo173vYsmOwyWEXZxqIHMultkl50iWyxrUQuDVZzfw==
-----END X509 CRL-----
```

* The 1950 date looks suspicious

## Quick Solution

* Switch off CRL verification on the server side - which prevents the client from doing it.
* This seems to work because the CRL looks invalid with that 1950 date



## References

* [Mentioned unable to get certificate CRL](https://www.reddit.com/r/PFSENSE/comments/uax0hs/openvpn_server_certificate_verify_failed_on/)
* [CRL enable on server might cause this](https://forums.openvpn.net/viewtopic.php?f=22&t=32341)
* [CRL is Certificate Revocation List](https://www.techtarget.com/searchsecurity/definition/Certificate-Revocation-List)
* [Parsing a CRL](https://langui.sh/2010/01/10/parsing-a-crl-with-openssl/)
* [What does next update date mean](https://security.stackexchange.com/questions/20958/x-509-crls-next-update)



