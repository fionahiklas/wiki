# Camera Module

## Overview

Playing with RPi5 with two cameras attached


## Notes

### Initial Camera Tests

* Running over X11/SSH connection from Mac laptop
* Running the following to get a list of cameras

```
libcamera-still --list-cameras
Available cameras
-----------------
0 : imx708_wide [4608x2592 10-bit RGGB] (/base/axi/pcie@120000/rp1/i2c@88000/imx708@1a)
    Modes: 'SRGGB10_CSI2P' : 1536x864 [120.13 fps - (768, 432)/3072x1728 crop]
                             2304x1296 [56.03 fps - (0, 0)/4608x2592 crop]
                             4608x2592 [14.35 fps - (0, 0)/4608x2592 crop]

1 : imx708 [4608x2592 10-bit RGGB] (/base/axi/pcie@120000/rp1/i2c@80000/imx708@1a)
    Modes: 'SRGGB10_CSI2P' : 1536x864 [120.13 fps - (768, 432)/3072x1728 crop]
                             2304x1296 [56.03 fps - (0, 0)/4608x2592 crop]
                             4608x2592 [14.35 fps - (0, 0)/4608x2592 crop]
```

* You can also use `libcamera-vid` with the same arguments to get the same list
* Using motion to view both cameras

```
libcamerify motion -n -l motion.log -m -c motion.conf
```

* Camera0.conf

```
video_device /dev/video0
camera_name Camera0
camera_id 0
```

* Camera1.conf

```
video_device /dev/video8
camera_name Camera1
camera_id 1
```

### Running more test on RPi screen

* Actually logged onto a desktop with a display connected to the RPi 
* In this case a cheap HDMI touchscreen
* This command 


## References

### Software

* [List available still cameras](https://techoverflow.net/2022/12/26/how-to-list-available-cameras-on-raspberry-pi-libcamera/)
* [RPi Camera tools](https://www.raspberrypi.com/documentation/computers/camera_software.html)

