# LXI Tools

## Overview

Information about LXI tools and how these can be used to communitcate with 
test equipment connected to the network over ethernet.


## Setup

### Tools Installation

#### Linux

On a Raspberry Pi (running Raspbian) the following command installs the 
LXI tools

```
apt install lximage-qt lxi-tools telnet
```


### Network Setup

#### Linux

On the Raspberry Pi I have the default rule for the INPUT chain is `DENY` so I needed to add rules
for the ports that LXI uses

```
iptables -I INPUT -p tcp -m tcp --sport 5555 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I INPUT -p tcp -m tcp --sport 4880 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I INPUT -p tcp -m tcp --sport 5025 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I INPUT -p tcp -m tcp --sport 5024 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I INPUT -p udp -m udp --sport 5044 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I INPUT -p tcp -m tcp --sport 111 -m state --state NEW,ESTABLISHED -j ACCEPT
```




## References

* [LXI Tools Repo](https://github.com/lxi-tools/lxi-tools)
* [LXI Tools Snapcraft](https://snapcraft.io/lxi-tools)
* [LXI Discovery Tool](https://www.lxistandard.org/Resources/LXIDiscoveryTool.aspx)
* [LXI Ports](https://www.lxistandard.org/Documents/GuidesForUsingLXI/Introducing%20LXI%20To%20Your%20Network%20Administrator%20Aug%203%202013.pdf)


