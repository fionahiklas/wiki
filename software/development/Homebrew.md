## Overview 

Finally caved and installing software using this because building all the 
dependencies is getting ridiculously complicated!


## Installation

Using the [untar anywhere](https://docs.brew.sh/Installation#untar-anywhere) installation method so that I don't need sudo.

```
cd $HOME/tools
mkdir homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
```


### Errors

Saw this error

```
==> Installing qemu dependency: gnutls
==> ./configure --disable-static --prefix=/Users/fiona/tools/homebrew/Cellar/gnutls/3.7.4 --sys
==> make LDFLAGS= install
Last 15 lines from /Users/fiona/Library/Logs/Homebrew/gnutls/02.make:
libtool: compile:  clang -Wa,-march=all -Wno-implicit-function-declaration -c macosx/sha512-armv8.s  -fno-common -DPIC -o macosx/.libs/sha512-armv8.o
libtool: compile:  clang -DHAVE_CONFIG_H -I. -I../../.. -I./../../../gl -I./../../../gl -I./../../includes -I./../../includes -I./../../ -I./../ -Wtype-limits -fno-common -Wall -Wbad-function-cast -Wdate-time -Wdisabled-optimization -Wdouble-promotion -Wextra -Winit-self -Winvalid-pch -Wmissing-declarations -Wmissing-include-dirs -Wmissing-prototypes -Wnested-externs -Wnull-dereference -Wold-style-definition -Wpacked -Wpointer-arith -Wshadow -Wstrict-prototypes -Wuninitialized -Wunknown-pragmas -Wvariadic-macros -Wwrite-strings -Wformat=2 -Wno-missing-field-initializers -Wno-unused-parameter -fdiagnostics-show-option -fno-builtin-strcmp -I/Users/fiona/tools/homebrew/Cellar/nettle/3.7.3/include -I/Users/fiona/tools/homebrew/Cellar/libtasn1/4.18.0/include -I/Users/fiona/tools/homebrew/Cellar/libidn2/2.3.2/include -I/Users/fiona/tools/homebrew/Cellar/p11-kit/0.24.1/include/p11-kit-1 -Wno-implicit-function-declaration -c sha-aarch64.c  -fno-common -DPIC -o .libs/sha-aarch64.o
../../../libtool: line 1760: 18753 Segmentation fault: 11  clang -Wa,-march=all -Wno-implicit-function-declaration -c macosx/sha1-armv8.s -fno-common -DPIC -o macosx/.libs/sha1-armv8.o
../../../libtool: line 1760: 18747 Segmentation fault: 11  clang -Wa,-march=all -Wno-implicit-function-declaration -c macosx/aes-aarch64.s -fno-common -DPIC -o macosx/.libs/aes-aarch64.o
../../../libtool: line 1760: 18716 Segmentation fault: 11  clang -Wa,-march=all -Wno-implicit-function-declaration -c macosx/sha256-armv8.s -fno-common -DPIC -o macosx/.libs/sha256-armv8.o
make[4]: *** [macosx/aes-aarch64.lo] Error 1
make[4]: *** Waiting for unfinished jobs....
make[4]: *** [macosx/sha1-armv8.lo] Error 1
make[4]: *** [macosx/sha256-armv8.lo] Error 1
../../../libtool: line 1760: 18764 Segmentation fault: 11  clang -Wa,-march=all -Wno-implicit-function-declaration -c macosx/sha512-armv8.s -fno-common -DPIC -o macosx/.libs/sha512-armv8.o
make[4]: *** [macosx/sha512-armv8.lo] Error 1
make[3]: *** [install-recursive] Error 1
make[2]: *** [install-recursive] Error 1
make[1]: *** [install] Error 2
make: *** [install-recursive] Error 1
```

Seems this is a known [bug](https://github.com/Homebrew/discussions/discussions/3147)

This actually relates to [Xcode 13.3 compiler issue on Apple Silicon](https://github.com/Homebrew/homebrew-core/pull/99002)

Tried using the changes from the above PR, seems to work :D  Added a comment to [the discussion](https://github.com/Homebrew/discussions/discussions/3147#discussioncomment-2558813)


