# Esmerelda Alpine Workstation

## Overview

Details on setting up a workstation mini computer with Alpine Linux

The device in question is a Trigkey Mini PC

* Trigkey Ryzen 5700U 8C/16T up to 4.3GHz
* Upgraded to 1Tb NVMe SSD
* Upgraded with 1Tb SATA SSD
* Upgraded to 64Gb main memory
* Powered by Anker Prime 100W USB-C and adapter to 5.5mm power jack


## Install and setup

* Follow instructions in [common setup steps](./common-alpine-x86-setup.md)





## References

### Hardware

* [Amazon UK page for Trigkey](https://www.amazon.co.uk/gp/product/B0DPMS14F8/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&th=1)
* [Trigkey product page](https://trigkey.com/en-gb/products/trigkey-speed-s5-5700u-mini-pc-ryzen-7-w11-pro-desktop-amd-8c16t-5700u-32g-ddr4-500g-nvme-ssd-micro-computer)
* [USB-C 100W 5A power cable with V/A/W display](https://www.amazon.co.uk/dp/B0BHYXLY44?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1)
* [Anker prime 100W USB-C](https://www.amazon.co.uk/dp/B0BHYXLY44?ref=ppx_yo2ov_dt_b_fed_asin_title&th=1)


