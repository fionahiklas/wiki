[//]: #TOCStart

[//]: #TOCEnd


## Overview


## Installation

### Packages

According to the [debian instructions](https://grafana.com/docs/grafana/latest/installation/debian/) the packages can be installed as follows

```
sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
```

Adding the Open Source repo using the following command

```
echo "deb https://packages.grafana.com/oss/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
```

Install the package using the following command

```
apt install grafana
```
