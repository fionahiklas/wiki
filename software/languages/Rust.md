## Overview


## Emacs Setup

* Install `rust-mode` from ELPA



## Installation

### From Rust page

As per the [rust install instructions](https://www.rust-lang.org/tools/install)

Ran this command

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

Got the following output on an M1 Mac

```
info: downloading installer

Welcome to Rust!

This will download and install the official compiler for the Rust
programming language, and its package manager, Cargo.

Rustup metadata and toolchains will be installed into the Rustup
home directory, located at:

  /Users/fiona/.rustup

This can be modified with the RUSTUP_HOME environment variable.

The Cargo home directory located at:

  /Users/fiona/.cargo

This can be modified with the CARGO_HOME environment variable.

The cargo, rustc, rustup and other commands will be added to
Cargo's bin directory, located at:

  /Users/fiona/.cargo/bin

This path will then be added to your PATH environment variable by
modifying the profile files located at:

  /Users/fiona/.profile
  /Users/fiona/.zshenv

You can uninstall at any time with rustup self uninstall and
these changes will be reverted.

Current installation options:


   default host triple: aarch64-apple-darwin
     default toolchain: stable (default)
               profile: default
  modify PATH variable: yes

1) Proceed with installation (default)
2) Customize installation
3) Cancel installation
>
```

It seems to have spotted the ARM64 CPU so proceeding

Seemed to work

```
info: profile set to 'default'
info: default host triple is aarch64-apple-darwin
info: syncing channel updates for 'stable-aarch64-apple-darwin'
info: latest update on 2022-02-24, rust version 1.59.0 (9d1b2106e 2022-02-23)
info: downloading component 'cargo'
info: downloading component 'clippy'
info: downloading component 'rust-std'
 24.2 MiB /  24.2 MiB (100 %)  22.8 MiB/s in  1s ETA:  0s
info: downloading component 'rustc'
 49.9 MiB /  49.9 MiB (100 %)  39.9 MiB/s in  1s ETA:  0s
info: downloading component 'rustfmt'
info: installing component 'cargo'
info: installing component 'clippy'
info: installing component 'rust-std'
 24.2 MiB /  24.2 MiB (100 %)  19.0 MiB/s in  1s ETA:  0s
info: installing component 'rustc'
 49.9 MiB /  49.9 MiB (100 %)  22.0 MiB/s in  2s ETA:  0s
info: installing component 'rustfmt'
info: default toolchain set to 'stable-aarch64-apple-darwin'

  stable-aarch64-apple-darwin installed - rustc 1.59.0 (9d1b2106e 2022-02-23)


Rust is installed now. Great!

To get started you may need to restart your current shell.
This would reload your PATH environment variable to include
Cargo's bin directory ($HOME/.cargo/bin).

To configure your current shell, run:
source $HOME/.cargo/env
```



## References

### Language

* [Effective Rust](https://www.lurklurk.org/effective-rust/intro.html)
* [How to Build a REST API in Rust](https://betterprogramming.pub/rest-api-in-rust-step-by-step-guide-b8a6c5fcbff0)
* [REST API Rust Warp](https://blog.logrocket.com/building-rest-api-rust-warp/)
* [Getting up to speed on Rust](https://blog.logrocket.com/getting-up-to-speed-with-rust/)
* [Writing tests](https://doc.rust-lang.org/book/ch11-01-writing-tests.html)
* [Modules cheatsheet](https://doc.rust-lang.org/book/ch07-02-defining-modules-to-control-scope-and-privacy.html)
* [Package layout](https://doc.rust-lang.org/cargo/guide/project-layout.html)


### Emacs

* [Rust Analyzer home page](https://rust-analyzer.github.io)
* [Emacs IDE with rust analyzer](https://robert.kra.hn/posts/rust-emacs-setup/)
* [Emacs rust IDE](https://unwoundstack.com/blog/emacs-as-a-rust-ide.html)
* [Official rust mode](https://github.com/rust-lang/rust-mode)
* [Rust LSP mode](https://emacs-lsp.github.io/lsp-mode/page/lsp-rust-analyzer/)
* [Rust LSP emacs install](https://rust-analyzer.github.io/manual.html#lsp-mode)
