# Git SSH Keys

## Overview

It occured to me that blindly accepting SSH identity from Github/Gitlab/etc
is probably not a good idea!  So I've looked around for information on the 
keys that they have published so I can at least do a basic check that 
connections are sound.


## SSH Keys

### GitHub

The details are published [here](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints)

Public key fringerprints, grabbed from that page on 26th June 2023

```
    SHA256:uNiVztksCsDhcc0u9e8BujQXVUpKZIDTMczCvj3tD2s (RSA)
    SHA256:br9IjFspm1vxR3iA35FWE+4VTyz1hYVLIE2t1/CeyWQ (DSA - deprecated)
    SHA256:p2QAMXNIC1TJYWeIOttrVc98/R1BUFWu3/LiyKgUfQM (ECDSA)
    SHA256:+DiY3wvvV6TuJJhbpZisF/zLDA0zPMSvHdkr4UvCOqU (Ed25519)
```


### GitLab

The keys are documented [here](https://docs.gitlab.com/ee/user/gitlab_com/index.html#ssh-host-keys-fingerprints)

Public key fringerprints, grabbed from that page on 26th June 2023


```
ED25519	eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8
RSA	ROQFvPThGrW4RuWLoL9tq9I9zJ42fK4XywyRtbOz/EQ
DSA (deprecated) p8vZBUOR0XQz6sYiaWSMLmh0t9i8srqYKool/Xfdfqw
ECDSA HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw
```
