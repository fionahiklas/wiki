## Time

### 4/4

4 beats to the bar


### 3/4

3 beats to the bar


### 2/4

2 beats to the bar



## Staves

### Treble Clef


### Bass/G Clef



## Notes

### Semibrieve

Full note, count of 4

![Semibrieve note](./images/note-semibrieve.svg width="100")


### Minim 

Half note, count of 2

![Minim note](images/note-minim.svg)


### Crotchet

Quarter note, count of 1

![Crotchet note](images/note-crotchet.svg)




## References

* How to encode musical noted in Unicode and which Fonts support them are discussed [here](./software:-configuration:-Unicode-and-Fonts)

&#x1D15D; - &#x1D15E; - &#x1D15F;

